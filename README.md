Reactive Google Maps Services reference app that:
===================================================
* utilizes RxJava, Retrofit2, Dagger2
* uses Google Location, Places, Directions API in reactive way
* consists of 2 screens and some custom views
* downloads .json with a user list from server and displays data about each user in a list view  
  GET - http://mobi.connectedcar360.net/api/?op=list
* keeps user list up-to-date on daily basis
* allows to select a user and downloads .json with current position of user vehicles (if valid cached position available - use that)  
  GET - http://mobi.connectedcar360.net/api/?op=getlocations&userid={userid}
* caches last known position of each vehicle with expiration time 30 sec
* displays user vehicles on a map with a custom annotation and callout that shows vehicle image (loaded dynamically from API above using Glide library), vehicle name, current address (loaded dynamically using Google Places API) and color
* reloads vehicle positions every minute while map is open
* can plot a nearest route (using Google Directions API) from vehicle of choice to current device location (uses Google Location API) on a map
* displays human-readable error if any occur
* well-considered project structure, clean code and logic is highly valued

Demo:
===============================================
* Screencast: https://drive.google.com/open?id=0B3xpzS7S85MyemFEVHd3WnZCYUk
* Apk: https://drive.google.com/open?id=0B3xpzS7S85MyNzZNUlBfMkFSbjA

App based on Reark:
===============================================
Reark = RxJava architecture library for Android
https://github.com/reark/reark

![High-level architecture](http://reark.github.io/reark/images/architecture2.1.png "High-level architecture")

License
=======

    The MIT License

    Copyright (c) 2013-2016 reark project contributors

    https://github.com/reark/reark/graphs/contributors

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
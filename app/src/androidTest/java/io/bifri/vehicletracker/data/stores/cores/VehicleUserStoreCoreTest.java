package io.bifri.vehicletracker.data.stores.cores;

import android.content.ContentProvider;
import android.content.pm.ProviderInfo;
import android.support.annotation.NonNull;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.test.ProviderTestCase2;
import android.test.mock.MockContentResolver;

import com.google.gson.Gson;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.bifri.vehicletracker.Constants;
import io.bifri.vehicletracker.data.schematicProvider.generated.AppProvider;
import io.bifri.vehicletracker.pojo.VehicleUser;
import io.bifri.vehicletracker.pojo.response.Owner;
import rx.observers.TestSubscriber;

import static io.bifri.vehicletracker.data.schematicProvider.DatabaseProvider.VehicleUsers.VEHICLE_USERS;
import static java.util.Arrays.asList;

@RunWith(AndroidJUnit4.class)
public class VehicleUserStoreCoreTest extends ProviderTestCase2<AppProvider> {

    private VehicleUserStoreCore mVehicleUserStoreCore;

    private ContentProvider mContentProvider;

    public VehicleUserStoreCoreTest() {
        super(AppProvider.class, AppProvider.AUTHORITY);
    }

    @Before
    @Override
    public void setUp() throws Exception {
        setContext(InstrumentationRegistry.getTargetContext());

        final ProviderInfo providerInfo = new ProviderInfo();
        providerInfo.authority = AppProvider.AUTHORITY;

        mContentProvider = new AppProvider();
        mContentProvider.attachInfo(InstrumentationRegistry.getTargetContext(), providerInfo);
        mContentProvider.delete(VEHICLE_USERS, null, null);

        Thread.sleep(Constants.Tests.PROVIDER_WAIT_TIME);

        final MockContentResolver contentResolver = new MockContentResolver();
        contentResolver.addProvider(AppProvider.AUTHORITY, mContentProvider);

        mVehicleUserStoreCore = new VehicleUserStoreCore(contentResolver, new Gson());

        super.setUp();
    }

    @After
    @Override
    public void tearDown() throws Exception {
        mContentProvider.delete(VEHICLE_USERS, null, null);

        super.tearDown();
    }

    // PUT

    @Test
    public void put_WithTwoDifferentIds_StoresTwoValues() throws InterruptedException {
        final VehicleUser value1 = create(100, "test name 1");
        final VehicleUser value2 = create(200, "test name 2");
        TestSubscriber<VehicleUser> testSubscriber1 = new TestSubscriber<>();
        TestSubscriber<VehicleUser> testSubscriber2 = new TestSubscriber<>();

        mVehicleUserStoreCore.put(100, value1);
        mVehicleUserStoreCore.put(200, value2);
        Thread.sleep(Constants.Tests.PROVIDER_WAIT_TIME);
        mVehicleUserStoreCore.getCached(100).subscribe(testSubscriber1);
        mVehicleUserStoreCore.getCached(200).subscribe(testSubscriber2);

        testSubscriber1.awaitTerminalEvent();
        testSubscriber1.assertCompleted();
        testSubscriber1.assertNoErrors();
        testSubscriber1.assertValue(value1);

        testSubscriber2.awaitTerminalEvent();
        testSubscriber2.assertCompleted();
        testSubscriber2.assertNoErrors();
        testSubscriber2.assertValue(value2);
    }

    // GET STREAM

    @Test
    public void getStream_EmitsValuesForId_AndDoesNotComplete() {
        final VehicleUser value1 = create(100, "test name 1");
        final VehicleUser value2 = create(200, "test name 2");
        TestSubscriber<VehicleUser> testSubscriber1 = new TestSubscriber<>();
        TestSubscriber<VehicleUser> testSubscriber2 = new TestSubscriber<>();

        mVehicleUserStoreCore.getStream(100).subscribe(testSubscriber1);
        mVehicleUserStoreCore.getStream(200).subscribe(testSubscriber2);
        mVehicleUserStoreCore.put(100, value1);
        mVehicleUserStoreCore.put(200, value2);

        testSubscriber1.awaitTerminalEvent(Constants.Tests.PROVIDER_WAIT_TIME, TimeUnit.MILLISECONDS);
        testSubscriber1.assertNotCompleted();
        testSubscriber1.assertNoErrors();
        testSubscriber1.assertValue(value1);

        testSubscriber2.awaitTerminalEvent(Constants.Tests.PROVIDER_WAIT_TIME, TimeUnit.MILLISECONDS);
        testSubscriber2.assertNotCompleted();
        testSubscriber2.assertNoErrors();
        testSubscriber2.assertValue(value2);
    }

    @Test
    public void getStream_DoesNotEmitInitialValue() {
        final VehicleUser value = create(100, "test name");
        TestSubscriber<VehicleUser> testSubscriber = new TestSubscriber<>();

        mVehicleUserStoreCore.put(100, value);
        mVehicleUserStoreCore.getStream(100).subscribe(testSubscriber);
        mVehicleUserStoreCore.put(100, value);

        testSubscriber.awaitTerminalEvent(Constants.Tests.PROVIDER_WAIT_TIME, TimeUnit.MILLISECONDS);
        testSubscriber.assertNotCompleted();
        testSubscriber.assertNoErrors();
        testSubscriber.assertValue(value);
    }

    // GET ALL CACHED

    @Test
    public void getAllCached_ReturnsAllData_AndCompletes() throws InterruptedException {
        // ARRANGE
        final VehicleUser value1 = create(100, "test name 1");
        final VehicleUser value2 = create(200, "test name 2");
        final VehicleUser value3 = create(300, "test name 3");
        TestSubscriber<List<VehicleUser>> testSubscriber = new TestSubscriber<>();

        // ACT
        mVehicleUserStoreCore.put(100, value1);
        mVehicleUserStoreCore.put(200, value2);
        Thread.sleep(Constants.Tests.PROVIDER_WAIT_TIME);
        mVehicleUserStoreCore.getAllCached().subscribe(testSubscriber);
        mVehicleUserStoreCore.put(300, value3);

        // ASSERT
        testSubscriber.awaitTerminalEvent(Constants.Tests.PROVIDER_WAIT_TIME, TimeUnit.MILLISECONDS);
        testSubscriber.assertCompleted();
        testSubscriber.assertNoErrors();
        testSubscriber.assertValue(asList(value1, value2));
    }

    // GET ALL STREAM

    @Test
    public void getAllStream_ReturnsAllData_AndDoesNotComplete() throws InterruptedException {
        // ARRANGE
        final VehicleUser value1 = create(100, "test name 1");
        final VehicleUser value2 = create(200, "test name 2");
        final VehicleUser value3 = create(300, "test name 3");
        TestSubscriber<VehicleUser> testSubscriber = new TestSubscriber<>();

        // ACT
        mVehicleUserStoreCore.put(100, value1);
        Thread.sleep(Constants.Tests.PROVIDER_WAIT_TIME);
        mVehicleUserStoreCore.getAllStream().subscribe(testSubscriber);
        mVehicleUserStoreCore.put(200, value2);
        Thread.sleep(Constants.Tests.PROVIDER_WAIT_TIME);
        mVehicleUserStoreCore.put(300, value3);

        // ASSERT
        testSubscriber.awaitTerminalEvent(Constants.Tests.PROVIDER_WAIT_TIME, TimeUnit.MILLISECONDS);
        testSubscriber.assertNotCompleted();
        testSubscriber.assertNoErrors();
        testSubscriber.assertReceivedOnNext(asList(value2, value3));
    }

    @NonNull
    private static VehicleUser create(Integer id, String name) {
        return new VehicleUser(id, name, 10, 10, new Owner("testAvatar"), new Date());
    }

}

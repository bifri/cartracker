package io.bifri.vehicletracker.data.stores;

import android.content.pm.ProviderInfo;
import android.support.annotation.NonNull;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.test.ProviderTestCase2;
import android.test.mock.MockContentResolver;

import com.google.gson.Gson;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import io.bifri.vehicletracker.Constants;
import io.bifri.vehicletracker.data.schematicProvider.generated.AppProvider;
import io.bifri.vehicletracker.pojo.VehicleUser;
import io.bifri.vehicletracker.pojo.response.Owner;
import rx.Observable;
import rx.observers.TestSubscriber;

import static io.bifri.vehicletracker.data.schematicProvider.DatabaseProvider.VehicleUsers.VEHICLE_USERS;
import static io.bifri.vehicletracker.pojo.VehicleUser.none;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;

@RunWith(AndroidJUnit4.class)
public class VehicleUserStoreTest extends ProviderTestCase2<AppProvider> {

    private VehicleUserStore mVehicleUserStore;

    private TestSubscriber<VehicleUser> mTestSubscriber;

    private AppProvider mContentProvider;

    private Gson mGson = new Gson();
    
    public VehicleUserStoreTest() {
        super(AppProvider.class, AppProvider.AUTHORITY);
    }

    @Before
    @Override
    public void setUp() throws Exception {
        setContext(InstrumentationRegistry.getTargetContext());

        final ProviderInfo providerInfo = new ProviderInfo();
        providerInfo.authority = AppProvider.AUTHORITY;

        mContentProvider = new AppProvider();
        mContentProvider.attachInfo(InstrumentationRegistry.getTargetContext(), providerInfo);
        mContentProvider.delete(VEHICLE_USERS, null, null);

        Thread.sleep(Constants.Tests.PROVIDER_WAIT_TIME);

        final MockContentResolver contentResolver = new MockContentResolver();
        contentResolver.addProvider(AppProvider.AUTHORITY, mContentProvider);

        mVehicleUserStore = new VehicleUserStore(contentResolver, mGson);
        mTestSubscriber = new TestSubscriber<>();
        
        super.setUp();
    }

    @After
    @Override
    public void tearDown() throws Exception {
        mContentProvider.delete(VEHICLE_USERS, null, null);

        super.tearDown();
    }

    @Test
    public void getOne_WithData_ReturnsData_AndCompletes() throws InterruptedException {
        final VehicleUser value = create(100, "vehicleuser1");
        mVehicleUserStore.put(value); // TODO synchronous init with mContentProvider
        Thread.sleep(Constants.Tests.PROVIDER_WAIT_TIME);

        // getOnce is expected to return a observable that emits the value and then completes.
        mVehicleUserStore.getOnce(100).subscribe(mTestSubscriber);

        mTestSubscriber.awaitTerminalEvent(Constants.Tests.PROVIDER_WAIT_TIME, TimeUnit.MILLISECONDS);
        mTestSubscriber.assertCompleted();
        mTestSubscriber.assertNoErrors();
        mTestSubscriber.assertReceivedOnNext(singletonList(value));
    }

    @Test
    public void getOne_WithNoData_ReturnsNoneValue_AndCompletes() {
        // getOnce is expected to emit empty value in case no actual value exists.
        mVehicleUserStore.getOnce(100).subscribe(mTestSubscriber);

        mTestSubscriber.awaitTerminalEvent(Constants.Tests.PROVIDER_WAIT_TIME, TimeUnit.MILLISECONDS);
        mTestSubscriber.assertCompleted();
        mTestSubscriber.assertNoErrors();
        mTestSubscriber.assertValue(none());
    }

    @Test
    public void getOnceAndStream_ReturnsOnlyValuesForSubscribedId_AndDoesNotComplete() throws InterruptedException {
        final VehicleUser value1 = create(100, "vehicleuser1");
        final VehicleUser value2 = create(200, "vehicleuser2");

        mVehicleUserStore.getOnceAndStream(100).subscribe(mTestSubscriber);
        Thread.sleep(Constants.Tests.PROVIDER_WAIT_TIME);
        mVehicleUserStore.put(value1);
        mVehicleUserStore.put(value2);

        mTestSubscriber.awaitTerminalEvent(Constants.Tests.PROVIDER_WAIT_TIME, TimeUnit.MILLISECONDS);
        mTestSubscriber.assertNotCompleted();
        mTestSubscriber.assertNoErrors();
        mTestSubscriber.assertReceivedOnNext(asList(none(), value1));
    }

    @Test
    public void getOnceAndStream_ReturnsAllValuesForSubscribedId_AndDoesNotComplete() throws InterruptedException {
        final VehicleUser value1 = create(100, "vehicleuser-1");
        final VehicleUser value2 = create(100, "vehicleuser-2");
        final VehicleUser value3 = create(100, "vehicleuser-3");

        mVehicleUserStore.getOnceAndStream(100).subscribe(mTestSubscriber);
        Thread.sleep(Constants.Tests.PROVIDER_WAIT_TIME);
        mVehicleUserStore.put(value1);
        Thread.sleep(Constants.Tests.PROVIDER_WAIT_TIME);
        mVehicleUserStore.put(value2);
        Thread.sleep(Constants.Tests.PROVIDER_WAIT_TIME);
        mVehicleUserStore.put(value3);

        mTestSubscriber.awaitTerminalEvent(Constants.Tests.PROVIDER_WAIT_TIME, TimeUnit.MILLISECONDS);
        mTestSubscriber.assertNotCompleted();
        mTestSubscriber.assertNoErrors();
        mTestSubscriber.assertReceivedOnNext(asList(none(), value1, value2, value3));
    }

    @Test
    public void getOnceAndStream_ReturnsOnlyNewValues_AndDoesNotComplete() throws InterruptedException {
        final VehicleUser value = create(100, "vehicleuser1");

        // In the default store implementation identical values are filtered out.
        mVehicleUserStore.getOnceAndStream(100).subscribe(mTestSubscriber);
        Thread.sleep(Constants.Tests.PROVIDER_WAIT_TIME);
        mVehicleUserStore.put(value);
        mVehicleUserStore.put(value);

        mTestSubscriber.awaitTerminalEvent(Constants.Tests.PROVIDER_WAIT_TIME, TimeUnit.MILLISECONDS);
        mTestSubscriber.assertNotCompleted();
        mTestSubscriber.assertNoErrors();
        mTestSubscriber.assertReceivedOnNext(asList(none(), value));
    }

    @Test
    public void getOnceAndStream_WithInitialValue_ReturnsInitialValues_AndDoesNotComplete() throws InterruptedException {
        final VehicleUser value = create(100, "vehicleuser1");
        mVehicleUserStore.put(value);
        Thread.sleep(Constants.Tests.PROVIDER_WAIT_TIME);

        mVehicleUserStore.getOnceAndStream(100).subscribe(mTestSubscriber);

        mTestSubscriber.awaitTerminalEvent(Constants.Tests.PROVIDER_WAIT_TIME, TimeUnit.MILLISECONDS);
        mTestSubscriber.assertNotCompleted();
        mTestSubscriber.assertNoErrors();
        mTestSubscriber.assertReceivedOnNext(singletonList(value));
    }

    @Test
    public void getOnceAndStream_WithInitialValue_WithDelayedSubscription_ReturnsFirstValue_AndDoesNotComplete() throws InterruptedException {
        final VehicleUser value1 = create(100, "vehicleuser1");
        final VehicleUser value2 = create(100, "vehicleuser1");

        // This behavior is a little surprising, but it is because we cannot guarantee that the
        // observable that is produced as the stream will keep its first (cached) value up to date.
        // The only ways to around this would be custom subscribe function or converting the
        // source observable into a behavior, but these would significantly increase the
        // complexity and are hard to implement in other kinds of store (such as content providers).

        // Put initial value.
        mVehicleUserStore.put(value1);
        Thread.sleep(Constants.Tests.PROVIDER_WAIT_TIME);

        // Create the stream observable but do not subscribe immediately.
        Observable<VehicleUser> stream = mVehicleUserStore.getOnceAndStream(100);

        // Put new value into the store.
        mVehicleUserStore.put(value2);

        // Subscribe to stream that was created potentially a long time ago.
        stream.subscribe(mTestSubscriber);

        // Observe that the stream actually gives as the first item the cached value at the time of
        // creating the stream observable.
        mTestSubscriber.awaitTerminalEvent(Constants.Tests.PROVIDER_WAIT_TIME, TimeUnit.MILLISECONDS);
        mTestSubscriber.assertNotCompleted();
        mTestSubscriber.assertNoErrors();
        mTestSubscriber.assertReceivedOnNext(singletonList(value1));
    }

    @NonNull
    private static VehicleUser create(int id, String name) {
        return new VehicleUser(id, name, 10, 10, new Owner("owner"), new Date());
    }

}

package io.bifri.vehicletracker.pojo.response;

import java.util.List;

import static java.util.Collections.unmodifiableList;

public class VehicleLocations {

    private List<VehicleLocation> data;

    public VehicleLocations() {
    }

    public VehicleLocations(List<VehicleLocation> data) {
        this.data = data;
    }

    public List<VehicleLocation> getData() {
        return unmodifiableList(data);
    }

}

package io.bifri.vehicletracker.pojo;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.lang.reflect.Field;
import java.util.Date;

import io.bifri.vehicletracker.pojo.response.Owner;
import io.reark.reark.pojo.MetadataAware;
import io.reark.reark.pojo.OverwritablePojo;
import io.reark.reark.utils.Log;

import static io.bifri.vehicletracker.utils.DateUtils.EMPTY_DATE;

public class VehicleUser extends OverwritablePojo<VehicleUser>
        implements MetadataAware<VehicleUser> {

    private static final String TAG = VehicleUser.class.getSimpleName();

    @SuppressWarnings("unchecked")
    private static final VehicleUser NONE =
            new VehicleUser(-1, Owner.empty(), EMPTY_DATE);

    @SerializedName("userid")
    private Integer id;
    private Owner owner;

    // Metadata fields
    private Date updateDate;

    public VehicleUser() {

    }

    public VehicleUser(Integer id,
                       Owner owner,
                       Date updateDate) {
        this.id = id;
        this.owner = owner;
        this.updateDate = updateDate;
    }

    public VehicleUser(@NonNull final VehicleUser other) {
        this(other.id,
                other.owner,
                other.updateDate);
    }

    @NonNull
    public static VehicleUser none() {
        return NONE;
    }

    public boolean isSome() {
        return id != null && id != -1;
    }

    @NonNull
    @Override
    protected Class<VehicleUser> getTypeParameterClass() {
        return VehicleUser.class;
    }

    @Override
    protected boolean isEmpty(@NonNull final Field field,
                              @NonNull final OverwritablePojo<VehicleUser> pojo) {
        try {
            if (field.get(pojo) instanceof Owner) {
                return false;
            }
        } catch (IllegalAccessException e) {
            Log.e(TAG, "Failed get at " + field.getName(), e);
        }

        return super.isEmpty(field, pojo);
    }

    public Integer getId() {
        return id;
    }

    public Owner getOwner() {
        return owner;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public VehicleUser withUpdateDate(Date updateDate) {
        return new VehicleUser(id, owner, updateDate);
    }

    @Override
    public String toString() {
        return "VehicleUser{" +
                "id=" + id +
                ", owner=" + owner +
                ", updateDate=" + updateDate +
                '}';
    }

    // Function for checking the data equality. Useful when we want to check only the data
    // parts, leaving out e.g. the update date. Note that we need to check every field:
    // this is an object comparison, not identity comparison.
    @SuppressWarnings("SimplifiableIfStatement")
    @Override
    public boolean dataEquals(@NonNull VehicleUser that) {
        if (id != null
                ? !id.equals(that.id)
                : that.id != null) {
            return false;
        }
        return owner != null
                ? owner.equals(that.owner)
                : that.owner == null;
    }

    @Override
    public boolean metadataEquals(@NonNull VehicleUser that) {
        return updateDate != null
                ? updateDate.equals(that.updateDate)
                : that.updateDate == null;
    }

    @SuppressWarnings("SimplifiableIfStatement")
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        VehicleUser that = (VehicleUser) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (owner != null ? !owner.equals(that.owner) : that.owner != null) return false;
        return updateDate != null ? updateDate.equals(that.updateDate) : that.updateDate == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (owner != null ? owner.hashCode() : 0);
        result = 31 * result + (updateDate != null ? updateDate.hashCode() : 0);
        return result;
    }

}


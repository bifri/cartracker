package io.bifri.vehicletracker.pojo.response;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

import io.reark.reark.pojo.MetadataAware;
import io.reark.reark.pojo.OverwritablePojo;

import static io.bifri.vehicletracker.utils.DateUtils.EMPTY_DATE;

public class VehicleLocation extends OverwritablePojo<VehicleLocation>
        implements MetadataAware<VehicleLocation> {

    private static final VehicleLocation NONE =
            new VehicleLocation(-1, Double.MIN_VALUE, Double.MIN_VALUE, EMPTY_DATE);

    @SerializedName("vehicleid")
    private Integer vehicleId;
    private Double lat;
    private Double lon;

    // Metadata fields
    private Date updateDate;

    public VehicleLocation() {

    }

    public VehicleLocation(Integer vehicleId, Double lat, Double lon, Date updateDate) {
        this.vehicleId = vehicleId;
        this.lat = lat;
        this.lon = lon;
        this.updateDate = updateDate;
    }

    public VehicleLocation(VehicleLocation other) {
        this(other.vehicleId,
                other.lat,
                other.lon,
                other.updateDate);
    }

    @NonNull
    public static VehicleLocation none() {
        return NONE;
    }

    public boolean isSome() {
        return vehicleId !=null && lat != null && lon != null;
    }

    public Integer getVehicleId() {
        return vehicleId;
    }

    public Double getLat() {
        return lat;
    }

    public Double getLon() {
        return lon;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public VehicleLocation withUpdateDate(Date updateDate) {
        return new VehicleLocation(vehicleId, lat, lon, updateDate);
    }

    @NonNull
    @Override
    protected Class<VehicleLocation> getTypeParameterClass() {
        return VehicleLocation.class;
    }

    @Override
    public String toString() {
        return "VehicleLocation{" +
                "vehicleId=" + vehicleId +
                ", lat=" + lat +
                ", lon=" + lon +
                ", updateDate=" + updateDate +
                '}';
    }

    // Function for checking the data equality. Useful when we want to check only the data
    // parts, leaving out e.g. the update date. Note that we need to check every field:
    // this is an object comparison, not identity comparison.
    @SuppressWarnings("SimplifiableIfStatement")
    @Override
    public boolean dataEquals(@NonNull VehicleLocation other) {
        if (vehicleId != null ? !vehicleId.equals(other.vehicleId) : other.vehicleId != null)
            return false;
        if (lat != null ? !lat.equals(other.lat) : other.lat != null) return false;
        return lon != null ? lon.equals(other.lon) : other.lon == null;
    }

    @Override
    public boolean metadataEquals(@NonNull VehicleLocation other) {
        return updateDate != null
                ? updateDate.equals(other.updateDate)
                : other.updateDate == null;
    }

    @SuppressWarnings("SimplifiableIfStatement")
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        VehicleLocation that = (VehicleLocation) o;

        if (vehicleId != null ? !vehicleId.equals(that.vehicleId) : that.vehicleId != null)
            return false;
        if (lat != null ? !lat.equals(that.lat) : that.lat != null) return false;
        if (lon != null ? !lon.equals(that.lon) : that.lon != null) return false;
        return updateDate != null ? updateDate.equals(that.updateDate) : that.updateDate == null;

    }

    @Override
    public int hashCode() {
        int result = vehicleId != null ? vehicleId.hashCode() : 0;
        result = 31 * result + (lat != null ? lat.hashCode() : 0);
        result = 31 * result + (lon != null ? lon.hashCode() : 0);
        result = 31 * result + (updateDate != null ? updateDate.hashCode() : 0);
        return result;
    }

}

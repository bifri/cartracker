package io.bifri.vehicletracker.pojo.response;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

public class Owner {

    private static final Owner EMPTY = new Owner("", "", "");

    private String name;
    private String surname;
    @SerializedName("foto")
    private String photo;

    public Owner() {

    }

    public Owner(String name, String surname, String photo) {
        this.name = name;
        this.surname = surname;
        this.photo = photo;
    }

    @NonNull
    public static Owner empty() {
        return EMPTY;
    }

    public boolean isSome() {
        return !name.isEmpty();
    }


    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getPhoto() {
        return photo;
    }

    @Override
    public String toString() {
        return "Owner{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", photo='" + photo + '\'' +
                '}';
    }

    @SuppressWarnings("SimplifiableIfStatement")
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Owner owner = (Owner) o;

        if (name != null ? !name.equals(owner.name) : owner.name != null) return false;
        if (surname != null ? !surname.equals(owner.surname) : owner.surname != null) return false;
        return photo != null ? photo.equals(owner.photo) : owner.photo == null;

    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (surname != null ? surname.hashCode() : 0);
        result = 31 * result + (photo != null ? photo.hashCode() : 0);
        return result;
    }

}

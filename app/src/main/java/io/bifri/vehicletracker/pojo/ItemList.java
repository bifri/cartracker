/*
 * The MIT License
 *
 * Copyright (c) 2013-2016 reark project contributors
 *
 * https://github.com/reark/reark/graphs/contributors
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package io.bifri.vehicletracker.pojo;

import android.support.annotation.NonNull;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import io.reark.reark.pojo.MetadataAware;

import static io.bifri.vehicletracker.utils.DateUtils.EMPTY_DATE;

public class ItemList<T> implements MetadataAware<ItemList<T>> {

    private static final ItemList<?> NONE =
            new ItemList<>("", Collections.emptyList(), EMPTY_DATE);

    @NonNull private final T key;
    @NonNull private final List<Integer> items;
    private Date updateDate;

    @NonNull
    public static ItemList<?> none() {
        //noinspection unchecked
        return NONE;
    }

    public ItemList(
            @NonNull final T key,
            @NonNull final List<Integer> items,
            final Date updateDate) {
        this.key = key;
        this.items = items;
        this.updateDate = updateDate;
    }

    public ItemList(@NonNull ItemList<T> other) {
        this(other.key(), other.items(), other.updateDate());
    }

    public T key() {
        return key;
    }

    public List<Integer> items() {
        return Collections.unmodifiableList(items);
    }

    public Date updateDate() {
        return updateDate;
    }

    public ItemList<T> withUpdateDate(Date updateDate) {
        return new ItemList<>(key, items, updateDate);
    }

    public boolean isSome() {
        return !items.isEmpty();
    }

    @SuppressWarnings("SimplifiableIfStatement")
    @Override
    public boolean dataEquals(@NonNull ItemList<T> other) {
        if (!key.equals(other.key)) return false;
        return items.equals(other.items);
    }

    @Override
    public boolean metadataEquals(@NonNull ItemList<T> other) {
        return updateDate.equals(other.updateDate);
    }

    @Override
    public String toString() {
        return "ItemList{" +
                "key=" + key +
                ", items=" + items +
                ", updateDate=" + updateDate +
                '}';
    }

    @SuppressWarnings("SimplifiableIfStatement")
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ItemList<?> itemList = (ItemList<?>) o;

        if (!key.equals(itemList.key)) return false;
        if (!items.equals(itemList.items)) return false;
        return updateDate.equals(itemList.updateDate);

    }

    @Override
    public int hashCode() {
        int result = key.hashCode();
        result = 31 * result + items.hashCode();
        result = 31 * result + updateDate.hashCode();
        return result;
    }

}

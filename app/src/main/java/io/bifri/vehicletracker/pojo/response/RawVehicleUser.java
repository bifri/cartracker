package io.bifri.vehicletracker.pojo.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import static java.util.Collections.unmodifiableList;

public class RawVehicleUser {

    private static final String TAG = RawVehicleUser.class.getSimpleName();

    @SerializedName("userid")
    private Integer id;
    private Owner owner;
    private List<Vehicle> vehicles;

    public RawVehicleUser() {

    }

    public RawVehicleUser(Integer id,
                          Owner owner,
                          List<Vehicle> vehicles) {
        this.id = id;
        this.owner = owner;
        this.vehicles = vehicles;
    }

    public Integer getId() {
        return id;
    }

    public Owner getOwner() {
        return owner;
    }

    public List<Vehicle> getVehicles() {
        return unmodifiableList(vehicles);
    }

    public boolean isSome() {
        return id != null && id != -1;
    }

    @Override
    public String toString() {
        return "VehicleUser{" +
                "id=" + id +
                ", owner=" + owner +
                ", vehicles=" + vehicles +
                '}';
    }

}


package io.bifri.vehicletracker.pojo.response;

import java.util.List;

import static java.util.Collections.unmodifiableList;

public class Users {

    private List<RawVehicleUser> data;

    public Users() {

    }

    public Users(List<RawVehicleUser> data) {
        this.data = data;
    }

    public List<RawVehicleUser> getData() {
        return unmodifiableList(data);
    }

}

package io.bifri.vehicletracker.pojo.response;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

import io.reark.reark.pojo.MetadataAware;
import io.reark.reark.pojo.OverwritablePojo;

import static io.bifri.vehicletracker.utils.DateUtils.EMPTY_DATE;

public class Vehicle extends OverwritablePojo<Vehicle>
        implements MetadataAware<Vehicle> {

    private static final Vehicle NONE = new Vehicle(-1, "", "", "", "", "", "", EMPTY_DATE);

    @SerializedName("vehicleid")
    private Integer id;
    private String make;
    private String model;
    private String year;
    private String color;
    private String vin;
    @SerializedName("foto")
    private String photo;

    // Metadata fields
    private Date updateDate;

    public Vehicle() {

    }

    public Vehicle(Integer id, String make, String model,
                   String year, String color, String vin,
                   String photo, Date updateDate) {
        this.id = id;
        this.make = make;
        this.model = model;
        this.year = year;
        this.color = color;
        this.vin = vin;
        this.photo = photo;
        this.photo = photo;
        this.updateDate = updateDate;
    }

    public Vehicle(Vehicle other) {
        this(other.id, other.make, other.model, other.year,
                other.color, other.vin, other.photo, other.updateDate);
    }

    @NonNull
    public static Vehicle none() {
        return NONE;
    }

    public boolean isSome() {
        return id != null && id != -1;
    }

    @NonNull
    @Override
    protected Class<Vehicle> getTypeParameterClass() {
        return Vehicle.class;
    }

    public Integer getId() {
        return id;
    }

    public String getMake() {
        return make;
    }

    public String getModel() {
        return model;
    }

    public String getYear() {
        return year;
    }

    public String getColor() {
        return color;
    }

    public String getVin() {
        return vin;
    }

    public String getPhoto() {
        return photo;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public Vehicle withUpdateDate(Date updateDate) {
        return new Vehicle(id, make, model, year, color, vin, photo, updateDate);
    }

    @Override
    public String toString() {
        return "Vehicle{" +
                "id=" + id +
                ", make='" + make + '\'' +
                ", model='" + model + '\'' +
                ", year='" + year + '\'' +
                ", color='" + color + '\'' +
                ", vin='" + vin + '\'' +
                ", photo='" + photo + '\'' +
                '}';
    }

    // Function for checking the data equality. Useful when we want to check only the data
    // parts, leaving out e.g. the update date. Note that we need to check every field:
    // this is an object comparison, not identity comparison.
    @SuppressWarnings("SimplifiableIfStatement")
    @Override
    public boolean dataEquals(@NonNull Vehicle that) {
        if (id != null ? !id.equals(that.id) : that.id != null)
            return false;
        if (make != null ? !make.equals(that.make) : that.make != null) return false;
        if (model != null ? !model.equals(that.model) : that.model != null) return false;
        if (year != null ? !year.equals(that.year) : that.year != null) return false;
        if (color != null ? !color.equals(that.color) : that.color != null) return false;
        if (vin != null ? !vin.equals(that.vin) : that.vin != null) return false;
        return photo != null ? photo.equals(that.photo) : that.photo == null;
    }

    @Override
    public boolean metadataEquals(@NonNull Vehicle that) {
        return updateDate != null
                ? updateDate.equals(that.updateDate)
                : that.updateDate == null;
    }

    @SuppressWarnings("SimplifiableIfStatement")
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Vehicle vehicle = (Vehicle) o;

        if (id != null ? !id.equals(vehicle.id) : vehicle.id != null)
            return false;
        if (make != null ? !make.equals(vehicle.make) : vehicle.make != null) return false;
        if (model != null ? !model.equals(vehicle.model) : vehicle.model != null) return false;
        if (year != null ? !year.equals(vehicle.year) : vehicle.year != null) return false;
        if (color != null ? !color.equals(vehicle.color) : vehicle.color != null) return false;
        if (vin != null ? !vin.equals(vehicle.vin) : vehicle.vin != null) return false;
        if (photo != null ? !photo.equals(vehicle.photo) : vehicle.photo != null) return false;
        return updateDate != null ? updateDate.equals(vehicle.updateDate) : vehicle.updateDate == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (make != null ? make.hashCode() : 0);
        result = 31 * result + (model != null ? model.hashCode() : 0);
        result = 31 * result + (year != null ? year.hashCode() : 0);
        result = 31 * result + (color != null ? color.hashCode() : 0);
        result = 31 * result + (vin != null ? vin.hashCode() : 0);
        result = 31 * result + (photo != null ? photo.hashCode() : 0);
        result = 31 * result + (updateDate != null ? updateDate.hashCode() : 0);
        return result;
    }

}

/*
 * The MIT License
 *
 * Copyright (c) 2013-2016 reark project contributors
 *
 * https://github.com/reark/reark/graphs/contributors
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package io.bifri.vehicletracker;

import android.app.Application;
import android.support.annotation.NonNull;

import javax.inject.Inject;

import io.bifri.vehicletracker.injections.Graph;
import io.bifri.vehicletracker.injections.Graph.Initializer;
import io.bifri.vehicletracker.utils.ApplicationInstrumentation;

public class App extends Application {

    private static final String TAG = App.class.getSimpleName();

    private static App sInstance;

    private Graph mGraph;

    @Inject
    ApplicationInstrumentation mInstrumentation;

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
        mGraph = Initializer.init(this);
        getGraph().inject(this);

        mInstrumentation.init();
    }

    @NonNull
    public static App getInstance() {
        return sInstance;
    }

    @NonNull
    public Graph getGraph() {
        return mGraph;
    }

}

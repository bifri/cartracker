/*
 * The MIT License
 *
 * Copyright (c) 2013-2016 reark project contributors
 *
 * https://github.com/reark/reark/graphs/contributors
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package io.bifri.vehicletracker.injections;

import android.app.Application;

import javax.inject.Singleton;

import dagger.Component;
import io.bifri.vehicletracker.App;
import io.bifri.vehicletracker.activities.UserVehiclesOnMapActivity;
import io.bifri.vehicletracker.activities.VehicleUserDetailsActivity;
import io.bifri.vehicletracker.activities.VehicleUserListActivity;
import io.bifri.vehicletracker.data.DataStoreModule;
import io.bifri.vehicletracker.fragments.VehicleUserFragment;
import io.bifri.vehicletracker.fragments.VehicleUsersFragment;
import io.bifri.vehicletracker.location.LocationServicesModule;
import io.bifri.vehicletracker.network.NetworkService;
import io.bifri.vehicletracker.viewmodels.LocationServicesViewModel;
import io.bifri.vehicletracker.viewmodels.UserVehiclesLocationViewModel;
import io.bifri.vehicletracker.viewmodels.VehicleUserViewModel;
import io.bifri.vehicletracker.viewmodels.VehicleUsersViewModel;
import io.bifri.vehicletracker.viewmodels.ViewModelModule;

@Singleton
@Component(modules = {
        ApplicationModule.class,
        DataStoreModule.class,
        LocationServicesModule.class,
        ViewModelModule.class,
        InstrumentationModule.class
})
public interface Graph {

    void inject(VehicleUsersViewModel vehicleUsersViewModel);
    void inject(VehicleUserViewModel vehicleUserViewModel);
    void inject(UserVehiclesLocationViewModel userVehiclesLocationViewModel);
    void inject(LocationServicesViewModel locationServicesViewModel);
    void inject(VehicleUserDetailsActivity vehicleUserDetailsActivity);
    void inject(VehicleUserListActivity vehicleUserListActivity);
    void inject(UserVehiclesOnMapActivity userVehiclesOnMapActivity);
    void inject(VehicleUserFragment vehicleUserFragment);
    void inject(VehicleUsersFragment vehicleUsersFragment);
    void inject(NetworkService networkService);
    void inject(App app);

    final class Initializer {

        public static Graph init(Application application) {
            return DaggerGraph.builder()
                              .applicationModule(new ApplicationModule(application))
                              .build();
        }
    }

}

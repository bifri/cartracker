package io.bifri.vehicletracker.view;

import android.location.Address;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.v4.util.Pair;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import io.bifri.vehicletracker.pojo.response.Vehicle;
import io.bifri.vehicletracker.pojo.response.VehicleLocation;
import io.bifri.vehicletracker.utils.rx.CreateObservable;
import io.bifri.vehicletracker.viewmodels.LocationServicesViewModel;
import io.bifri.vehicletracker.viewmodels.UserVehiclesLocationViewModel;
import io.reark.reark.utils.Log;
import io.reark.reark.utils.RxViewBinder;
import rx.Observable;
import rx.observables.ConnectableObservable;
import rx.subscriptions.CompositeSubscription;

import static io.bifri.vehicletracker.viewmodels.LocationServicesViewModel.DEFAULT_PROVIDER;
import static io.reark.reark.utils.Preconditions.get;
import static rx.Observable.combineLatest;
import static rx.android.schedulers.AndroidSchedulers.mainThread;

public class UserVehiclesOnMapViewBinder extends RxViewBinder {

    private static final String TAG = UserVehiclesOnMapViewBinder.class.getSimpleName();

    private final UserVehiclesOnMapView mView;
    private final UserVehiclesLocationViewModel mVehicleLocationViewModel;
    private final LocationServicesViewModel mLocationServicesViewModel;

    public UserVehiclesOnMapViewBinder(
            @NonNull final UserVehiclesOnMapView view,
            @NonNull final UserVehiclesLocationViewModel viewModel,
            @NonNull final LocationServicesViewModel locationServicesViewModel) {
        mView = get(view);
        mVehicleLocationViewModel = get(viewModel);
        mLocationServicesViewModel = get(locationServicesViewModel);
    }

    @Override
    protected void bindInternal(@NonNull final CompositeSubscription s) {
        ConnectableObservable<Pair<Marker, Pair<Vehicle, VehicleLocation>>> markerClicks =
                mView.getMapReadyStream()
                        .switchMap(mView::markerClicks)
                        .filter(pair -> mView.vehicleMarker(pair.first))
                        .doOnNext(pair -> mView.onMarkerClicked(pair.first))
                        .publish();

        s.add(markerClicks
                .switchMap(pair -> {
                    Observable<Marker> imageStream = mView.getImageReadyStream(pair.first);
                    Observable<Address> addressStream = CreateObservable.from(
                            mLocationServicesViewModel.getStreamOfAddress(),
                            () -> {
                                Location location = new Location(DEFAULT_PROVIDER);
                                location.setLatitude(pair.second.second.getLat());
                                location.setLongitude(pair.second.second.getLon());
                                mLocationServicesViewModel.setRequestForAddress(location);
                            });
                    return Observable.zip(imageStream, addressStream, Pair::new);
                })
                .observeOn(mainThread())
                .subscribe(mView::showInfoWindow,
                        e -> Log.e(TAG, "Error getting image or/and address", e)));

        s.add(markerClicks
                .switchMap(pair -> CreateObservable.from(
                        mLocationServicesViewModel.getStreamOfRoutes(),
                        () -> {
                            LatLng latLng = new LatLng(
                                    pair.second.second.getLat(),
                                    pair.second.second.getLon());
                            mLocationServicesViewModel.setRequestForRoute(latLng);
                        }))
                .observeOn(mainThread())
                .subscribe(mView::drawRoute,
                        e -> Log.e(TAG, "Error getting route", e)));


        s.add(combineLatest(
                mView.getMapReadyStream(),
                mVehicleLocationViewModel.getStreamOfVehicleLocationLists(),
                (__, list) -> list)
                .observeOn(mainThread())
                .subscribe(mView::setMarkers,
                        e -> Log.e(TAG, "Error getting vehicle location lists", e)));

        s.add(mVehicleLocationViewModel.getStreamOfNetworkRequestStatusText()
                .observeOn(mainThread())
                .subscribe(mView::setNetworkRequestStatus));

        s.add(markerClicks.connect());
    }

}

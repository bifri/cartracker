package io.bifri.vehicletracker.view;

import android.support.annotation.NonNull;

import io.bifri.vehicletracker.viewmodels.VehicleUserViewModel;
import io.reark.reark.utils.RxViewBinder;
import rx.android.schedulers.AndroidSchedulers;
import rx.subscriptions.CompositeSubscription;

import static io.reark.reark.utils.Preconditions.get;

public class VehicleUserViewBinder extends RxViewBinder {
    private final VehicleUserView view;
    private final VehicleUserViewModel viewModel;

    public VehicleUserViewBinder(@NonNull final VehicleUserView view,
                                 @NonNull final VehicleUserViewModel viewModel) {
        this.view = get(view);
        this.viewModel = get(viewModel);
    }

    @Override
    protected void bindInternal(@NonNull final CompositeSubscription s) {
        s.add(viewModel.getStreamOfVehicleUser()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(view::setVehicleUser));
    }
}

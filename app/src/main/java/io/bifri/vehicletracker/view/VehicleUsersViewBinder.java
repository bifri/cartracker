package io.bifri.vehicletracker.view;

import android.support.annotation.NonNull;
import android.view.View;

import io.bifri.vehicletracker.pojo.VehicleUser;
import io.bifri.vehicletracker.viewmodels.VehicleUsersViewModel;
import io.reark.reark.utils.RxViewBinder;
import rx.Emitter;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.subscriptions.CompositeSubscription;

import static io.reark.reark.utils.Preconditions.get;
import static rx.android.MainThreadSubscription.verifyMainThread;

public class VehicleUsersViewBinder extends RxViewBinder {

    private final VehicleUsersView view;
    private final VehicleUsersViewModel viewModel;

    public VehicleUsersViewBinder(@NonNull final VehicleUsersView view,
                                  @NonNull final VehicleUsersViewModel viewModel) {
        this.view = get(view);
        this.viewModel = get(viewModel);
    }

    @Override
    protected void bindInternal(@NonNull final CompositeSubscription s) {
        s.add(viewModel.getStreamVehicleUsers()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(view::setVehicleUsers));
        s.add(viewModel.getStreamOfNetworkRequestStatusText()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(view::setNetworkRequestStatus));
        s.add(view.getStreamOfSearchString()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(viewModel::setSearchString));
        s.add(vehicleUsersAdapterOnClick()
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(viewModel::selectedVehicleUser));
    }

    private Observable<VehicleUser> vehicleUsersAdapterOnClick() {
        return Observable.create(
                emitter -> {
                    verifyMainThread();

                    emitter.setCancellation(() -> view.vehicleUsersAdapter().setOnClickListener(null));
                    view.vehicleUsersAdapter().setOnClickListener(this::repositoriesAdapterOnClick);
                },
                Emitter.BackpressureMode.BUFFER);
    }

    private void repositoriesAdapterOnClick(View clickedView) {
        viewModel.selectedVehicleUser(vehicleUser(clickedView));
    }

    private VehicleUser vehicleUser(View clickedView) {
        final int itemPosition = view.mRVVehicleUsers.getChildAdapterPosition(clickedView);
        return view.vehicleUsersAdapter().getItem(itemPosition);
    }

}

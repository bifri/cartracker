/*
 * The MIT License
 *
 * Copyright (c) 2013-2016 reark project contributors
 *
 * https://github.com/reark/reark/graphs/contributors
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package io.bifri.vehicletracker.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.jakewharton.rxbinding.view.RxView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.bifri.vehicletracker.R;
import io.bifri.vehicletracker.pojo.VehicleUser;
import io.bifri.vehicletracker.pojo.response.Owner;
import io.bifri.vehicletracker.utils.glide.SerialTarget;

import static io.reark.reark.utils.Preconditions.checkNotNull;

public class VehicleUserView {

    @BindView(R.id.widget_layout_title) TextView mTVTitle;
    @BindView(R.id.widget_layout_stargazers) TextView mTVStargazers;
    @BindView(R.id.widget_layout_forks) TextView mTVForks;
    @BindView(R.id.widget_avatar_image_view) ImageView mIVAvatar;
    private View mRootView;
    private Unbinder mUnbinder;

    private Context mContext;

    @NonNull
    private final SerialTarget<GlideDrawable> mRequest = new SerialTarget<>();

    public VehicleUserView(Context context, View rootView) {
        mContext = context;
        mRootView = rootView;
        mUnbinder = ButterKnife.bind(this, rootView);
    }

    void setVehicleUser(@NonNull final VehicleUser vehicleUser) {
        checkNotNull(vehicleUser);
        Owner owner = vehicleUser.getOwner();
        if (owner == null) {
            owner = Owner.empty();
        }

        mTVTitle.setText(owner.getName() + " " + owner.getSurname());
        mRequest.set(Glide.with(mContext)
                .load(vehicleUser.getOwner().getPhoto())
                .fitCenter()
                .crossFade()
                .placeholder(android.R.drawable.sym_def_app_icon)
                .into(mIVAvatar));
        RxView.detaches(mRootView).subscribe(__ -> mRequest.onDestroy());
    }

    public void onDestroyView() {
        mUnbinder.unbind();
    }

}

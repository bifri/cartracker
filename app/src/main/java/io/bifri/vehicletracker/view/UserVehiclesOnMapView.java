package io.bifri.vehicletracker.view;

import android.content.Context;
import android.graphics.Color;
import android.location.Address;
import android.support.annotation.NonNull;
import android.support.v4.util.Pair;
import android.view.View;
import android.widget.Toast;

import com.fernandocejas.frodo.annotation.RxLogObservable;
import com.github.polok.routedrawer.RouteDrawer;
import com.github.polok.routedrawer.model.Routes;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.Polyline;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import hugo.weaving.DebugLog;
import io.bifri.vehicletracker.R;
import io.bifri.vehicletracker.adapter.GoogleMapMarkersAdapter;
import io.bifri.vehicletracker.adapter.UserVehiclesOnMapInfoWindowAdapter;
import io.bifri.vehicletracker.pojo.response.Vehicle;
import io.bifri.vehicletracker.pojo.response.VehicleLocation;
import io.bifri.vehicletracker.utils.AnimationUtils;
import io.bifri.vehicletracker.utils.rx.binding.glide.RxGlide;
import io.bifri.vehicletracker.utils.rx.binding.glide.TargetEvent;
import io.bifri.vehicletracker.utils.rx.binding.googlemaps.RxGoogleMaps;
import io.bifri.vehicletracker.viewmodels.UserVehiclesLocationViewModel.ProgressStatus;
import java8.util.stream.StreamSupport;
import rx.Observable;
import rx.observables.ConnectableObservable;
import rx.subscriptions.CompositeSubscription;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.content.pm.PackageManager.PERMISSION_GRANTED;
import static android.support.v4.content.ContextCompat.checkSelfPermission;
import static io.bifri.vehicletracker.utils.rx.binding.glide.TargetEvent.Kind.LOAD_FAILED;
import static io.bifri.vehicletracker.utils.rx.binding.glide.TargetEvent.Kind.RESOURCE_READY;
import static io.bifri.vehicletracker.viewmodels.LocationServicesViewModel.EMPTY_ROUTES;

public class UserVehiclesOnMapView {

    private final Context mContext;
    private final Unbinder mUnbinder;
    private GoogleMap mMap;
    private Map<Marker, Pair<Vehicle, VehicleLocation>> mMarkerToVehicle;
    private GoogleMapMarkersAdapter mMarkersAdapter;
    private UserVehiclesOnMapInfoWindowAdapter mInfoWindowAdapter;
    private Pair<Set<Marker>, Set<Polyline>> mLastRouteComponents =
            new Pair<>(new HashSet<>(), new HashSet<>());
    private ConnectableObservable<GoogleMap> mStreamOfMapReady;

    private final CompositeSubscription mSubscriptions = new CompositeSubscription();

    public UserVehiclesOnMapView(Context context, View rootView, SupportMapFragment mapFragment) {
        mContext = context;
        mUnbinder = ButterKnife.bind(this, rootView);

        mStreamOfMapReady = RxGoogleMaps.mapAndViewReady(mapFragment).replay(1);
        mSubscriptions.add(mStreamOfMapReady.subscribe(this::initMap));
        mSubscriptions.add(mStreamOfMapReady.connect());
    }

    public void onDestroyView() {
        mSubscriptions.unsubscribe();
        mInfoWindowAdapter.clear();
        mUnbinder.unbind();
    }

    @RxLogObservable
    public Observable<GoogleMap> getMapReadyStream() {
        return mStreamOfMapReady.asObservable();
    }

    @RxLogObservable
    public Observable<Pair<Marker, Pair<Vehicle, VehicleLocation>>> markerClicks(GoogleMap map) {
        // We return true to indicate that we have consumed the event and that we do not wish
        // for the default behavior to occur (which is for the camera to move such that the
        // marker is centered and for the marker's info window to open, if it has one).
        return RxGoogleMaps.markerClicks(map, __ -> true)
                .map(marker -> new Pair<>(marker, mMarkerToVehicle.get(marker)));
    }

    @RxLogObservable
    public Observable<Marker> getImageReadyStream(Marker marker) {
        return RxGlide.targetEvents(mContext, mInfoWindowAdapter, marker)
                .filter(targetEvent -> targetEvent.kind() == RESOURCE_READY
                        || targetEvent.kind() == LOAD_FAILED)
                .map(TargetEvent::marker);
    }

    @DebugLog
    private void initMap(GoogleMap map) {
        mMap = map;
        initMapUI();
        mMarkersAdapter = new GoogleMapMarkersAdapter(map);
        mInfoWindowAdapter = new UserVehiclesOnMapInfoWindowAdapter(mContext);
    }

    private void initMapUI() {
        mMap.clear();
        // Override the default content description on the view, for accessibility mode.
        mMap.setContentDescription(mContext.getString(R.string.map_content_description));
        mMap.getUiSettings().setZoomControlsEnabled(true);
//      mMap.getUiSettings().setCompassEnabled(true);
        mMap.getUiSettings().setMapToolbarEnabled(true);
        mMap.setMyLocationEnabled(true);
        if (checkSelfPermission(mContext, ACCESS_FINE_LOCATION) != PERMISSION_GRANTED
                && checkSelfPermission(mContext, ACCESS_COARSE_LOCATION) != PERMISSION_GRANTED) {
            return;
        }
        mMap.setMyLocationEnabled(true);
    }

    @DebugLog
    void setMarkers(@NonNull List<Pair<Vehicle, VehicleLocation>> pairs) {
        if (!mapReady()) {
            return;
        }
        clearPreviousRoute();
        mMarkerToVehicle = mMarkersAdapter.setMarkers(pairs);
        // Setting an info window adapter allows to change the both
        // the contents and look of the info window.
        mInfoWindowAdapter.set(mMarkerToVehicle);
        mMap.setInfoWindowAdapter(mInfoWindowAdapter);
    }

    @DebugLog
    public void onMarkerClicked(Marker marker) {
        clearPreviousRoute();
        AnimationUtils.animateMarkerClick(marker);
    }

    @DebugLog
    public void showInfoWindow(Pair<Marker, Address> markerAndAddress) {
        // tell the maps API it can try to call getInfoWindow,
        // finding the loaded image, address etc.
        mInfoWindowAdapter.setAddress(markerAndAddress.second);
        markerAndAddress.first.showInfoWindow();
    }

    @DebugLog
    public void drawRoute(Routes routes) {
        // TODO: add support of the Google Maps Roads API (Snap to Roads)
        clearPreviousRoute();
        if (routes.equals(EMPTY_ROUTES)) {
            Toast.makeText(mContext, R.string.failed_to_retrieve_route, Toast.LENGTH_LONG)
                    .show();
            return;
        }
        mLastRouteComponents = routeDrawer().drawPath(routes);
    }

    private void clearPreviousRoute() {
        StreamSupport.stream(mLastRouteComponents.first)
                .forEach(Marker::remove);
        StreamSupport.stream(mLastRouteComponents.second)
                .forEach(Polyline::remove);
    }

    void setNetworkRequestStatus(ProgressStatus progressStatus) {

    }

    public boolean vehicleMarker(Marker marker) {
        return mMarkerToVehicle.containsKey(marker);
    }

    private boolean mapReady() {
        if (mMap == null) {
            Toast.makeText(mContext, R.string.map_not_ready, Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private RouteDrawer routeDrawer() {
        return new RouteDrawer.RouteDrawerBuilder(mMap)
                .withColor(Color.BLUE)
                .withWidth(8)
                .withAlpha(0.5f)
                .withMarkerIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE))
                .build();
    }

}

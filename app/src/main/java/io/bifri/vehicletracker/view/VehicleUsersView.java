/*
 * The MIT License
 *
 * Copyright (c) 2013-2016 reark project contributors
 *
 * https://github.com/reark/reark/graphs/contributors
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package io.bifri.vehicletracker.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.jakewharton.rxbinding.widget.RxTextView;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.bifri.vehicletracker.R;
import io.bifri.vehicletracker.adapter.VehicleUsersAdapter;
import io.bifri.vehicletracker.pojo.VehicleUser;
import io.bifri.vehicletracker.viewmodels.VehicleUsersViewModel.ProgressStatus;
import jp.wasabeef.recyclerview.animators.SlideInLeftAnimator;
import rx.Observable;

import static io.reark.reark.utils.Preconditions.checkNotNull;

public class VehicleUsersView {

    @BindView(R.id.vehicle_users_list_view) RecyclerView mRVVehicleUsers;
    @BindView(R.id.vehicle_users_search) EditText mETVehicleUsersSearch;
    @BindView(R.id.vehicle_users_status_text) TextView mTVStatusText;
    private Unbinder mUnbinder;

    private VehicleUsersAdapter mVehicleUsersAdapter;

    private Observable<String> mOutStreamSearchString;

    public VehicleUsersView(Context context, View rootView) {
        mUnbinder = ButterKnife.bind(this, rootView);

        mOutStreamSearchString = RxTextView.textChanges(mETVehicleUsersSearch)
                .map(CharSequence::toString);

        mVehicleUsersAdapter = new VehicleUsersAdapter(Collections.emptyList());

        mRVVehicleUsers.setLayoutManager(new LinearLayoutManager(context));
        mRVVehicleUsers.setHasFixedSize(true);
        mRVVehicleUsers.setItemAnimator(new SlideInLeftAnimator());
        mRVVehicleUsers.addItemDecoration(
                new DividerItemDecoration(context, DividerItemDecoration.VERTICAL));
        mRVVehicleUsers.setAdapter(mVehicleUsersAdapter);
    }

    public void onDestroyView() {
        mUnbinder.unbind();
    }

    public VehicleUsersAdapter vehicleUsersAdapter() {
        return mVehicleUsersAdapter;
    }

    public Observable<String> getStreamOfSearchString() {
        return mOutStreamSearchString;
    }

    void setVehicleUsers(@NonNull final List<VehicleUser> vehicleUsers) {
        checkNotNull(vehicleUsers);
        checkNotNull(mVehicleUsersAdapter);

        mVehicleUsersAdapter.set(vehicleUsers);
    }

    void setNetworkRequestStatus(@NonNull final ProgressStatus networkRequestStatus) {
        checkNotNull(networkRequestStatus);

        setNetworkRequestStatusText(getLoadingStatusString(networkRequestStatus));
    }

    @NonNull
    private static String getLoadingStatusString(ProgressStatus networkRequestStatus) {
        switch (networkRequestStatus) {
            case LOADING:
                return "Loading..";
            case ERROR:
                return "Error occurred";
            case IDLE:
            default:
                return "";
        }
    }

    private void setNetworkRequestStatusText(@NonNull final String networkRequestStatusText) {
        checkNotNull(networkRequestStatusText);
        checkNotNull(mTVStatusText);

        mTVStatusText.setText(networkRequestStatusText);
    }

}

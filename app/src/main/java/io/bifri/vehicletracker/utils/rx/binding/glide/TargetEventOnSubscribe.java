package io.bifri.vehicletracker.utils.rx.binding.glide;

import android.content.Context;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.model.Marker;

import hugo.weaving.DebugLog;
import io.bifri.vehicletracker.adapter.UserVehiclesOnMapInfoWindowAdapter;
import rx.Emitter;
import rx.functions.Action1;

import static rx.android.MainThreadSubscription.verifyMainThread;

final class TargetEventOnSubscribe implements Action1<Emitter<TargetEvent>> {

    private static final String TAG = TargetEventOnSubscribe.class.getSimpleName();

    final Context context;
    final UserVehiclesOnMapInfoWindowAdapter adapter;
    final Marker marker;

    TargetEventOnSubscribe(Context context, UserVehiclesOnMapInfoWindowAdapter adapter, Marker marker) {
        this.context = context;
        this.adapter = adapter;
        this.marker = marker;
    }

    @Override public void call(Emitter<TargetEvent> emitter) {
        verifyMainThread();

        startImageLoader(emitter, marker);
    }

    @DebugLog
    /** Initiates loading the info window and makes sure the new image is used in case it changed */
    private void startImageLoader(Emitter<TargetEvent> emitter, Marker marker) {
        if (adapter.containsTarget(marker)) {
            Glide.clear(adapter.getTarget(marker)); // will do mImages.remove(marker) too
        } else {
            adapter.removeImage(marker);
        }
        startGlide(emitter, marker);
    }

    @DebugLog
    private void startGlide(Emitter<TargetEvent> emitter, Marker marker) {
        Glide.with(context)
                .load(adapter.imagePath(marker))
                .asBitmap()
                .dontAnimate()
                .into(getTarget(emitter, marker));
    }

    private InfoTarget getTarget(Emitter<TargetEvent> emitter, Marker marker) {
        InfoTarget target = adapter.getTarget(marker);
        if (target == null) {
            target = new InfoTarget(emitter, adapter, marker);
            adapter.putTarget(marker, target);
        } else {
            target.setEmitter(emitter);
        }
        return target;
    }

}

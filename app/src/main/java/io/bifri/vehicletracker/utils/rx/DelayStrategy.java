package io.bifri.vehicletracker.utils.rx;

/**
 * http://stackoverflow.com/questions/22066481/rxjava-can-i-use-retry-but-with-delay
 * https://github.com/ReactiveX/RxJava/issues/4207
 */
public enum DelayStrategy {
    CONSTANT_DELAY, 
    RETRY_COUNT, 
    CONSTANT_DELAY_TIMES_RETRY_COUNT, 
    CONSTANT_DELAY_RAISED_TO_RETRY_COUNT;
}

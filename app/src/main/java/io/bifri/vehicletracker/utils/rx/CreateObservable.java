package io.bifri.vehicletracker.utils.rx;


import android.support.annotation.NonNull;

import rx.Emitter;
import rx.Observable;
import rx.Subscription;
import rx.functions.Action0;

public class CreateObservable {

    @NonNull
    public static <T> Observable<T> from(@NonNull Observable<T> observable,
                                         @NonNull Action0 actionAfterSubscribe) {
        return Observable.create(emitter -> {
            Subscription subscription = observable.subscribe(
                    emitter::onNext,
                    emitter::onError,
                    emitter::onCompleted);
            emitter.setCancellation(subscription::unsubscribe);
            actionAfterSubscribe.call();
        }, Emitter.BackpressureMode.BUFFER);
    }

}

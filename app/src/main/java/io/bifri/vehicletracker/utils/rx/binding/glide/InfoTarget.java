package io.bifri.vehicletracker.utils.rx.binding.glide;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;

import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.android.gms.maps.model.Marker;

import java.lang.ref.WeakReference;

import hugo.weaving.DebugLog;
import io.bifri.vehicletracker.adapter.UserVehiclesOnMapInfoWindowAdapter;
import io.reark.reark.utils.Log;
import rx.Emitter;

import static io.bifri.vehicletracker.adapter.UserVehiclesOnMapInfoWindowAdapter.IMAGE_HEIGHT_IN_PIXELS;
import static io.bifri.vehicletracker.adapter.UserVehiclesOnMapInfoWindowAdapter.IMAGE_WIDTH_IN_PIXELS;

public class InfoTarget extends SimpleTarget<Bitmap> {

    private static final String TAG = InfoTarget.class.getSimpleName();

    private WeakReference<Emitter<TargetEvent>> weakEmitter;
    private UserVehiclesOnMapInfoWindowAdapter adapter;
    private Marker marker;

    InfoTarget(Emitter<TargetEvent> emitter, UserVehiclesOnMapInfoWindowAdapter adapter, Marker marker) {
        // otherwise Glide will load original sized bitmap which is huge
        super(IMAGE_WIDTH_IN_PIXELS, IMAGE_HEIGHT_IN_PIXELS);
        this.weakEmitter = new WeakReference<>(emitter);
        this.adapter = adapter;
        this.marker = marker;
    }

    void setEmitter(Emitter<TargetEvent> emitter) {
        this.weakEmitter = new WeakReference<>(emitter);
    }

    @Override
    public void onLoadFailed(Exception e, Drawable errorDrawable) {
        adapter.removeImage(marker); // clean up previous image, it became invalid
        Emitter<TargetEvent> emitter = weakEmitter.get();
        if (emitter != null) {
            emitter.onNext(TargetEvent.create(marker, TargetEvent.Kind.LOAD_FAILED));
        }
    }

    @DebugLog
    @Override
    public void onLoadCleared(Drawable placeholder) {
        adapter.removeImage(marker); // clean up previous image, it became invalid
        // don't call marker.showInfoWindow() to update because this is most likely called from Glide.into()
        Emitter<TargetEvent> emitter = weakEmitter.get();
        if (emitter != null) {
            emitter.onNext(TargetEvent.create(marker, TargetEvent.Kind.LOAD_CLEARED));
        }
    }

    @DebugLog
    @Override
    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
        // this prevents recursion, because Glide load only starts if image == null in getInfoWindow
        adapter.putImage(marker, resource);
        Emitter<TargetEvent> emitter = weakEmitter.get();
        if (emitter != null) {
            Log.d(TAG, "emitter.onNext(TargetEvent.create(marker, TargetEvent.Kind.RESOURCE_READY));");
            emitter.onNext(TargetEvent.create(marker, TargetEvent.Kind.RESOURCE_READY));
        }
    }
}

package io.bifri.vehicletracker.utils.rx.binding.googlemaps;

import android.view.View;

import com.google.android.gms.maps.GoogleMap;

import rx.Observable;

import static com.jakewharton.rxbinding.view.RxView.globalLayouts;
import static rx.android.MainThreadSubscription.verifyMainThread;

final class MapAndViewReadyTransformer implements Observable.Transformer<GoogleMap, GoogleMap> {
    final View mapView;

    MapAndViewReadyTransformer(View mapView) {
        this.mapView = mapView;
    }

    @Override public Observable<GoogleMap> call(Observable<GoogleMap> mapReady) {
        verifyMainThread();

        // An observable that emits when both the GoogleMap
        // and the View are initialized.
        return Observable.zip(
                mapReady.take(1),
                viewCompletedLayoutSuccessfullyOrGlobalLayout(),
                (googleMap, __) -> googleMap);
    }

    private Observable<Void> viewCompletedLayoutSuccessfully() {
        return Observable.fromCallable(
                () -> mapView.getWidth() != 0 && mapView.getHeight() != 0)
                .filter(viewCompletedLayout -> viewCompletedLayout)
                .map(__ -> null);
    }

    private Observable<Void> viewCompletedLayoutSuccessfullyOrGlobalLayout() {
        return viewCompletedLayoutSuccessfully()
                // Map has not undergone layout, register a View observer.
                .concatWith(globalLayouts(mapView))
                .take(1);
    }

}

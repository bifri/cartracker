package io.bifri.vehicletracker.utils.rx;

import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CopyOnWriteArrayList;

import rx.Emitter;
import rx.Observable;
import rx.Scheduler;
import rx.functions.Func1;

/**
 * Modified version of konmik RxState
 * (added BackpressureMode argument to Observable.create()):
 * See <a href="https://github.com/konmik/rxstate">https://github.com/konmik/rxstate</a>,
 * <a href="http://konmik.com/post/managing_state_reactive_way/">http://konmik.com/post/managing_state_reactive_way/</a>
 */
@SuppressWarnings("Duplicates")
public class RxState<T> {

    private final Scheduler scheduler;
    private final ConcurrentLinkedQueue<Entry<T>> queue = new ConcurrentLinkedQueue<>();
    private final List<Emitter<T>> emitters = new CopyOnWriteArrayList<>();

    private volatile T value;
    private volatile boolean emitting;

    public RxState(T initialValue, Scheduler scheduler) {
        this.value = initialValue;
        this.scheduler = scheduler;
    }

    /**
     * Applies a function to the current value, emitting the new returned value.
     * <p>
     * The function should fast to take less synchronized time.
     */
    public void apply(Func1<T, T> func) {

        synchronized (this) {
            value = func.call(value);
            for (Emitter<T> emitter : emitters) {
                queue.add(new Entry<>(emitter, value));
            }
        }

        emit();
    }

    /**
     * Observable of sequential value changes.
     */
    public Observable<T> values(StartWith startWith) {
        return Observable.create(emitter -> {
                    if (startWith == StartWith.IMMEDIATE) {
                        onSubscribeImmediate(emitter);
                    } else if (startWith == StartWith.SCHEDULE) {
                        onSubscribeSchedule(emitter);
                    } else {
                        onSubscribeNo(emitter);
                    }
                },
                Emitter.BackpressureMode.BUFFER);
    }

    /**
     * A check if there are values to be emitted.
     */
    public boolean isEmitting() {
        synchronized (this) {
            return emitting || !queue.isEmpty();
        }
    }

    /**
     * Use this function only if you guarantee that no other functions are trying to modify the value
     * the same time, otherwise you're asking for race conditions.
     * <p>
     * 1. RxValue can be still emitting previous values on the scheduler.
     * 2. Concurrent {@link #apply(Func1)} can be called.
     */
    public T value() {
        return value;
    }

    private void onSubscribeNo(Emitter< T> emitter) {
        synchronized (this) {
            emitters.add(emitter);
        }
        setCancellation(emitter);
    }

    private void onSubscribeSchedule(Emitter<T> emitter) {
        synchronized (this) {
            emitters.add(emitter);
            queue.add(new Entry<>(emitter, value));
        }
        setCancellation(emitter);
        emit();
    }

    private void onSubscribeImmediate(Emitter<T> emitter) {
        T emit;
        synchronized (this) {
            emit = value;
        }
        emitter.onNext(emit);
        synchronized (this) {
            emitters.add(emitter);
        }
        setCancellation(emitter);
    }

    private void setCancellation(Emitter<T> emitter) {
        emitter.setCancellation(() -> {
            synchronized (this) {
                emitters.remove(emitter);
            }
        });
    }

    private void emit() {
        Scheduler.Worker worker = scheduler.createWorker();
        worker.schedule(() -> {
            emitLoop();
            worker.unsubscribe();
        });
    }

    private void emitLoop() {

        synchronized (this) {
            if (emitting) {
                return;
            }
            emitting = true;
        }

        for (; ; ) {
            Entry<T> next;
            synchronized (this) {
                if (queue.isEmpty()) {
                    emitting = false;
                    return;
                }
                next = queue.poll();
            }

            next.emitter.onNext(next.value);
        }
    }

    private static class Entry<T> {
        final Emitter<T> emitter;
        final T value;

        private Entry(Emitter<T> emitter, T value) {
            this.emitter = emitter;
            this.value = value;
        }
    }

}
package io.bifri.vehicletracker.utils.rx.binding.googlemaps;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;

import rx.Emitter;
import rx.functions.Action1;

import static rx.android.MainThreadSubscription.verifyMainThread;

final class MapFragmentMapReadyOnSubscribe implements Action1<Emitter<GoogleMap>> {
  final MapFragment mapFragment;

  MapFragmentMapReadyOnSubscribe(MapFragment mapFragment) {
    this.mapFragment = mapFragment;
  }

  @Override public void call(Emitter<GoogleMap> emitter) {
    verifyMainThread();

    OnMapReadyCallback callback = new OnMapReadyCallback() {
      @Override public void onMapReady(GoogleMap googleMap) {
        emitter.onNext(googleMap);
      }
    };

    mapFragment.getMapAsync(callback);
  }
}

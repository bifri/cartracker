package io.bifri.vehicletracker.utils;


import android.graphics.Color;
import android.support.annotation.ColorInt;
import android.support.annotation.Size;

import io.reark.reark.utils.Log;

public class ColorUtils {

    private static final String TAG = ColorUtils.class.getSimpleName();

    @ColorInt
    public static int parseColor(@Size(min=1) String colorString) {
        return parseColor(colorString, Color.TRANSPARENT);
    }

    @ColorInt
    public static int parseColor(@Size(min=1) String colorString, @ColorInt int defaultColor) {
        try {
            return Color.parseColor(colorString);
        } catch (IllegalArgumentException e) {
            Log.e(TAG, "Failed to parse color: " + colorString);
            return defaultColor;
        }

    }

}

package io.bifri.vehicletracker.utils.rx.binding.glide;

import android.support.annotation.CheckResult;
import android.support.annotation.NonNull;

import com.google.android.gms.maps.model.Marker;

/**
 * A Glide target event.
 * <p>
 * <strong>Warning:</strong> Instances keep a strong reference to the marker.
 */
public final class TargetEvent {
    public enum Kind {
        LOAD_CLEARED, RESOURCE_READY, LOAD_FAILED
    }

    @CheckResult
    @NonNull
    public static TargetEvent create(@NonNull Marker marker, @NonNull Kind kind) {
        return new TargetEvent(marker, kind);
    }

    private final Marker marker;
    private final Kind kind;

    private TargetEvent(@NonNull Marker marker, @NonNull Kind kind) {
        this.marker = marker;
        this.kind = kind;
    }

    @NonNull
    public Marker marker() {
        return marker;
    }

    @NonNull
    public Kind kind() {
        return kind;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TargetEvent that = (TargetEvent) o;

        //noinspection SimplifiableIfStatement
        if (!marker.equals(that.marker)) return false;
        return kind == that.kind;
    }

    @Override
    public int hashCode() {
        int result = marker.hashCode();
        result = 31 * result + kind.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "TargetEvent{" +
                "marker=" + marker +
                ", kind=" + kind +
                '}';
    }
}

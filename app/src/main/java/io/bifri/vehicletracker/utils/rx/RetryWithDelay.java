package io.bifri.vehicletracker.utils.rx;

import java.util.concurrent.TimeUnit;

import io.reark.reark.utils.Log;
import rx.Observable;
import rx.functions.Func1;

/**
 * http://stackoverflow.com/questions/22066481/rxjava-can-i-use-retry-but-with-delay
 * https://github.com/ReactiveX/RxJava/issues/4207
 */
public class RetryWithDelay implements Func1<Observable<? extends Throwable>, Observable<?>> {

    private static final String TAG = RepeatWithDelay.class.getSimpleName();

    private static final int INFINITE_RETRY = -1;

    private final int maxRetries;
    private final long retryDelayMillis;
    private final DelayStrategy delayStrategy;

    private int retryCount;

    public RetryWithDelay(long retryDelayMillis, DelayStrategy delayStrategy) {
        this(INFINITE_RETRY, retryDelayMillis, delayStrategy);
    }

    public RetryWithDelay(int maxRetries, long retryDelayMillis, DelayStrategy delayStrategy) {
        this.maxRetries = maxRetries;
        this.retryDelayMillis = retryDelayMillis;
        this.delayStrategy = delayStrategy;
    }

    @Override
    public Observable<?> call(Observable<? extends Throwable> errors) {
        return errors
                .concatMap(throwable -> {
                    Log.i(TAG, "concatMap()");
                    ++retryCount;
                    if (maxRetries == INFINITE_RETRY || retryCount <= maxRetries) {
                        // When this Observable calls onNext, the original
                        // Observable will be retried (i.e. resubscribed).
                        return Observable.timer(delayMillis(), TimeUnit.MILLISECONDS);
                    }
                    return Observable.error(throwable);
                });
    }

    private long delayMillis() {
        switch (delayStrategy) {
        case CONSTANT_DELAY:
            return retryDelayMillis;
        case RETRY_COUNT:
            return retryCount;
        case CONSTANT_DELAY_TIMES_RETRY_COUNT:
            return retryDelayMillis * retryCount;
        case CONSTANT_DELAY_RAISED_TO_RETRY_COUNT:
            return (long) Math.pow(retryDelayMillis, retryCount);
        default:
            return 0;
        }
    }
}
package io.bifri.vehicletracker.utils.rx.binding.glide;

import android.content.Context;
import android.support.annotation.CheckResult;
import android.support.annotation.NonNull;

import com.google.android.gms.maps.model.Marker;

import hugo.weaving.DebugLog;
import io.bifri.vehicletracker.adapter.UserVehiclesOnMapInfoWindowAdapter;
import rx.Emitter.BackpressureMode;
import rx.Observable;

import static io.reark.reark.utils.Preconditions.checkNotNull;

public class RxGlide {

    /**
     * Create an observable of Glide target events.
     * <p>
     * <em>Warning:</em> The created observable keeps a strong reference to {@code context}.
     * Unsubscribe to free this reference.
     */
    @DebugLog
    @CheckResult @NonNull
    public static Observable<TargetEvent> targetEvents(
            @NonNull Context context,
            @NonNull UserVehiclesOnMapInfoWindowAdapter adapter,
            @NonNull Marker marker) {

        checkNotNull(context, "context == null");
        checkNotNull(adapter, "adapter == null");
        checkNotNull(marker, "marker == null");
        return Observable.create(
                new TargetEventOnSubscribe(context, adapter, marker),
                BackpressureMode.BUFFER);
    }
}

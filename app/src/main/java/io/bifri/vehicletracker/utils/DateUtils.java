package io.bifri.vehicletracker.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateUtils {

    public static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd", Locale.ROOT);

    public static final Date EMPTY_DATE = new Date(0);

    private DateUtils() {}

    public static Date value(Date date) {
        return date != null ? date : new Date();
    }

    public static long toDbValue(Date date) {
        return date != null
                ? date.getTime()
                : 0;
    }

    public static Date fromDbValue(long value) {
        return value > 0
                ? new Date(value)
                : null;
    }

    public static boolean isLater(Date first, Date second) {
        if (first == null) {
            return false;
        } else if (second == null) {
            return true;
        } else {
            return first.compareTo(second) > 0;
        }
    }
}

package io.bifri.vehicletracker.utils.rx.binding.googlemaps;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;

import rx.Emitter;
import rx.functions.Action1;

import static rx.android.MainThreadSubscription.verifyMainThread;

final class MapViewMapReadyOnSubscribe implements Action1<Emitter<GoogleMap>> {
  final MapView mapView;

  MapViewMapReadyOnSubscribe(MapView mapView) {
    this.mapView = mapView;
  }

  @Override public void call(Emitter<GoogleMap> emitter) {
    verifyMainThread();

    OnMapReadyCallback callback = new OnMapReadyCallback() {
      @Override public void onMapReady(GoogleMap googleMap) {
        emitter.onNext(googleMap);
      }
    };

    mapView.getMapAsync(callback);
  }
}

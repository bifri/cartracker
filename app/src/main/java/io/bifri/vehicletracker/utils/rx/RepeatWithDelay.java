package io.bifri.vehicletracker.utils.rx;

import java.util.concurrent.TimeUnit;

import io.reark.reark.utils.Log;
import rx.Observable;
import rx.functions.Func1;
import rx.subjects.BehaviorSubject;
import rx.subjects.Subject;

public class RepeatWithDelay implements Func1<Observable<? extends Void>, Observable<?>> {

    private static final String TAG = RepeatWithDelay.class.getSimpleName();

    private static final int INFINITE_REPEAT = -1;

    private final int maxRepeats;
    private final long repeatDelayMillis;
    private final DelayStrategy repeatDelayStrategy;

    private int repeatCount;

    public RepeatWithDelay(long repeatDelayMillis, DelayStrategy repeatDelayStrategy) {
        this(INFINITE_REPEAT, repeatDelayMillis, repeatDelayStrategy);
    }

    public RepeatWithDelay(int maxRepeats,
                           long repeatDelayMillis,
                           DelayStrategy repeatDelayStrategy) {
        this.maxRepeats = maxRepeats;
        this.repeatDelayMillis = repeatDelayMillis;
        this.repeatDelayStrategy = repeatDelayStrategy;
    }

    @Override
    public Observable<?> call(Observable<? extends Void> completed) {
        final Subject<Void, Void> subject = BehaviorSubject.create();

        return completed
                .concatMap(completed1 -> {
                    Log.i(TAG, "concatMap()");
                    ++repeatCount;
                    if (maxRepeats == INFINITE_REPEAT || repeatCount <= maxRepeats) {
                        // When this Observable calls onNext, the original
                        // Observable will be repeated (i.e. resubscribed).
                        return Observable.timer(delayMillis(), TimeUnit.MILLISECONDS);
                    }
                    subject.onNext(null);
                    return Observable.never();
                })
                .takeUntil(subject);
    }

    private long delayMillis() {
        switch (repeatDelayStrategy) {
            case CONSTANT_DELAY:
                return repeatDelayMillis;
            case RETRY_COUNT:
                return repeatCount;
            case CONSTANT_DELAY_TIMES_RETRY_COUNT:
                return repeatDelayMillis * repeatCount;
            case CONSTANT_DELAY_RAISED_TO_RETRY_COUNT:
                return (long) Math.pow(repeatDelayMillis, repeatCount);
            default:
                return 0;
        }
    }
}
/*
 * The MIT License
 *
 * Copyright (c) 2013-2016 reark project contributors
 *
 * https://github.com/reark/reark/graphs/contributors
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package io.bifri.vehicletracker.utils.glide;

import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;

import com.bumptech.glide.request.Request;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SizeReadyCallback;
import com.bumptech.glide.request.target.Target;

import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;

import static io.reark.reark.utils.Preconditions.checkNotNull;

public class SerialTarget<T> implements Target<T> {

    private volatile State<T> mState = new State<>(NullTarget.empty());

    private static final AtomicReferenceFieldUpdater<SerialTarget, State> STATE_UPDATER
            = AtomicReferenceFieldUpdater.newUpdater(SerialTarget.class, State.class, "mState");

    private static final class State<T> {

        final Target<T> target;

        State(Target<T> t) {
            target = t;
        }

        State<T> set(@NonNull final Target<T> t) {
            return new State<>(t);
        }
    }

    public void set(@NonNull final Target<T> s) {
        checkNotNull(s);

        State<T> oldState;
        State<T> newState;

        do {
            oldState = mState;
            newState = oldState.set(s);
        } while (!STATE_UPDATER.compareAndSet(this, oldState, newState));

        oldState.target.onDestroy();
    }

    public Target<T> get() {
        return mState.target;
    }

    @Override
    public void onLoadStarted(Drawable placeholder) {
        mState.target.onLoadStarted(placeholder);
    }

    @Override
    public void onLoadFailed(Exception e, Drawable errorDrawable) {
        mState.target.onLoadFailed(e, errorDrawable);
    }

    @Override
    public void onResourceReady(T resource, GlideAnimation<? super T> glideAnimation) {
        mState.target.onResourceReady(resource, glideAnimation);
    }

    @Override
    public void onLoadCleared(Drawable placeholder) {
        mState.target.onLoadCleared(placeholder);
    }

    @Override
    public void getSize(SizeReadyCallback cb) {
        mState.target.getSize(cb);
    }

    @Override
    public void setRequest(Request request) {
        mState.target.setRequest(request);
    }

    @Override
    public Request getRequest() {
        return mState.target.getRequest();
    }

    @Override
    public void onStart() {
        mState.target.onStart();
    }

    @Override
    public void onStop() {
        mState.target.onStop();
    }

    @Override
    public void onDestroy() {
        mState.target.onDestroy();
    }

}

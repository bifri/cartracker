package io.bifri.vehicletracker.utils.rx.binding.googlemaps;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;

import rx.Emitter;
import rx.functions.Action1;

import static rx.android.MainThreadSubscription.verifyMainThread;

final class SupportMapFragmentMapReadyOnSubscribe implements Action1<Emitter<GoogleMap>> {
  final SupportMapFragment supportMapFragment;

  SupportMapFragmentMapReadyOnSubscribe(SupportMapFragment supportMapFragment) {
    this.supportMapFragment = supportMapFragment;
  }

  @Override public void call(Emitter<GoogleMap> emitter) {
    verifyMainThread();

    OnMapReadyCallback callback = new OnMapReadyCallback() {
      @Override public void onMapReady(GoogleMap googleMap) {
        emitter.onNext(googleMap);
      }
    };

    supportMapFragment.getMapAsync(callback);
  }
}

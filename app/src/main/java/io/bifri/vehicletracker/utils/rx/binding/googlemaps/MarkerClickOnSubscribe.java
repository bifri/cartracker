package io.bifri.vehicletracker.utils.rx.binding.googlemaps;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

import rx.Emitter;
import rx.functions.Action1;
import rx.functions.Cancellable;
import rx.functions.Func1;

import static rx.android.MainThreadSubscription.verifyMainThread;

final class MarkerClickOnSubscribe implements Action1<Emitter<Marker>> {
  final GoogleMap googleMap;
  final Func1<? super Marker, Boolean> handled;

  MarkerClickOnSubscribe(GoogleMap googleMap, Func1<? super Marker, Boolean> handled) {
    this.googleMap = googleMap;
    this.handled = handled;
  }

  @Override public void call(Emitter<Marker> emitter) {
    verifyMainThread();

    GoogleMap.OnMarkerClickListener listener = new GoogleMap.OnMarkerClickListener() {
      @Override public boolean onMarkerClick(Marker marker) {
        emitter.onNext(marker);
        return handled.call(marker);
      }
    };

    emitter.setCancellation(new Cancellable() {
      @Override
      public void cancel() throws Exception {
        googleMap.setOnMarkerClickListener(null);
      }

    });

    googleMap.setOnMarkerClickListener(listener);
  }
}

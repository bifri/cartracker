/*
 * The MIT License
 *
 * Copyright (c) 2013-2016 reark project contributors
 *
 * https://github.com/reark/reark/graphs/contributors
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package io.bifri.vehicletracker.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import javax.inject.Inject;

import io.bifri.vehicletracker.App;
import io.bifri.vehicletracker.R;
import io.bifri.vehicletracker.activities.VehicleUserDetailsActivity;
import io.bifri.vehicletracker.utils.ApplicationInstrumentation;
import io.bifri.vehicletracker.view.VehicleUserView;
import io.bifri.vehicletracker.view.VehicleUserViewBinder;
import io.bifri.vehicletracker.viewmodels.VehicleUserViewModel;

public class VehicleUserFragment extends Fragment {

    @Inject
    VehicleUserViewModel mViewModel;

    @Inject
    ApplicationInstrumentation mInstrumentation;

    private VehicleUserView mVehicleUserView;
    private VehicleUserViewBinder mViewBinder;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        App.getInstance().getGraph().inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.vehicle_user_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mVehicleUserView = new VehicleUserView(getContext(), view);
        mViewBinder = new VehicleUserViewBinder(mVehicleUserView, mViewModel);
        mViewModel.subscribeToDataStore();

        view.findViewById(R.id.vehicle_user_fragment_choose_vehicle_user_button)
                .setOnClickListener(e ->
                        ((VehicleUserDetailsActivity) getActivity()).chooseVehicleUser());
    }

    @Override
    public void onResume() {
        super.onResume();
        mViewBinder.bind();
    }

    @Override
    public void onPause() {
        mViewBinder.unbind();
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        mViewModel.unsubscribeFromDataStore();
        mVehicleUserView.onDestroyView();
        mVehicleUserView = null;
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        mViewModel.dispose();
        mInstrumentation.getLeakTracing().traceLeakage(this);
        super.onDestroy();
    }

}

package io.bifri.vehicletracker.adapter.viewholder;

import android.graphics.Bitmap;
import android.location.Address;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.model.Marker;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.bifri.vehicletracker.R;
import io.bifri.vehicletracker.utils.ColorUtils;

public class VehicleMarkerViewHolder {

    private final View mRootView;
    @BindView(R.id.badge) ImageView mIVBadge;
    @BindView(R.id.title) TextView mTVTitle;
    @BindView(R.id.snippet) TextView mTVAddress;
    @BindView(R.id.vehicle_color) View mVVehicleColor;

    public VehicleMarkerViewHolder(View view) {
        mRootView = view;
        ButterKnife.bind(this, view);
    }

    public View rootView() {
        return mRootView;
    }

    public ImageView ivBadge() {
        return mIVBadge;
    }

    public TextView tvTitle() {
        return mTVTitle;
    }

    public TextView tvAddress() {
        return mTVAddress;
    }

    public View vVehicleColor() {
        return mVVehicleColor;
    }

    public void setVehicleDetails(Marker marker,
                                  Bitmap image,
                                  Address address,
                                  String colorStr) {
        if (image != null) {
            mIVBadge.setVisibility(View.VISIBLE);
            mIVBadge.setImageBitmap(image);
        } else {
            mIVBadge.setVisibility(View.INVISIBLE);
        }

        mTVTitle.setText(marker.getTitle());
        String addressStr = "";
        if (address != null && address.getMaxAddressLineIndex() > 0) {
            addressStr = address.getAddressLine(0);
        }
        mTVAddress.setText(addressStr);
        mVVehicleColor.setBackgroundColor(ColorUtils.parseColor(colorStr));
    }

}

package io.bifri.vehicletracker.adapter;

import android.support.annotation.NonNull;
import android.support.v4.util.Pair;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.bifri.vehicletracker.pojo.response.Vehicle;
import io.bifri.vehicletracker.pojo.response.VehicleLocation;
import java8.util.stream.StreamSupport;

import static io.bifri.vehicletracker.utils.StringUtils.value;

public class GoogleMapMarkersAdapter {

    @NonNull private final GoogleMap mMap;

    public GoogleMapMarkersAdapter(@NonNull GoogleMap map) {
        mMap = map;
    }

    public @NonNull Map<Marker, Pair<Vehicle, VehicleLocation>> setMarkers(
            @NonNull List<Pair<Vehicle, VehicleLocation>> pairs) {

        mMap.clear();

        final Map<Marker, Pair<Vehicle, VehicleLocation>> markerToVehicle = new HashMap<>();
        final Map<Pair<Vehicle, VehicleLocation>, MarkerOptions> vehicleToMarkerOptions = new HashMap<>();

        LatLngBounds.Builder boundsBuilder = new LatLngBounds.Builder();
        StreamSupport.stream(pairs)
                .forEach(pair -> {
                    MarkerOptions options = bind(pair);
                    vehicleToMarkerOptions.put(pair, options);
                    boundsBuilder.include(options.getPosition());
                });
        LatLngBounds bounds = boundsBuilder.build();
        // Set the camera to the greatest possible zoom level that
        // includes the bounds
        mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 200));

        // Add markers
        StreamSupport.stream(pairs)
                .forEach(pair -> {
                    Marker marker = mMap.addMarker(vehicleToMarkerOptions.get(pair));
                    markerToVehicle.put(marker, pair);
                });
        return markerToVehicle;
    }

    private MarkerOptions bind(Pair<Vehicle, VehicleLocation> pair) {
        Vehicle vehicle = pair.first;
        VehicleLocation vehicleLocation = pair.second;
        return new MarkerOptions()
                .position(new LatLng(vehicleLocation.getLat(), vehicleLocation.getLon()))
                .title(value(vehicle.getMake()) + " " + value(vehicle.getModel())
                        + " " + value(vehicle.getYear()))
                .snippet("")
                .flat(true);
    }

}

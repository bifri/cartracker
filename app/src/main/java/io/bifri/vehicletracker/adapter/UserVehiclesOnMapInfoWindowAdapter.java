package io.bifri.vehicletracker.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.location.Address;
import android.support.annotation.Nullable;
import android.support.v4.util.Pair;
import android.view.LayoutInflater;
import android.view.View;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import io.bifri.vehicletracker.R;
import io.bifri.vehicletracker.adapter.viewholder.VehicleMarkerViewHolder;
import io.bifri.vehicletracker.pojo.response.Vehicle;
import io.bifri.vehicletracker.pojo.response.VehicleLocation;
import io.bifri.vehicletracker.utils.rx.binding.glide.InfoTarget;
import java8.util.stream.StreamSupport;

public class UserVehiclesOnMapInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

    private static final String TAG = UserVehiclesOnMapInfoWindowAdapter.class.getSimpleName();

    public static final int IMAGE_WIDTH_IN_PIXELS = 128;
    public static final int IMAGE_HEIGHT_IN_PIXELS = 128;

    private final Map<Marker, Pair<Vehicle, VehicleLocation>> mItems = new HashMap<>();
    private final Map<Marker, Bitmap> mImages = new HashMap<>();
    private final Map<Marker, InfoTarget> mTargets = new HashMap<>();
    private Address mAddress;

    private final View mWindow;
    private final VehicleMarkerViewHolder mMarkerVH;

    public UserVehiclesOnMapInfoWindowAdapter(Context context) {
        mWindow = LayoutInflater.from(context).inflate(R.layout.custom_info_window, null);
        mMarkerVH = new VehicleMarkerViewHolder(mWindow);
    }

    public void set(Map<Marker, Pair<Vehicle, VehicleLocation>> pairs) {
        clear();
        mItems.putAll(pairs);
    }

    public void clear() {
        StreamSupport.stream(new HashSet<>(mItems.keySet())).forEach(this::remove);
    }

    /** Use this to discard a marker to make sure all resources are freed and not leaked */
    public void remove(Marker marker) {
        mItems.remove(marker);
        if (mTargets.containsKey(marker)) {
            Glide.clear(mTargets.remove(marker)); // will do mImages.remove(marker) too
        } else {
            mImages.remove(marker);
        }
        marker.remove();
    }

    @Override
    public View getInfoContents(Marker marker) {
        return null;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        bindView(marker, mImages.get(marker));
        return mWindow;
    }

    private void bindView(Marker marker, Bitmap image) {
        mMarkerVH.setVehicleDetails(
                marker,
                image,
                mAddress,
                mItems.get(marker).first.getColor());
    }

    public @Nullable String imagePath(Marker marker) {
        return mItems.get(marker).first.getPhoto();
    }

    public boolean containsTarget(Marker marker) {
        return mTargets.containsKey(marker);
    }

    public InfoTarget getTarget(Marker marker) {
        return mTargets.get(marker);
    }

    public void putTarget(Marker marker, InfoTarget target) {
        mTargets.put(marker, target);
    }

    public void putImage(Marker marker, Bitmap bitmap) {
        mImages.put(marker, bitmap);
    }

    public void removeImage(Marker marker) {
        mImages.remove(marker);
    }

    public void setAddress(Address address) {
        mAddress = address;
    }

}

package io.bifri.vehicletracker.adapter.viewholder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.bifri.vehicletracker.R;

public class VehicleUserViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.iv_photo) ImageView mIVPhoto;
    @BindView(R.id.tv_title) TextView mTVTitle;

    public VehicleUserViewHolder(View view, OnClickListener onClickListener) {
        super(view);
        ButterKnife.bind(this, view);
        view.setOnClickListener(onClickListener);
    }

    @NonNull
    public View rootView() {
        return itemView;
    }

    @NonNull
    public ImageView ivPhoto() {
        return mIVPhoto;
    }

    @NonNull
    public TextView tvTitle() {
        return mTVTitle;
    }
}

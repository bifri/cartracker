/*
 * The MIT License
 *
 * Copyright (c) 2013-2016 reark project contributors
 *
 * https://github.com/reark/reark/graphs/contributors
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package io.bifri.vehicletracker.adapter;

import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import io.bifri.vehicletracker.R;
import io.bifri.vehicletracker.adapter.viewholder.VehicleUserViewHolder;
import io.bifri.vehicletracker.pojo.VehicleUser;
import io.bifri.vehicletracker.pojo.response.Owner;

import static android.text.TextUtils.isEmpty;
import static android.view.View.OnClickListener;
import static io.bifri.vehicletracker.utils.StringUtils.value;

public class VehicleUsersAdapter extends Adapter<VehicleUserViewHolder> {

    private final List<VehicleUser> mVehicleUsers = new ArrayList<>();
    private OnClickListener mOnClickListener;

    public VehicleUsersAdapter(List<VehicleUser> vehicleUsers) {
        mVehicleUsers.addAll(vehicleUsers);
    }

    public void setOnClickListener(OnClickListener onClickListener) {
        mOnClickListener = onClickListener;
    }

    public VehicleUser getItem(int position) {
        return mVehicleUsers.get(position);
    }

    @Override
    public VehicleUserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.vehicle_user_item, parent, false);
        return new VehicleUserViewHolder(v, mOnClickListener);
    }

    @Override
    public void onBindViewHolder(VehicleUserViewHolder holder, int position) {
        Owner owner = mVehicleUsers.get(position).getOwner();
        if (owner == null) {
            owner = Owner.empty();
        }
        holder.tvTitle().setText(value(owner.getName()) + " " + value(owner.getSurname()));
        if (!isEmpty(owner.getPhoto())) {
            Glide.with(holder.ivPhoto().getContext())
                    .load(owner.getPhoto())
                    .fitCenter()
                    .into(holder.ivPhoto());
        }
    }

    @Override
    public int getItemCount() {
        return mVehicleUsers.size();
    }

    public void set(List<VehicleUser> vehicleUsers) {
        int prevSize = mVehicleUsers.size();
        this.mVehicleUsers.clear();
        this.mVehicleUsers.addAll(vehicleUsers);
        notifyItemRangeRemoved(0, prevSize);
        notifyItemRangeInserted(0, vehicleUsers.size());
    }

}

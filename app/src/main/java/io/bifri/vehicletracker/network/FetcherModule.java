/*
 * The MIT License
 *
 * Copyright (c) 2013-2016 reark project contributors
 *
 * https://github.com/reark/reark/graphs/contributors
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package io.bifri.vehicletracker.network;

import android.net.Uri;

import java.util.Arrays;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import io.bifri.vehicletracker.data.stores.NetworkRequestStatusStore;
import io.bifri.vehicletracker.data.stores.UserVehiclesLocationStore;
import io.bifri.vehicletracker.data.stores.VehicleLocationStore;
import io.bifri.vehicletracker.data.stores.VehicleStore;
import io.bifri.vehicletracker.data.stores.VehicleUserStore;
import io.bifri.vehicletracker.data.stores.VehicleUsersStore;
import io.bifri.vehicletracker.data.stores.VehiclesStore;
import io.bifri.vehicletracker.network.fetchers.UserVehiclesLocationFetcher;
import io.bifri.vehicletracker.network.fetchers.VehicleUserFetcher;
import io.bifri.vehicletracker.network.fetchers.VehicleUsersFetcher;
import io.reark.reark.network.fetchers.Fetcher;
import io.reark.reark.network.fetchers.UriFetcherManager;

@Module(includes = NetworkModule.class)
public final class FetcherModule {

    @Provides
    @Named("vehicleUser")
    public Fetcher<Uri> provideVehicleUserFetcher(
            NetworkApi networkApi,
            NetworkRequestStatusStore networkRequestStatusStore,
            VehicleUserStore vehicleUserStore) {
        return new VehicleUserFetcher(
                networkApi,
                networkRequestStatusStore::put,
                vehicleUserStore);
    }

    @Provides
    @Named("vehicleUsers")
    public Fetcher<Uri> provideVehicleUsersFetcher(
            NetworkApi networkApi,
            NetworkRequestStatusStore networkRequestStatusStore,
            VehicleStore vehicleStore,
            VehiclesStore vehiclesStore,
            VehicleUserStore vehicleUserStore,
            VehicleUsersStore vehicleUsersStore) {
        return new VehicleUsersFetcher(
                networkApi,
                networkRequestStatusStore::put,
                vehicleStore,
                vehiclesStore,
                vehicleUserStore,
                vehicleUsersStore);
    }

    @Provides
    @Named("userVehiclesLocation")
    public Fetcher<Uri> provideUserVehiclesLocationFetcher(
            NetworkApi networkApi,
            NetworkRequestStatusStore networkRequestStatusStore,
            VehicleLocationStore vehicleLocationStore,
            UserVehiclesLocationStore userVehiclesLocationStore) {
        return new UserVehiclesLocationFetcher(
                networkApi,
                networkRequestStatusStore::put,
                vehicleLocationStore,
                userVehiclesLocationStore);
    }

    @Provides
    public UriFetcherManager provideUriFetcherManager(
            @Named("vehicleUser") Fetcher<Uri> vehicleUserFetcher,
            @Named("vehicleUsers") Fetcher<Uri> vehicleUsersFetcher,
            @Named("userVehiclesLocation") Fetcher<Uri> userVehiclesLocationFetcher) {
        return new UriFetcherManager.Builder()
                .fetchers(Arrays.asList(
                        vehicleUserFetcher,
                        vehicleUsersFetcher,
                        userVehiclesLocationFetcher))
                .build();
    }

}

package io.bifri.vehicletracker.network;

import io.bifri.vehicletracker.pojo.response.Users;
import io.bifri.vehicletracker.pojo.response.VehicleLocations;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

public interface VehicleUserService {

    String BASE_URL = "http://mobi.connectedcar360.net/api/";

    String USERS_OPERATION = "list";
    String USER_VEHICLES_LOCATION_OPERATION = "getlocations";


    @GET("./")
    Observable<Users> users(@Query("op") String operation);

    @GET("./")
    Observable<VehicleLocations> userVehiclesLocation(
            @Query("op") String operation,
            @Query("userid") int userId);

}

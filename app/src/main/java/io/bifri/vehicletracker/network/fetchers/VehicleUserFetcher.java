/*
 * The MIT License
 *
 * Copyright (c) 2013-2016 reark project contributors
 *
 * https://github.com/reark/reark/graphs/contributors
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package io.bifri.vehicletracker.network.fetchers;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;

import java.util.Date;

import io.bifri.vehicletracker.network.NetworkApi;
import io.bifri.vehicletracker.network.NetworkService;
import io.bifri.vehicletracker.network.ServiceUri;
import io.bifri.vehicletracker.pojo.VehicleUser;
import io.reark.reark.data.stores.interfaces.StorePutInterface;
import io.reark.reark.pojo.NetworkRequestStatus;
import io.reark.reark.utils.Log;
import rx.Observable;
import rx.Subscription;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

import static io.reark.reark.utils.Preconditions.checkNotNull;

public class VehicleUserFetcher extends AppFetcherBase<Uri> {

    private static final String TAG = VehicleUserFetcher.class.getSimpleName();

    @NonNull
    private final StorePutInterface<VehicleUser, Integer> mVehicleUserStore;

    public VehicleUserFetcher(@NonNull final NetworkApi networkApi,
                          @NonNull final Action1<NetworkRequestStatus> updateNetworkRequestStatus,
                          @NonNull final StorePutInterface<VehicleUser, Integer> vehicleUserStore) {
        super(networkApi, updateNetworkRequestStatus);

        checkNotNull(vehicleUserStore);

        this.mVehicleUserStore = vehicleUserStore;
    }

    @Override
    public void fetch(@NonNull final Intent intent) {
        checkNotNull(intent);

        final int vehicleUserId = intent.getIntExtra(NetworkService.IN_EXTRA_USER_ID, -1);

        if (vehicleUserId >= 0) {
            fetchVehicleUser(vehicleUserId);
        } else {
            Log.e(TAG, "No vehicleUserId provided in the intent extras");
        }
    }

    private void fetchVehicleUser(final int vehicleUserId) {
        Log.d(TAG, "fetchVehicleUser(" + vehicleUserId + ")");

        if (isOngoingRequest(vehicleUserId)) {
            Log.d(TAG, "Found an ongoing request for vehicle user " + vehicleUserId);
            return;
        }

        final String uri = mVehicleUserStore.getUriForId(vehicleUserId).toString();

        Subscription subscription = createNetworkObservable(vehicleUserId)
                .observeOn(Schedulers.computation())
                .map(vehicleUser -> vehicleUser.withUpdateDate(new Date()))
                .doOnSubscribe(() -> startRequest(uri))
                .doOnError(doOnError(uri))
                .doOnCompleted(() -> completeRequest(uri))
                .subscribe(mVehicleUserStore::put,
                        e -> Log.e(TAG, "Error fetching vehicle user " + vehicleUserId, e));

        addRequest(vehicleUserId, subscription);
    }

    @NonNull
    private Observable<VehicleUser> createNetworkObservable(int vehicleUserId) {
        return getNetworkApi().getVehicleUser(vehicleUserId);
    }

    @NonNull
    @Override
    public Uri getServiceUri() {
        return ServiceUri.VEHICLE_USER.uri();
    }

}

/*
 * The MIT License
 *
 * Copyright (c) 2013-2016 reark project contributors
 *
 * https://github.com/reark/reark/graphs/contributors
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package io.bifri.vehicletracker.network;

import android.support.annotation.NonNull;

import java.util.List;

import io.bifri.vehicletracker.pojo.VehicleUser;
import io.bifri.vehicletracker.pojo.response.RawVehicleUser;
import io.bifri.vehicletracker.pojo.response.Users;
import io.bifri.vehicletracker.pojo.response.VehicleLocation;
import io.bifri.vehicletracker.pojo.response.VehicleLocations;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;

import static io.bifri.vehicletracker.network.VehicleUserService.USERS_OPERATION;
import static io.bifri.vehicletracker.network.VehicleUserService.USER_VEHICLES_LOCATION_OPERATION;
import static io.reark.reark.utils.Preconditions.checkNotNull;
import static rx.schedulers.Schedulers.io;

public class NetworkApi {

    @NonNull
    private final VehicleUserService mVehicleUserService;

    public NetworkApi(@NonNull final OkHttpClient client) {
        checkNotNull(client, "Client cannot be null.");

        Retrofit retrofit = new Retrofit.Builder()
                .addCallAdapterFactory(RxJavaCallAdapterFactory.createWithScheduler(io()))
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(VehicleUserService.BASE_URL)
                .client(client)
                .build();

        mVehicleUserService = retrofit.create(VehicleUserService.class);
    }

    @NonNull
    public Observable<List<RawVehicleUser>> users() {
        return mVehicleUserService.users(USERS_OPERATION)
                .map(Users::getData);
    }

    @NonNull
    public Observable<List<VehicleLocation>> userVehiclesLocation(int userId) {
        return mVehicleUserService.userVehiclesLocation(
                USER_VEHICLES_LOCATION_OPERATION, userId)
                .map(VehicleLocations::getData);
    }

    @NonNull
    public Observable<VehicleUser> getVehicleUser(int id) {
        // not supported
        return Observable.empty();
    }

}

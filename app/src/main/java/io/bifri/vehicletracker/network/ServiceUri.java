package io.bifri.vehicletracker.network;

import android.net.Uri;
import android.support.annotation.NonNull;

import static io.bifri.vehicletracker.network.VehicleUserService.BASE_URL;

public enum ServiceUri {

    VEHICLE_USER(Uri.parse(BASE_URL + "user")),
    VEHICLE_USERS(Uri.parse(BASE_URL + "users")),
    USER_VEHICLES_LOCATION(Uri.parse(BASE_URL + "userVehiclesLocation"));

    Uri mServiceUri;

    ServiceUri(Uri serviceUri) {
        mServiceUri = serviceUri;
    }

    public @NonNull Uri uri() {
        return mServiceUri;
    }

}

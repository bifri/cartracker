/*
 * The MIT License
 *
 * Copyright (c) 2013-2016 reark project contributors
 *
 * https://github.com/reark/reark/graphs/contributors
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package io.bifri.vehicletracker.network.fetchers;

import android.support.annotation.NonNull;

import io.bifri.vehicletracker.utils.rx.DelayStrategy;
import io.reark.reark.network.fetchers.FetcherBase;
import io.reark.reark.pojo.NetworkRequestStatus;
import io.bifri.vehicletracker.network.NetworkApi;
import rx.functions.Action1;

import static io.bifri.vehicletracker.utils.rx.DelayStrategy.CONSTANT_DELAY_TIMES_RETRY_COUNT;
import static io.reark.reark.utils.Preconditions.get;

public abstract class AppFetcherBase<T> extends FetcherBase<T> {

    protected static final int DEFAULT_RETRY_COUNT = 3;
    protected static final int DEFAULT_RETRY_DELAY_MILLIS = 1000;
    protected static final DelayStrategy DEFAULT_RETRY_DELAY_STRATEGY =
            CONSTANT_DELAY_TIMES_RETRY_COUNT;
    @NonNull
    private final NetworkApi mNetworkApi;

    protected AppFetcherBase(@NonNull final NetworkApi networkApi,
                             @NonNull final Action1<NetworkRequestStatus> updateNetworkRequestStatus) {
        super(updateNetworkRequestStatus);

        this.mNetworkApi = get(networkApi);
    }

    @NonNull
    public NetworkApi getNetworkApi() {
        return mNetworkApi;
    }

}

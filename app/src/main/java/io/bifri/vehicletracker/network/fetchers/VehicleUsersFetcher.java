/*
 * The MIT License
 *
 * Copyright (c) 2013-2016 reark project contributors
 *
 * https://github.com/reark/reark/graphs/contributors
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package io.bifri.vehicletracker.network.fetchers;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.bifri.vehicletracker.network.NetworkApi;
import io.bifri.vehicletracker.network.NetworkService;
import io.bifri.vehicletracker.network.ServiceUri;
import io.bifri.vehicletracker.pojo.ItemList;
import io.bifri.vehicletracker.pojo.VehicleUser;
import io.bifri.vehicletracker.pojo.response.RawVehicleUser;
import io.bifri.vehicletracker.pojo.response.Vehicle;
import io.bifri.vehicletracker.utils.rx.RetryWithDelay;
import io.reark.reark.data.stores.interfaces.StorePutInterface;
import io.reark.reark.pojo.NetworkRequestStatus;
import io.reark.reark.utils.Log;
import java8.util.stream.StreamSupport;
import rx.Observable;
import rx.Subscription;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

import static io.reark.reark.utils.Preconditions.checkNotNull;
import static io.reark.reark.utils.Preconditions.get;

public class VehicleUsersFetcher extends AppFetcherBase<Uri> {

    private static final String TAG = VehicleUsersFetcher.class.getSimpleName();

    @NonNull private final StorePutInterface<Vehicle, Integer> mVehicleStore;
    @NonNull private final StorePutInterface<ItemList<Integer>, Integer> mVehiclesStore;
    @NonNull private final StorePutInterface<VehicleUser, Integer> mVehicleUserStore;
    @NonNull private final StorePutInterface<ItemList<String>, String> mVehicleUsersStore;

    public VehicleUsersFetcher(
            @NonNull final NetworkApi networkApi,
            @NonNull final Action1<NetworkRequestStatus> updateNetworkRequestStatus,
            @NonNull final StorePutInterface<Vehicle, Integer> vehicleStore,
            @NonNull final StorePutInterface<ItemList<Integer>, Integer> vehiclesStore,
            @NonNull final StorePutInterface<VehicleUser, Integer> vehicleUserStore,
            @NonNull final StorePutInterface<ItemList<String>, String> vehicleUsersStore) {
        super(networkApi, updateNetworkRequestStatus);

        this.mVehicleStore = get(vehicleStore);
        this.mVehiclesStore = get(vehiclesStore);
        this.mVehicleUserStore = get(vehicleUserStore);
        this.mVehicleUsersStore = get(vehicleUsersStore);
    }

    @Override
    public void fetch(@NonNull final Intent intent) {
        checkNotNull(intent);

        final String searchString = intent.getStringExtra(NetworkService.IN_EXTRA_SEARCH_STRING);

        if (searchString != null) {
            fetchVehicleUsers(searchString);
        } else {
            Log.e(TAG, "No searchString provided in the intent extras");
        }
    }

    private void fetchVehicleUsers(@NonNull final String searchString) {
        checkNotNull(searchString);

        Log.d(TAG, "fetchVehicleUsers(" + searchString + ")");

        if (isOngoingRequest(searchString.hashCode())) {
            Log.d(TAG, "Found an ongoing request for vehicle users " + searchString);
            return;
        }

        final String uri = mVehicleUsersStore.getUriForId(searchString).toString();

        Subscription subscription = createNetworkObservable()
                .retryWhen(new RetryWithDelay(
                        DEFAULT_RETRY_COUNT,
                        DEFAULT_RETRY_DELAY_MILLIS,
                        DEFAULT_RETRY_DELAY_STRATEGY))
                .observeOn(Schedulers.computation())
                .map((rawVehicleUsers) -> {
                    Date updateDate = new Date();
                    final List<Integer> userIds =
                            putUsersAndVehiclesInStoreAndReturnUserIds(
                                    rawVehicleUsers,
                                    updateDate);
                    return new ItemList<>(searchString, userIds, updateDate);
                })
                .doOnNext(mVehicleUsersStore::put)
                .doOnSubscribe(() -> startRequest(uri))
                .doOnCompleted(() -> completeRequest(uri))
                .doOnError(doOnError(uri))
                .subscribe(__ -> {},
                        e -> Log.e(TAG, "Error fetching vehicle users for '"
                                + searchString + "'", e));

        addRequest(searchString.hashCode(), subscription);
    }

    @NonNull
    private Observable<List<RawVehicleUser>> createNetworkObservable() {
        return getNetworkApi().users();
    }

    @NonNull
    @Override
    public Uri getServiceUri() {
        return ServiceUri.VEHICLE_USERS.uri();
    }

    @NonNull
    private List<Integer> putVehiclesInStoreAndReturnVehicleIds(
            @NonNull List<Vehicle> vehicles, Date updateDate) {

        final List<Integer> result = new ArrayList<>();
        StreamSupport.stream(vehicles)
                .filter(Vehicle::isSome)
                .forEach(vehicle -> {
                    Vehicle newVehicle = vehicle.withUpdateDate(updateDate);
                    mVehicleStore.put(newVehicle);
                    result.add(newVehicle.getId());
                });

        return result;
    }

    private void putVehiclesAndVehicleItemListInStore(
            List<Vehicle> vehicles, @NonNull Integer userId, Date updateDate) {

        if (vehicles == null) {
            return;
        }
        final List<Integer> vehicleIds =
                putVehiclesInStoreAndReturnVehicleIds(vehicles, updateDate);
        mVehiclesStore.put(new ItemList<>(userId, vehicleIds, updateDate));
    }

    private List<Integer> putUsersAndVehiclesInStoreAndReturnUserIds(
            @NonNull List<RawVehicleUser> rawVehicleUsers, Date updateDate) {

        final List<Integer> result = new ArrayList<>();
        StreamSupport.stream(rawVehicleUsers)
                .filter(RawVehicleUser::isSome)
                .forEach(rawVehicleUser -> {
                            putVehiclesAndVehicleItemListInStore(
                                    rawVehicleUser.getVehicles(),
                                    rawVehicleUser.getId(),
                                    updateDate);
                            VehicleUser vu = new VehicleUser(
                                    rawVehicleUser.getId(),
                                    rawVehicleUser.getOwner(),
                                    updateDate);
                            mVehicleUserStore.put(vu);
                            result.add(vu.getId());
                        }
                );
        return result;
    }

}

/*
 * The MIT License
 *
 * Copyright (c) 2013-2016 reark project contributors
 *
 * https://github.com/reark/reark/graphs/contributors
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package io.bifri.vehicletracker.network;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.IBinder;

import javax.inject.Inject;

import io.bifri.vehicletracker.App;
import io.reark.reark.utils.Log;

public class NetworkService extends Service {

    private static final String TAG = NetworkService.class.getSimpleName();

    @Inject
    ServiceDataLayer mServiceDataLayer;

    public static final String IN_EXTRA_SERVICE_URI = "inExtraServiceUri";
    public static final String IN_EXTRA_USER_ID = "inExtraUserId";
    public static final String IN_EXTRA_SEARCH_STRING = "inExtraSearchString";

    public static Intent makeIntent(Context context, Uri serviceUri, int userId) {
        Intent result = makeIntent(context, serviceUri);
        result.putExtra(IN_EXTRA_USER_ID, userId);
        return result;
    }

    public static Intent makeIntent(Context context, Uri serviceUri, String searchString) {
        Intent result = makeIntent(context, serviceUri);
        result.putExtra(IN_EXTRA_SEARCH_STRING, searchString);
        return result;
    }

    private static Intent makeIntent(Context context, Uri serviceUri) {
        Intent result = new Intent(context, NetworkService.class);
        result.putExtra(IN_EXTRA_SERVICE_URI, serviceUri);
        return result;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        App.getInstance().getGraph().inject(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand");
        super.onStartCommand(intent, flags, startId);

        if (intent != null) {
            mServiceDataLayer.processIntent(intent);
        } else {
            Log.d(TAG, "Intent was null, no action taken");
        }

        return START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

}

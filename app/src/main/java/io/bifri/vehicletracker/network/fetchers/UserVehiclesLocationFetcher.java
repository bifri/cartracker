/*
 * The MIT License
 *
 * Copyright (c) 2013-2016 reark project contributors
 *
 * https://github.com/reark/reark/graphs/contributors
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package io.bifri.vehicletracker.network.fetchers;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.bifri.vehicletracker.network.NetworkApi;
import io.bifri.vehicletracker.network.NetworkService;
import io.bifri.vehicletracker.network.ServiceUri;
import io.bifri.vehicletracker.pojo.ItemList;
import io.bifri.vehicletracker.pojo.response.VehicleLocation;
import io.bifri.vehicletracker.utils.rx.RetryWithDelay;
import io.reark.reark.data.stores.interfaces.StorePutInterface;
import io.reark.reark.pojo.NetworkRequestStatus;
import io.reark.reark.utils.Log;
import java8.util.stream.StreamSupport;
import rx.Observable;
import rx.Subscription;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

import static io.reark.reark.utils.Preconditions.checkNotNull;
import static io.reark.reark.utils.Preconditions.get;

public class UserVehiclesLocationFetcher extends AppFetcherBase<Uri> {

    private static final String TAG = UserVehiclesLocationFetcher.class.getSimpleName();

    @NonNull
    private final StorePutInterface<VehicleLocation, Integer> mVehicleLocationStore;

    @NonNull
    private final StorePutInterface<ItemList<Integer>, Integer> mUserVehiclesLocationStore;

    public UserVehiclesLocationFetcher(
            @NonNull final NetworkApi networkApi,
            @NonNull final Action1<NetworkRequestStatus> updateNetworkRequestStatus,
            @NonNull final StorePutInterface<VehicleLocation, Integer> vehicleUserStore,
            @NonNull final StorePutInterface<ItemList<Integer>, Integer> vehicleUsersStore) {
        super(networkApi, updateNetworkRequestStatus);

        this.mVehicleLocationStore = get(vehicleUserStore);
        this.mUserVehiclesLocationStore = get(vehicleUsersStore);
    }

    @Override
    public void fetch(@NonNull final Intent intent) {

        checkNotNull(intent);

        final int vehicleUserId = intent.getIntExtra(NetworkService.IN_EXTRA_USER_ID, -1);

        if (vehicleUserId >= 0) {
            fetchUserVehiclesLocation(vehicleUserId);
        } else {
            Log.e(TAG, "No vehicleUserId provided in the intent extras");
        }

    }

    private void fetchUserVehiclesLocation(final int vehicleUserId) {
        checkNotNull(vehicleUserId);

        Log.d(TAG, "fetchUserVehiclesLocation(" + vehicleUserId + ")");

        if (isOngoingRequest(vehicleUserId)) {
            Log.d(TAG, "Found an ongoing request for user vehicles location " + vehicleUserId);
            return;
        }

        final String uri = mUserVehiclesLocationStore.getUriForId(vehicleUserId).toString();

        Subscription subscription = createNetworkObservable(vehicleUserId)
                .retryWhen(new RetryWithDelay(
                        DEFAULT_RETRY_COUNT,
                        DEFAULT_RETRY_DELAY_MILLIS,
                        DEFAULT_RETRY_DELAY_STRATEGY))
                .observeOn(Schedulers.computation())
                .map((vehicleLocations) -> {
                    Date updateDate = new Date();
                    final List<Integer> vehicleIds =
                            putVehicleLocationsInStoreAndReturnVehicleIds(
                                    vehicleLocations,
                                    updateDate);
                    return new ItemList<>(vehicleUserId, vehicleIds, updateDate);
                })
                .doOnNext(mUserVehiclesLocationStore::put)
                .doOnSubscribe(() -> startRequest(uri))
                .doOnCompleted(() -> completeRequest(uri))
                .doOnError(doOnError(uri))
                .subscribe(__ -> {},
                        e -> Log.e(TAG, "Error fetching vehicle locations for '"
                                + vehicleUserId + "'", e));

        addRequest(vehicleUserId, subscription);
    }

    @NonNull
    private Observable<List<VehicleLocation>> createNetworkObservable(
            final int vehicleUserId) {
        return getNetworkApi().userVehiclesLocation(vehicleUserId);
    }

    @NonNull
    @Override
    public Uri getServiceUri() {
        return ServiceUri.USER_VEHICLES_LOCATION.uri();
    }

    @NonNull
    private List<Integer> putVehicleLocationsInStoreAndReturnVehicleIds(
            @NonNull List<VehicleLocation> locations, Date updateDate) {

        final List<Integer> result = new ArrayList<>();
        StreamSupport.stream(locations)
                .filter(VehicleLocation::isSome)
                .forEach(vehicleLocation -> {
                            VehicleLocation vl = vehicleLocation.withUpdateDate(updateDate);
                            mVehicleLocationStore.put(vl);
                            result.add(vl.getVehicleId());
                        }
                );
        return result;
    }

}

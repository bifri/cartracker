/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.bifri.vehicletracker.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.maps.SupportMapFragment;

import javax.inject.Inject;

import io.bifri.vehicletracker.App;
import io.bifri.vehicletracker.R;
import io.bifri.vehicletracker.utils.ApplicationInstrumentation;
import io.bifri.vehicletracker.view.UserVehiclesOnMapView;
import io.bifri.vehicletracker.view.UserVehiclesOnMapViewBinder;
import io.bifri.vehicletracker.viewmodels.LocationServicesViewModel;
import io.bifri.vehicletracker.viewmodels.UserVehiclesLocationViewModel;

public class UserVehiclesOnMapActivity extends AppCompatActivity {

    private static final String IN_EXTRA_VEHICLE_USER_ID = "inExtraVehicleUserId";

    private UserVehiclesOnMapView mUserVehiclesOnMapView;
    private UserVehiclesOnMapViewBinder mVehiclesLocationViewBinder;

    @Inject UserVehiclesLocationViewModel mUserVehiclesLocationViewModel;
    @Inject LocationServicesViewModel mLocationServicesViewModel;
    @Inject ApplicationInstrumentation mInstrumentation;

    public static Intent makeIntent(Context context, int vehicleUserId) {
        Intent result = new Intent(context, UserVehiclesOnMapActivity.class);
        result.putExtra(IN_EXTRA_VEHICLE_USER_ID, vehicleUserId);
        return result;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.getInstance().getGraph().inject(this);
        setContentView(R.layout.map);

        mUserVehiclesOnMapView = new UserVehiclesOnMapView(
                this,
                findViewById(R.id.fl_map),
                (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map));

        mVehiclesLocationViewBinder = new UserVehiclesOnMapViewBinder(
                mUserVehiclesOnMapView,
                mUserVehiclesLocationViewModel,
                mLocationServicesViewModel);
//      mUserVehiclesLocationViewModel.subscribeToDataStore();
        mUserVehiclesLocationViewModel.setVehicleUser(
                getIntent().getIntExtra(IN_EXTRA_VEHICLE_USER_ID, -1));
    }

    @Override
    public void onResume() {
        super.onResume();
        mUserVehiclesLocationViewModel.subscribeToDataStore();
        mLocationServicesViewModel.subscribeToDataStore();
        mVehiclesLocationViewBinder.bind();
    }

    @Override
    public void onPause() {
        mVehiclesLocationViewBinder.unbind();
        mUserVehiclesLocationViewModel.unsubscribeFromDataStore();
        mLocationServicesViewModel.unsubscribeFromDataStore();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
//      mUserVehiclesLocationViewModel.unsubscribeFromDataStore();
        mUserVehiclesOnMapView.onDestroyView();
        mUserVehiclesOnMapView = null;
        mUserVehiclesLocationViewModel.dispose();
        mLocationServicesViewModel.dispose();
        mInstrumentation.getLeakTracing().traceLeakage(this);
        super.onDestroy();
    }

}

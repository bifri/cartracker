/*
 * The MIT License
 *
 * Copyright (c) 2013-2016 reark project contributors
 *
 * https://github.com/reark/reark/graphs/contributors
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package io.bifri.vehicletracker.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import javax.inject.Inject;

import io.bifri.vehicletracker.fragments.VehicleUserFragment;
import io.reark.reark.utils.Log;
import io.bifri.vehicletracker.R;
import io.bifri.vehicletracker.App;
import io.bifri.vehicletracker.data.DataFunctions;
import io.bifri.vehicletracker.pojo.AppUserSettings;

public class VehicleUserDetailsActivity extends AppCompatActivity {

    private static final String TAG = VehicleUserDetailsActivity.class.getSimpleName();

    private static final int REQUEST_CHOOSE_VEHICLE_USER = 0;

    @Inject
    DataFunctions.SetUserSettings mSetUserSettings;

    public VehicleUserDetailsActivity() {
        App.getInstance().getGraph().inject(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new VehicleUserFragment())
                    .commit();
        }
    }

    public void chooseVehicleUser() {
        Log.d(TAG, "chooseVehicleUser");
        Intent intent = VehicleUserListActivity.makeIntent(this);
        startActivityForResult(intent, REQUEST_CHOOSE_VEHICLE_USER);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult");
        if (data == null) {
            Log.d(TAG, "No data from onActivityResult");
            return;
        }
        final int vehicleUserId = data.getIntExtra("userId", 0);
        if (vehicleUserId == 0) {
            Log.e(TAG, "Invalid vehicleUserId from onActivityResult");
            return;
        }
        Log.d(TAG, "New vehicleUserId: " + vehicleUserId);
        // We should probably send an intent to update the widget
        // in case its service is not alive anymore. This works as
        // long as it is alive, though.
        mSetUserSettings.call(new AppUserSettings(vehicleUserId));
    }

}

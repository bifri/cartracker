/*
 * The MIT License
 *
 * Copyright (c) 2013-2016 reark project contributors
 *
 * https://github.com/reark/reark/graphs/contributors
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package io.bifri.vehicletracker.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.tbruyelle.rxpermissions.RxPermissions;

import io.bifri.vehicletracker.R;
import io.bifri.vehicletracker.fragments.VehicleUsersFragment;
import io.bifri.vehicletracker.utils.SdkUtils;

public class VehicleUserListActivity extends AppCompatActivity {

    private static final String TAG = VehicleUserListActivity.class.getSimpleName();

    public static Intent makeIntent(Context context) {
        return new Intent(context, VehicleUserListActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Pair<Boolean, String> pair = SdkUtils.checkGooglePlayServices(this);
        if (!pair.first) {
            Toast.makeText(this, R.string.play_services_problem, Toast.LENGTH_LONG)
                    .show();
            finish();
        }

        RxPermissions rxPermissions = new RxPermissions(this);
        rxPermissions.setLogging(true);

        // Must be done during an initialization phase like onCreate
        rxPermissions
                .request(Manifest.permission.ACCESS_FINE_LOCATION)
                .subscribe(granted -> {
                    if (granted) { // Always true pre-M
                        Log.i(TAG, "All permissions granted.");
                        onCreateAfterAllPermissionsGranted(savedInstanceState);
                    } else {
                        Log.i(TAG, "Some or all permissions are not granted.");
                        Toast.makeText(this, R.string.app_stop_no_permissions, Toast.LENGTH_LONG)
                                .show();
                        finish();
                    }
                });
    }

    protected void onCreateAfterAllPermissionsGranted(Bundle savedInstanceState) {
        setContentView(R.layout.activity_main);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new VehicleUsersFragment())
                    .commit();
        }
    }

    public void chooseVehicleUser(int vehicleUserId) {
        startActivity(UserVehiclesOnMapActivity.makeIntent(this, vehicleUserId));
    }

}

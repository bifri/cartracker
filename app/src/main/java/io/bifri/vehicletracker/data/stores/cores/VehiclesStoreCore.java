package io.bifri.vehicletracker.data.stores.cores;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Date;

import io.bifri.vehicletracker.data.schematicProvider.DatabaseProvider;
import io.bifri.vehicletracker.data.schematicProvider.VehicleListColumns;
import io.bifri.vehicletracker.pojo.ItemList;
import io.bifri.vehicletracker.utils.DateUtils;
import io.reark.reark.data.stores.cores.ContentProviderStoreCore;

import static io.reark.reark.utils.Preconditions.checkNotNull;
import static io.reark.reark.utils.Preconditions.get;

public class VehiclesStoreCore extends ContentProviderStoreCore<Integer, ItemList<Integer>> {

    private final Gson mGson;

    public VehiclesStoreCore(@NonNull final ContentResolver contentResolver, @NonNull final Gson gson) {
        super(contentResolver);

        this.mGson = get(gson);
    }

    @NonNull
    @Override
    protected String getAuthority() {
        return DatabaseProvider.AUTHORITY;
    }

    @NonNull
    @Override
    public Uri getContentUri() {
        return DatabaseProvider.VehicleLists.VEHICLE_LISTS;
    }

    @NonNull
    @Override
    protected String[] getProjection() {
        return new String[] {
                VehicleListColumns.USER_ID,
                VehicleListColumns.JSON,
                VehicleListColumns.UPDATED
        };
    }

    @NonNull
    @Override
    protected ContentValues getContentValuesForItem(@NonNull final ItemList<Integer> item) {
        checkNotNull(item);

        ContentValues contentValues = new ContentValues();
        contentValues.put(VehicleListColumns.USER_ID, item.key());
        contentValues.put(VehicleListColumns.JSON, mGson.toJson(item));
        contentValues.put(VehicleListColumns.UPDATED, DateUtils.toDbValue(item.updateDate()));
        return contentValues;
    }

    @NonNull
    @Override
    protected ItemList<Integer> read(@NonNull final Cursor cursor) {
        checkNotNull(cursor);

        final String json = cursor.getString(cursor.getColumnIndex(VehicleListColumns.JSON));
        final Date updated = DateUtils.fromDbValue(
                cursor.getLong(cursor.getColumnIndex(VehicleListColumns.UPDATED)));

        Type listType = new TypeToken<ItemList<String>>(){}.getType();
        ItemList<Integer> vehicleUserList = mGson.fromJson(json, listType);
        vehicleUserList = vehicleUserList.withUpdateDate(updated);

        return vehicleUserList;
    }

    @NonNull
    @Override
    public Uri getUriForId(@NonNull final Integer id) {
        checkNotNull(id);

        return DatabaseProvider.VehicleLists.withId(String.valueOf(id));
    }

    @NonNull
    @Override
    protected Integer getIdForUri(@NonNull final Uri uri) {
        checkNotNull(uri);

        return (int) DatabaseProvider.VehicleLists.fromUri(uri);
    }

}

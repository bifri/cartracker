/*
 * The MIT License
 *
 * Copyright (c) 2013-2016 reark project contributors
 *
 * https://github.com/reark/reark/graphs/contributors
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package io.bifri.vehicletracker.data.schematicProvider;

import net.simonvt.schematic.annotation.Database;
import net.simonvt.schematic.annotation.Table;

@Database(version = AppDatabase.VERSION)
public final class AppDatabase {

    public static final int VERSION = 1;

    @Table(VehicleUserColumns.class) public static final String VEHICLE_USERS = "vehicleUsers";
    @Table(VehicleUserListColumns.class) public static final String VEHICLE_USER_LISTS = "vehicleUserLists";
    @Table(VehicleColumns.class) public static final String VEHICLES = "vehicles";
    @Table(VehicleListColumns.class) public static final String VEHICLE_LISTS = "vehicleLists";
    @Table(VehicleLocationColumns.class) public static final String VEHICLES_LOCATION = "vehiclesLocation";
    @Table(VehicleLocationListColumns.class) public static final String VEHICLE_LOCATION_LISTS = "vehicleLocationLists";
    @Table(NetworkRequestStatusColumns.class) public static final String NETWORK_REQUEST_STATUSES = "networkRequestStatuses";
    @Table(AppUserSettingsColumns.class) public static final String APP_USER_SETTINGS = "appUserSettings";

}

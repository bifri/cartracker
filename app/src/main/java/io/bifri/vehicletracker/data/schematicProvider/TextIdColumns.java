package io.bifri.vehicletracker.data.schematicProvider;

import net.simonvt.schematic.annotation.DataType;
import net.simonvt.schematic.annotation.PrimaryKey;

public interface TextIdColumns {

    @DataType(DataType.Type.TEXT) @PrimaryKey String ID = "_id";

}

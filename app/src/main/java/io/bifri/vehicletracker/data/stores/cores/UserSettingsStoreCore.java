/*
 * The MIT License
 *
 * Copyright (c) 2013-2016 reark project contributors
 *
 * https://github.com/reark/reark/graphs/contributors
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package io.bifri.vehicletracker.data.stores.cores;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.NonNull;

import com.google.gson.Gson;

import io.bifri.vehicletracker.data.DataLayer;
import io.bifri.vehicletracker.data.schematicProvider.AppUserSettingsColumns;
import io.bifri.vehicletracker.data.schematicProvider.DatabaseProvider;
import io.bifri.vehicletracker.pojo.AppUserSettings;
import io.reark.reark.data.stores.cores.ContentProviderStoreCore;
import io.reark.reark.utils.Preconditions;

import static io.reark.reark.utils.Preconditions.checkNotNull;

public class UserSettingsStoreCore extends ContentProviderStoreCore<Integer, AppUserSettings> {

    private final Gson mGson;

    public UserSettingsStoreCore(@NonNull final ContentResolver contentResolver,
                                 @NonNull final Gson gson) {
        super(contentResolver);

        this.mGson = Preconditions.get(gson);
    }

    @NonNull
    @Override
    protected String getAuthority() {
        return DatabaseProvider.AUTHORITY;
    }

    @NonNull
    @Override
    public Uri getContentUri() {
        return DatabaseProvider.AppUserSettings.APP_USER_SETTINGS;
    }

    @NonNull
    @Override
    protected String[] getProjection() {
        return new String[] { AppUserSettingsColumns.ID, AppUserSettingsColumns.JSON };
    }

    @NonNull
    @Override
    protected ContentValues getContentValuesForItem(@NonNull final AppUserSettings item) {
        checkNotNull(item);

        ContentValues contentValues = new ContentValues();
        contentValues.put(AppUserSettingsColumns.ID, DataLayer.DEFAULT_USER_ID);
        contentValues.put(AppUserSettingsColumns.JSON, mGson.toJson(item));
        return contentValues;
    }

    @NonNull
    @Override
    protected AppUserSettings read(@NonNull final Cursor cursor) {
        checkNotNull(cursor);

        final String json = cursor.getString(cursor.getColumnIndex(AppUserSettingsColumns.JSON));
        return mGson.fromJson(json, AppUserSettings.class);
    }

    @NonNull
    @Override
    public Uri getUriForId(@NonNull final Integer id) {
        checkNotNull(id);

        return DatabaseProvider.AppUserSettings.withId(id);
    }

    @NonNull
    @Override
    protected Integer getIdForUri(@NonNull final Uri uri) {
        checkNotNull(uri);

        return (int) DatabaseProvider.AppUserSettings.fromUri(uri);
    }

}

/*
 * The MIT License
 *
 * Copyright (c) 2013-2016 reark project contributors
 *
 * https://github.com/reark/reark/graphs/contributors
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package io.bifri.vehicletracker.data;

import android.support.annotation.NonNull;

import io.bifri.vehicletracker.pojo.ItemList;
import io.bifri.vehicletracker.pojo.AppUserSettings;
import io.bifri.vehicletracker.pojo.VehicleUser;
import io.bifri.vehicletracker.pojo.response.Vehicle;
import io.bifri.vehicletracker.pojo.response.VehicleLocation;
import io.bifri.vehicletracker.utils.rx.RepeatWithDelay;
import io.reark.reark.data.DataStreamNotification;
import io.reark.reark.data.stores.interfaces.StoreInterface;
import io.reark.reark.data.utils.DataLayerUtils;
import io.reark.reark.pojo.NetworkRequestStatus;
import io.reark.reark.utils.Log;
import rx.Observable;
import rx.Subscription;
import rx.functions.Func1;

import static android.text.format.DateUtils.DAY_IN_MILLIS;
import static io.bifri.vehicletracker.utils.rx.DelayStrategy.CONSTANT_DELAY;
import static io.reark.reark.utils.Preconditions.checkNotNull;
import static io.reark.reark.utils.Preconditions.get;

public abstract class ClientDataLayerBase extends DataLayerBase {

    private static final String TAG = ClientDataLayerBase.class.getSimpleName();

    public static final int DEFAULT_USER_ID = 1;

    @NonNull
    protected final StoreInterface<Integer, AppUserSettings, AppUserSettings> mUserSettingsStore;

    protected ClientDataLayerBase(
            @NonNull final StoreInterface<Integer, NetworkRequestStatus, NetworkRequestStatus> networkRequestStatusStore,
            @NonNull final StoreInterface<Integer, VehicleUser, VehicleUser> vehicleUserStore,
            @NonNull final StoreInterface<String, ItemList<String>, ItemList<String>> vehicleUsersStore,
            @NonNull final StoreInterface<Integer, Vehicle, Vehicle> vehicleStore,
            @NonNull final StoreInterface<Integer, ItemList<Integer>, ItemList<Integer>> vehiclesStore,
            @NonNull final StoreInterface<Integer, VehicleLocation, VehicleLocation> vehicleLocationStore,
            @NonNull final StoreInterface<Integer, ItemList<Integer>, ItemList<Integer>> userVehiclesLocationStore,
            @NonNull final StoreInterface<Integer, AppUserSettings, AppUserSettings> userSettingsStore) {
        super(networkRequestStatusStore,
                vehicleUserStore,
                vehicleUsersStore,
                vehicleStore,
                vehiclesStore,
                vehicleLocationStore,
                userVehiclesLocationStore);

        this.mUserSettingsStore = get(userSettingsStore);
    }

    private static <T, U, R> int getUniqueId(StoreInterface<T, U, R> storeInterface, T id) {
        return storeInterface.getUniqueId(storeInterface.getUriForId(id));
    }

    @NonNull
    public Observable<DataStreamNotification<ItemList<String>>> getVehicleUsers(@NonNull final String searchString) {
        checkNotNull(searchString);

        Log.d(TAG, "getVehicleUsers(" + searchString + ")");

        final Observable<NetworkRequestStatus> networkRequestStatusObservable =
                mNetworkRequestStatusStore
                        .getOnceAndStream(getUniqueId(mVehicleUsersStore, searchString))
                        .filter(NetworkRequestStatus::isSome);

        final Observable<ItemList<String>> vehicleUsersObservable =
                mVehicleUsersStore
                        .getOnceAndStream(searchString)
                        .filter(ItemList::isSome);

        return DataLayerUtils.createDataStreamNotificationObservable(
                networkRequestStatusObservable, vehicleUsersObservable);
    }

    @NonNull
    public Observable<DataStreamNotification<ItemList<String>>> fetchAndGetVehicleUsers(@NonNull final String searchString) {
        checkNotNull(searchString);
        Log.d(TAG, "fetchAndGetVehicleUsers(" + searchString + ")");

        fetchVehicleUsers(searchString);
        return getVehicleUsers(searchString);
    }

    @NonNull
    public Observable<DataStreamNotification<ItemList<String>>> dailyFetchAndGetVehicleUsers(
            @NonNull final String searchString) {

        checkNotNull(searchString);
        Log.d(TAG, "dailyFetchAndGetVehicleUsers(" + searchString + ")");

        return Observable.defer(() -> {
            Subscription subscription = dailyFetchVehicleUsers(searchString).subscribe();
            return getVehicleUsers(searchString)
                    .doOnUnsubscribe(subscription::unsubscribe);
        });
    }

    private Observable<Void> dailyFetchVehicleUsers(String searchString) {
        return Observable.<Void>fromCallable(() -> {
            fetchVehicleUsers(searchString);
            return null;
        })
                .repeatWhen(new RepeatWithDelay(DAY_IN_MILLIS, CONSTANT_DELAY));
    }

    protected abstract void fetchVehicleUsers(@NonNull final String searchString);

    @NonNull
    public Observable<DataStreamNotification<ItemList<Integer>>> getVehicles(
            @NonNull final Integer userId ) {
        checkNotNull(userId);

        Log.d(TAG, "getVehicles(" + userId + ")");

        final Observable<NetworkRequestStatus> networkRequestStatusObservable =
                mNetworkRequestStatusStore
                        .getOnceAndStream(getUniqueId(mVehiclesStore, userId))
                        .filter(NetworkRequestStatus::isSome);

        final Observable<ItemList<Integer>> vehiclesObservable =
                mVehiclesStore
                        .getOnceAndStream(userId)
                        .filter(ItemList::isSome);

        return DataLayerUtils.createDataStreamNotificationObservable(
                networkRequestStatusObservable, vehiclesObservable);
    }

    @NonNull
    public Observable<DataStreamNotification<ItemList<Integer>>> getUserVehiclesLocation(
            @NonNull final Integer userId) {
        checkNotNull(userId);

        Log.d(TAG, "getUserVehiclesLocation(" + userId + ")");

        final Observable<NetworkRequestStatus> networkRequestStatusObservable =
                mNetworkRequestStatusStore
                        .getOnceAndStream(getUniqueId(mVehiclesLocationStore, userId))
                        .filter(NetworkRequestStatus::isSome);

        final Observable<ItemList<Integer>> vehiclesLocationObservable =
                mVehiclesLocationStore
                        .getOnceAndStream(userId)
                        .filter(ItemList::isSome);

        return DataLayerUtils.createDataStreamNotificationObservable(
                networkRequestStatusObservable, vehiclesLocationObservable);
    }

    @NonNull
    public Observable<DataStreamNotification<ItemList<Integer>>> fetchAndGetUserVehiclesLocation(
            @NonNull final Integer userId) {
        checkNotNull(userId);
        Log.d(TAG, "fetchAndGetUserVehiclesLocation(" + userId + ")");

        fetchUserVehiclesLocation(userId);
        return getUserVehiclesLocation(userId);
    }

    @NonNull
    public Observable<DataStreamNotification<ItemList<Integer>>> fetchAndGetUserVehiclesLocation(
            @NonNull final Integer userId,
            Func1<Observable<? extends Void>, Observable<?>> repeatFrequency) {

        checkNotNull(userId);
        Log.d(TAG, "fetchAndGetUserVehiclesLocation(" + userId + ")");

        return Observable.defer(() -> {
            Subscription subscription = fetchUserVehiclesLocation(userId, repeatFrequency)
                    .subscribe();
            return getUserVehiclesLocation(userId)
                    .doOnUnsubscribe(subscription::unsubscribe);
        });
    }

    private Observable<Void> fetchUserVehiclesLocation(
            Integer userId,
            Func1<Observable<? extends Void>, Observable<?>> repeatFrequency) {

        return Observable.<Void>fromCallable(() -> {
            fetchUserVehiclesLocation(userId);
            return null;
        }).repeatWhen(repeatFrequency);
    }

    protected abstract void fetchUserVehiclesLocation(@NonNull final Integer userId);

    @NonNull
    public Observable<DataStreamNotification<ItemList<Integer>>> getUserVehicles(
            @NonNull final Integer userId) {
        checkNotNull(userId);

        Log.d(TAG, "getUserVehicles(" + userId + ")");

        final Observable<NetworkRequestStatus> networkRequestStatusObservable =
                mNetworkRequestStatusStore
                        .getOnceAndStream(getUniqueId(mVehiclesStore, userId))
                        .filter(NetworkRequestStatus::isSome);

        final Observable<ItemList<Integer>> vehicleUsersObservable =
                mVehiclesStore
                        .getOnceAndStream(userId)
                        .filter(ItemList::isSome);

        return DataLayerUtils.createDataStreamNotificationObservable(
                networkRequestStatusObservable, vehicleUsersObservable);
    }

    @NonNull
    public Observable<VehicleUser> getVehicleUser(@NonNull final Integer vehicleUserId) {
        checkNotNull(vehicleUserId);

        return mVehicleUserStore
                .getOnceAndStream(vehicleUserId)
                .filter(VehicleUser::isSome);
    }

    @NonNull
    public Observable<VehicleUser> fetchAndGetVehicleUser(@NonNull final Integer vehicleUserId) {
        checkNotNull(vehicleUserId);

        fetchVehicleUser(vehicleUserId);
        return getVehicleUser(vehicleUserId);
    }

    protected abstract void fetchVehicleUser(@NonNull final Integer vehicleUserId);

    @NonNull
    public Observable<Vehicle> getVehicle(@NonNull final Integer vehicleId) {
        checkNotNull(vehicleId);

        return mVehicleStore
                .getOnceAndStream(vehicleId)
                .filter(Vehicle::isSome);
    }

    @NonNull
    public Observable<VehicleLocation> getVehicleLocation(@NonNull final Integer vehicleId) {
        checkNotNull(vehicleId);

        return mVehicleLocationStore
                .getOnceAndStream(vehicleId)
                .filter(VehicleLocation::isSome);
    }

    @NonNull
    public Observable<AppUserSettings> getUserSettings() {
        return mUserSettingsStore
                .getOnceAndStream(DEFAULT_USER_ID)
                .filter(AppUserSettings::isSome);
    }

    public void setUserSettings(@NonNull final AppUserSettings appUserSettings) {
        checkNotNull(appUserSettings);

        mUserSettingsStore.put(appUserSettings);
    }

}

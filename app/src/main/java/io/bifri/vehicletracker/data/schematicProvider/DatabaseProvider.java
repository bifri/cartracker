package io.bifri.vehicletracker.data.schematicProvider;

import android.net.Uri;
import android.net.Uri.Builder;
import android.support.annotation.NonNull;

import net.simonvt.schematic.annotation.ContentProvider;
import net.simonvt.schematic.annotation.ContentUri;
import net.simonvt.schematic.annotation.InexactContentUri;
import net.simonvt.schematic.annotation.TableEndpoint;

import io.bifri.vehicletracker.BuildConfig;

import static io.reark.reark.utils.Preconditions.get;

@ContentProvider(authority = DatabaseProvider.AUTHORITY, database = AppDatabase.class)
public final class DatabaseProvider {

    public static final String AUTHORITY = BuildConfig.DB_PROVIDER_AUTHORITY;
    private static final Uri AUTHORITY_URI = Uri.parse("content://" + AUTHORITY);
    private static final String BASE_TYPE_DIR = "vnd.android.cursor.dir/vnd.io.bifri.vehicletracker.";
    private static final String BASE_TYPE_ITEM = "vnd.android.cursor.item/vnd.io.bifri.vehicletracker.";

    private static Uri buildUri(@NonNull final String... paths) {
        Builder builder = AUTHORITY_URI.buildUpon();
        for (String path : paths) {
            builder.appendPath(path);
        }
        return get(builder.build());
    }

    @TableEndpoint(table = AppDatabase.APP_USER_SETTINGS)
    public static final class AppUserSettings {
        @ContentUri(
                path = AppDatabase.APP_USER_SETTINGS,
                type = BASE_TYPE_DIR + AppDatabase.APP_USER_SETTINGS,
                defaultSort = AppUserSettingsColumns.ID + " ASC")
        public static final Uri APP_USER_SETTINGS = Uri.withAppendedPath(AUTHORITY_URI, AppDatabase.APP_USER_SETTINGS);

        @InexactContentUri(
                path = AppDatabase.APP_USER_SETTINGS + "/*",
                name = "USER_SETTINGS_ID",
                type = BASE_TYPE_ITEM + AppDatabase.APP_USER_SETTINGS,
                whereColumn = AppUserSettingsColumns.ID,
                pathSegment = 1)
        public static Uri withId(long id) {
            return buildUri(AppDatabase.APP_USER_SETTINGS, String.valueOf(id));
        }

        public static long fromUri(Uri uri) {
            return Long.valueOf(uri.getLastPathSegment());
        }
    }

    @TableEndpoint(table = AppDatabase.NETWORK_REQUEST_STATUSES)
    public static final class NetworkRequestStatuses {
        @ContentUri(
                path = AppDatabase.NETWORK_REQUEST_STATUSES,
                type = BASE_TYPE_DIR + AppDatabase.NETWORK_REQUEST_STATUSES,
                defaultSort = NetworkRequestStatusColumns.ID + " ASC")
        public static final Uri NETWORK_REQUEST_STATUSES = Uri.withAppendedPath(AUTHORITY_URI, AppDatabase.NETWORK_REQUEST_STATUSES);

        @InexactContentUri(
                path = AppDatabase.NETWORK_REQUEST_STATUSES + "/*",
                name = "NETWORK_REQUEST_STATUSES_ID",
                type = BASE_TYPE_DIR + AppDatabase.NETWORK_REQUEST_STATUSES,
                whereColumn = NetworkRequestStatusColumns.ID,
                pathSegment = 1)
        public static Uri withId(long id) {
            return buildUri(AppDatabase.NETWORK_REQUEST_STATUSES, String.valueOf(id));
        }

        public static long fromUri(Uri uri) {
            return Long.valueOf(uri.getLastPathSegment());
        }
    }

    @TableEndpoint(table = AppDatabase.VEHICLE_USERS)
    public static final class VehicleUsers {
        @ContentUri(
                path = AppDatabase.VEHICLE_USERS,
                type = BASE_TYPE_DIR + AppDatabase.VEHICLE_USERS,
                defaultSort = VehicleUserColumns.ID + " ASC")
        public static final Uri VEHICLE_USERS = Uri.withAppendedPath(AUTHORITY_URI, AppDatabase.VEHICLE_USERS);

        @InexactContentUri(
                path = AppDatabase.VEHICLE_USERS + "/*",
                name = "VEHICLE_USERS_ID",
                type = BASE_TYPE_ITEM + AppDatabase.VEHICLE_USERS,
                whereColumn = VehicleUserColumns.ID,
                pathSegment = 1)
        public static Uri withId(long id) {
            return buildUri(AppDatabase.VEHICLE_USERS, String.valueOf(id));
        }

        public static long fromUri(Uri uri) {
            String lastSegment = uri.getLastPathSegment();
            if (AppDatabase.VEHICLE_USERS.equals(lastSegment)) {
                return 0;
            } else {
                return Long.valueOf(uri.getLastPathSegment());
            }
        }
    }

    @TableEndpoint(table = AppDatabase.VEHICLE_USER_LISTS)
    public static final class VehicleUserLists {
        @ContentUri(
                path = AppDatabase.VEHICLE_USER_LISTS,
                type = BASE_TYPE_DIR + AppDatabase.VEHICLE_USER_LISTS,
                defaultSort = VehicleUserListColumns.ID + " ASC")
        public static final Uri VEHICLE_USER_LISTS = Uri.withAppendedPath(AUTHORITY_URI, AppDatabase.VEHICLE_USER_LISTS);

        @InexactContentUri(
                path = AppDatabase.VEHICLE_USER_LISTS + "/*",
                name = "VEHICLE_USER_LISTS_ID",
                type = BASE_TYPE_ITEM + AppDatabase.VEHICLE_USER_LISTS,
                whereColumn = VehicleUserListColumns.ID,
                pathSegment = 1)
        public static Uri withId(String id) {
            return buildUri(AppDatabase.VEHICLE_USER_LISTS, id);
        }

        public static String fromUri(Uri uri) {
            return uri.getLastPathSegment();
        }
    }

    @TableEndpoint(table = AppDatabase.VEHICLES)
    public static final class Vehicles {
        @ContentUri(
                path = AppDatabase.VEHICLES,
                type = BASE_TYPE_DIR + AppDatabase.VEHICLES,
                defaultSort = VehicleColumns.ID + " ASC")
        public static final Uri VEHICLES = Uri.withAppendedPath(AUTHORITY_URI, AppDatabase.VEHICLES);

        @InexactContentUri(
                path = AppDatabase.VEHICLES + "/*",
                name = "VEHICLES_ID",
                type = BASE_TYPE_ITEM + AppDatabase.VEHICLES,
                whereColumn = VehicleColumns.ID,
                pathSegment = 1)
        public static Uri withId(long id) {
            return buildUri(AppDatabase.VEHICLES, String.valueOf(id));
        }

        public static long fromUri(Uri uri) {
            String lastSegment = uri.getLastPathSegment();
            if (AppDatabase.VEHICLE_USERS.equals(lastSegment)) {
                return 0;
            } else {
                return Long.valueOf(uri.getLastPathSegment());
            }
        }
    }

    @TableEndpoint(table = AppDatabase.VEHICLE_LISTS)
    public static final class VehicleLists {
        @ContentUri(
                path = AppDatabase.VEHICLE_LISTS,
                type = BASE_TYPE_DIR + AppDatabase.VEHICLE_LISTS,
                defaultSort = VehicleListColumns.USER_ID + " ASC")
        public static final Uri VEHICLE_LISTS = Uri.withAppendedPath(AUTHORITY_URI, AppDatabase.VEHICLE_LISTS);

        @InexactContentUri(
                path = AppDatabase.VEHICLE_LISTS + "/*",
                name = "VEHICLE_LISTS_ID",
                type = BASE_TYPE_ITEM + AppDatabase.VEHICLE_LISTS,
                whereColumn = VehicleListColumns.USER_ID,
                pathSegment = 1)
        public static Uri withId(String id) {
            return buildUri(AppDatabase.VEHICLE_LISTS, id);
        }

        public static long fromUri(Uri uri) {
            return Long.valueOf(uri.getLastPathSegment());
        }
    }

    @TableEndpoint(table = AppDatabase.VEHICLES_LOCATION)
    public static final class VehiclesLocation {
        @ContentUri(
                path = AppDatabase.VEHICLES_LOCATION,
                type = BASE_TYPE_DIR + AppDatabase.VEHICLES_LOCATION,
                defaultSort = VehicleLocationColumns.VEHICLE_ID + " ASC")
        public static final Uri VEHICLES_LOCATION = Uri.withAppendedPath(AUTHORITY_URI, AppDatabase.VEHICLES_LOCATION);

        @InexactContentUri(
                path = AppDatabase.VEHICLES_LOCATION + "/*",
                name = "VEHICLES_LOCATION_ID",
                type = BASE_TYPE_ITEM + AppDatabase.VEHICLES_LOCATION,
                whereColumn = VehicleLocationColumns.VEHICLE_ID,
                pathSegment = 1)
        public static Uri withId(long id) {
            return buildUri(AppDatabase.VEHICLES_LOCATION, String.valueOf(id));
        }

        public static long fromUri(Uri uri) {
            String lastSegment = uri.getLastPathSegment();
            if (AppDatabase.VEHICLES_LOCATION.equals(lastSegment)) {
                return 0;
            } else {
                return Long.valueOf(uri.getLastPathSegment());
            }
        }
    }

    @TableEndpoint(table = AppDatabase.VEHICLE_LOCATION_LISTS)
    public static final class VehicleLocationLists {
        @ContentUri(
                path = AppDatabase.VEHICLE_LOCATION_LISTS,
                type = BASE_TYPE_DIR + AppDatabase.VEHICLE_LOCATION_LISTS,
                defaultSort = VehicleLocationListColumns.USER_ID + " ASC")
        public static final Uri VEHICLE_LOCATION_LISTS = Uri.withAppendedPath(AUTHORITY_URI, AppDatabase.VEHICLE_LOCATION_LISTS);

        @InexactContentUri(
                path = AppDatabase.VEHICLE_LOCATION_LISTS + "/*",
                name = "VEHICLES_LOCATION_LISTS_ID",
                type = BASE_TYPE_ITEM + AppDatabase.VEHICLE_LOCATION_LISTS,
                whereColumn = VehicleLocationListColumns.USER_ID,
                pathSegment = 1)
        public static Uri withId(Integer id) {
            return buildUri(AppDatabase.VEHICLE_LOCATION_LISTS, String.valueOf(id));
        }

        public static long fromUri(Uri uri) {
            return Long.valueOf(uri.getLastPathSegment());
        }
    }

}

/*
 * The MIT License
 *
 * Copyright (c) 2013-2016 reark project contributors
 *
 * https://github.com/reark/reark/graphs/contributors
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package io.bifri.vehicletracker.data;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;

import io.bifri.vehicletracker.data.stores.NetworkRequestStatusStore;
import io.bifri.vehicletracker.data.stores.UserSettingsStore;
import io.bifri.vehicletracker.data.stores.UserVehiclesLocationStore;
import io.bifri.vehicletracker.data.stores.VehicleLocationStore;
import io.bifri.vehicletracker.data.stores.VehicleStore;
import io.bifri.vehicletracker.data.stores.VehicleUserStore;
import io.bifri.vehicletracker.data.stores.VehicleUsersStore;
import io.bifri.vehicletracker.data.stores.VehiclesStore;
import io.bifri.vehicletracker.network.NetworkService;
import io.bifri.vehicletracker.network.ServiceUri;

import static io.reark.reark.utils.Preconditions.checkNotNull;
import static io.reark.reark.utils.Preconditions.get;

public class DataLayer extends ClientDataLayerBase {

    private static final String TAG = DataLayer.class.getSimpleName();

    private final Context mContext;

    public DataLayer(
            @NonNull final Context context,
            @NonNull final UserSettingsStore userSettingsStore,
            @NonNull final NetworkRequestStatusStore networkRequestStatusStore,
            @NonNull final VehicleUserStore vehicleUserStore,
            @NonNull final VehicleUsersStore vehicleUsersStore,
            @NonNull final VehicleStore vehicleStore,
            @NonNull final VehiclesStore vehiclesStore,
            @NonNull final VehicleLocationStore vehicleLocationStore,
            @NonNull final UserVehiclesLocationStore userVehiclesLocationStore) {
        super(networkRequestStatusStore,
                vehicleUserStore,
                vehicleUsersStore,
                vehicleStore,
                vehiclesStore,
                vehicleLocationStore,
                userVehiclesLocationStore,
                userSettingsStore);

        checkNotNull(context);
        checkNotNull(userSettingsStore);

        this.mContext = context;
    }

    @Override
    protected void fetchVehicleUser(@NonNull final Integer vehicleUserId) {
        Intent intent = NetworkService.makeIntent(
                mContext, ServiceUri.VEHICLE_USER.uri(), get(vehicleUserId));
        mContext.startService(intent);
    }

    @Override
    protected void fetchVehicleUsers(@NonNull final String searchString) {
        Intent intent = NetworkService.makeIntent(
                mContext, ServiceUri.VEHICLE_USERS.uri(), get(searchString));
        mContext.startService(intent);
    }

    @Override
    protected void fetchUserVehiclesLocation(@NonNull final Integer userId) {
        Intent intent = NetworkService.makeIntent(
                mContext, ServiceUri.USER_VEHICLES_LOCATION.uri(), get(userId));
        mContext.startService(intent);
    }

}

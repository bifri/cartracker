/*
 * The MIT License
 *
 * Copyright (c) 2013-2016 reark project contributors
 *
 * https://github.com/reark/reark/graphs/contributors
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package io.bifri.vehicletracker.data.stores.cores;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.NonNull;

import com.google.gson.Gson;

import java.util.Date;
import java.util.List;

import io.bifri.vehicletracker.data.schematicProvider.DatabaseProvider;
import io.bifri.vehicletracker.data.schematicProvider.DatabaseProvider.VehicleUsers;
import io.bifri.vehicletracker.data.schematicProvider.VehicleUserColumns;
import io.bifri.vehicletracker.pojo.VehicleUser;
import io.bifri.vehicletracker.utils.DateUtils;
import io.reark.reark.data.stores.StoreItem;
import io.reark.reark.data.stores.cores.ContentProviderStoreCore;
import rx.Observable;

import static io.reark.reark.utils.Preconditions.checkNotNull;
import static io.reark.reark.utils.Preconditions.get;

public class VehicleUserStoreCore extends ContentProviderStoreCore<Integer, VehicleUser> {

    private final Gson mGson;

    public VehicleUserStoreCore(@NonNull final ContentResolver contentResolver,
                                @NonNull final Gson gson) {
        super(contentResolver);

        this.mGson = get(gson);
    }

    @NonNull
    public Observable<List<VehicleUser>> getAllCached() {
        return getAllOnce(getContentUri());
    }

    @NonNull
    public Observable<VehicleUser> getAllStream() {
        return getStream().map(StoreItem::item);
    }

    @NonNull
    @Override
    protected String getAuthority() {
        return DatabaseProvider.AUTHORITY;
    }

    @NonNull
    @Override
    public Uri getContentUri() {
        return VehicleUsers.VEHICLE_USERS;
    }

    @NonNull
    @Override
    protected String[] getProjection() {
        return new String[] {
                VehicleUserColumns.ID,
                VehicleUserColumns.JSON,
                VehicleUserColumns.UPDATED
        };
    }

    @NonNull
    @Override
    protected ContentValues getContentValuesForItem(@NonNull final VehicleUser item) {
        checkNotNull(item);

        ContentValues contentValues = new ContentValues();
        contentValues.put(VehicleUserColumns.ID, item.getId());
        contentValues.put(VehicleUserColumns.JSON, mGson.toJson(item));
        contentValues.put(VehicleUserColumns.UPDATED, DateUtils.toDbValue(item.getUpdateDate()));
        return contentValues;
    }

    @NonNull
    @Override
    protected VehicleUser read(@NonNull final Cursor cursor) {
        checkNotNull(cursor);

        final String json = cursor.getString(cursor.getColumnIndex(VehicleUserColumns.JSON));
        final Date updated = DateUtils.fromDbValue(
                cursor.getInt(cursor.getColumnIndex(VehicleUserColumns.UPDATED)));

        VehicleUser vehicleUser = mGson.fromJson(json, VehicleUser.class);
        vehicleUser = vehicleUser.withUpdateDate(updated);

        return vehicleUser;
    }

    @NonNull
    @Override
    protected VehicleUser mergeValues(@NonNull final VehicleUser v1,
                                      @NonNull final VehicleUser v2) {
        checkNotNull(v1);
        checkNotNull(v2);

        // Creating a new object to avoid overwriting the passed argument
        VehicleUser newValue = new VehicleUser(v1);

        return newValue.overwrite(v2);
    }

    @NonNull
    @Override
    public Uri getUriForId(@NonNull final Integer id) {
        checkNotNull(id);

        return DatabaseProvider.VehicleUsers.withId(id);
    }

    @NonNull
    @Override
    protected Integer getIdForUri(@NonNull final Uri uri) {
        checkNotNull(uri);

        return (int) DatabaseProvider.VehicleUsers.fromUri(uri);
    }

}

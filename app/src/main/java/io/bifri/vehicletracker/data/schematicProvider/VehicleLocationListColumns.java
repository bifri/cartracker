package io.bifri.vehicletracker.data.schematicProvider;

import net.simonvt.schematic.annotation.DataType;
import net.simonvt.schematic.annotation.PrimaryKey;

public interface VehicleLocationListColumns extends JsonColumns, UpdateColumns {

    @DataType(DataType.Type.TEXT) @PrimaryKey String USER_ID = "user_id";

}

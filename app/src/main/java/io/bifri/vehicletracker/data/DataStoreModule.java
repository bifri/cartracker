/*
 * The MIT License
 *
 * Copyright (c) 2013-2016 reark project contributors
 *
 * https://github.com/reark/reark/graphs/contributors
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package io.bifri.vehicletracker.data;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.bifri.vehicletracker.data.DataFunctions.FetchAndGetUserVehiclesLocation;
import io.bifri.vehicletracker.data.DataFunctions.FetchAndGetVehicleUser;
import io.bifri.vehicletracker.data.DataFunctions.FetchAndGetVehicleUsers;
import io.bifri.vehicletracker.data.DataFunctions.GetUserSettings;
import io.bifri.vehicletracker.data.DataFunctions.GetUserVehicles;
import io.bifri.vehicletracker.data.DataFunctions.GetUserVehiclesLocation;
import io.bifri.vehicletracker.data.DataFunctions.GetVehicle;
import io.bifri.vehicletracker.data.DataFunctions.GetVehicleLocation;
import io.bifri.vehicletracker.data.DataFunctions.GetVehicleUser;
import io.bifri.vehicletracker.data.DataFunctions.RepeatFetchAndGetUserVehiclesLocation;
import io.bifri.vehicletracker.data.DataFunctions.SetUserSettings;
import io.bifri.vehicletracker.data.stores.NetworkRequestStatusStore;
import io.bifri.vehicletracker.data.stores.StoreModule;
import io.bifri.vehicletracker.data.stores.UserSettingsStore;
import io.bifri.vehicletracker.data.stores.UserVehiclesLocationStore;
import io.bifri.vehicletracker.data.stores.VehicleLocationStore;
import io.bifri.vehicletracker.data.stores.VehicleStore;
import io.bifri.vehicletracker.data.stores.VehicleUserStore;
import io.bifri.vehicletracker.data.stores.VehicleUsersStore;
import io.bifri.vehicletracker.data.stores.VehiclesStore;
import io.bifri.vehicletracker.injections.ForApplication;
import io.bifri.vehicletracker.network.FetcherModule;
import io.bifri.vehicletracker.network.ServiceDataLayer;
import io.reark.reark.network.fetchers.UriFetcherManager;

@Module(includes = { FetcherModule.class, StoreModule.class })
public final class DataStoreModule {

    @Provides
    public GetUserSettings provideGetUserSettings(DataLayer dataLayer) {
        return dataLayer::getUserSettings;
    }

    @Provides
    public SetUserSettings provideSetUserSettings(DataLayer dataLayer) {
        return dataLayer::setUserSettings;
    }

    @Provides
    public FetchAndGetVehicleUser provideFetchAndGetVehicleUser(DataLayer dataLayer) {
        return dataLayer::fetchAndGetVehicleUser;
    }

    @Provides
    public FetchAndGetVehicleUsers provideFetchAndGetVehicleUsers(DataLayer dataLayer) {
        return dataLayer::dailyFetchAndGetVehicleUsers;
    }

    @Provides
    public GetVehicleUser provideGetVehicleUser(DataLayer dataLayer) {
        return dataLayer::getVehicleUser;
    }

    @Provides
    public GetUserVehicles provideGetUserVehicles(DataLayer dataLayer) {
        return dataLayer::getUserVehicles;
    }

    @Provides
    public GetVehicle provideGetVehicle(DataLayer dataLayer) {
        return dataLayer::getVehicle;
    }

    @Provides
    public GetVehicleLocation provideGetVehicleLocation(DataLayer dataLayer) {
        return dataLayer::getVehicleLocation;
    }

    @Provides
    public GetUserVehiclesLocation provideGetUserVehiclesLocation(DataLayer dataLayer) {
        return dataLayer::getUserVehiclesLocation;
    }

    @Provides
    public FetchAndGetUserVehiclesLocation provideFetchAndGetUserVehiclesLocation(DataLayer dataLayer) {
        return dataLayer::fetchAndGetUserVehiclesLocation;
    }

    @Provides
    public RepeatFetchAndGetUserVehiclesLocation provideRepeatFetchAndGetUserVehiclesLocation(DataLayer dataLayer) {
        return dataLayer::fetchAndGetUserVehiclesLocation;
    }

    @Provides
    @Singleton
    public DataLayer provideApplicationDataLayer(
            @ForApplication Context context,
            UserSettingsStore userSettingsStore,
            NetworkRequestStatusStore networkRequestStatusStore,
            VehicleUserStore vehicleUserStore,
            VehicleUsersStore vehicleUsersStore,
            VehicleStore vehicleStore,
            VehiclesStore vehiclesStore,
            VehicleLocationStore vehicleLocationStore,
            UserVehiclesLocationStore userVehiclesLocationStore) {
        return new DataLayer(
                context,
                userSettingsStore,
                networkRequestStatusStore,
                vehicleUserStore,
                vehicleUsersStore,
                vehicleStore,
                vehiclesStore,
                vehicleLocationStore,
                userVehiclesLocationStore);
    }

    @Provides
    @Singleton
    public ServiceDataLayer provideServiceDataLayer(
            UriFetcherManager fetcherManager,
            NetworkRequestStatusStore networkRequestStatusStore,
            VehicleUserStore vehicleUserStore,
            VehicleUsersStore vehicleUsersStore,
            VehicleStore vehicleStore,
            VehiclesStore vehiclesStore,
            VehicleLocationStore vehicleLocationStore,
            UserVehiclesLocationStore userVehiclesLocationStore) {
        return new ServiceDataLayer(
                fetcherManager,
                networkRequestStatusStore,
                vehicleUserStore,
                vehicleUsersStore,
                vehicleStore,
                vehiclesStore,
                vehicleLocationStore,
                userVehiclesLocationStore);
    }

}

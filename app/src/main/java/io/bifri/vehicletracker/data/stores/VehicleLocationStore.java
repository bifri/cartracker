package io.bifri.vehicletracker.data.stores;

import android.content.ContentResolver;
import android.support.annotation.NonNull;

import com.google.gson.Gson;

import io.bifri.vehicletracker.data.stores.cores.VehicleLocationStoreCore;
import io.bifri.vehicletracker.pojo.response.VehicleLocation;
import io.reark.reark.data.stores.DefaultStore;

public class VehicleLocationStore
        extends DefaultStore<Integer, VehicleLocation, VehicleLocation> {

    @NonNull
    private static final GetIdForItem<Integer, VehicleLocation> GET_ID_FOR_ITEM =
            VehicleLocation::getVehicleId;

    @NonNull
    private static final GetNullSafe<VehicleLocation, VehicleLocation> GET_NULL_SAFE =
            vehicleLocation -> vehicleLocation != null ? vehicleLocation : VehicleLocation.none();

    @NonNull
    private static final GetEmptyValue<VehicleLocation> GET_EMPTY_VALUE =
            VehicleLocation::none;

    public VehicleLocationStore(@NonNull final ContentResolver contentResolver,
                                @NonNull final Gson gson) {
        super(new VehicleLocationStoreCore(contentResolver, gson),
                GET_ID_FOR_ITEM,
                GET_NULL_SAFE,
                GET_EMPTY_VALUE);
    }

}

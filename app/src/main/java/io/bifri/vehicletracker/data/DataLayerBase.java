/*
 * The MIT License
 *
 * Copyright (c) 2013-2016 reark project contributors
 *
 * https://github.com/reark/reark/graphs/contributors
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package io.bifri.vehicletracker.data;

import android.support.annotation.NonNull;

import io.bifri.vehicletracker.pojo.ItemList;
import io.bifri.vehicletracker.pojo.response.Vehicle;
import io.bifri.vehicletracker.pojo.response.VehicleLocation;
import io.bifri.vehicletracker.pojo.VehicleUser;
import io.reark.reark.data.stores.interfaces.StoreInterface;
import io.reark.reark.pojo.NetworkRequestStatus;

import static io.reark.reark.utils.Preconditions.get;

public abstract class DataLayerBase {

    @NonNull protected final StoreInterface<Integer, NetworkRequestStatus, NetworkRequestStatus> mNetworkRequestStatusStore;
    @NonNull protected final StoreInterface<Integer, VehicleUser, VehicleUser> mVehicleUserStore;
    @NonNull protected final StoreInterface<String, ItemList<String>, ItemList<String>> mVehicleUsersStore;
    @NonNull protected final StoreInterface<Integer, Vehicle, Vehicle> mVehicleStore;
    @NonNull protected final StoreInterface<Integer, ItemList<Integer>, ItemList<Integer>> mVehiclesStore;
    @NonNull protected final StoreInterface<Integer, VehicleLocation, VehicleLocation> mVehicleLocationStore;
    @NonNull protected final StoreInterface<Integer, ItemList<Integer>, ItemList<Integer>> mVehiclesLocationStore;

    protected DataLayerBase(
            @NonNull final StoreInterface<Integer, NetworkRequestStatus, NetworkRequestStatus> networkRequestStatusStore,
            @NonNull final StoreInterface<Integer, VehicleUser, VehicleUser> vehicleUserStore,
            @NonNull final StoreInterface<String, ItemList<String>, ItemList<String>> vehicleUsersStore,
            @NonNull final StoreInterface<Integer, Vehicle, Vehicle> vehicleStore,
            @NonNull final StoreInterface<Integer, ItemList<Integer>, ItemList<Integer>> vehiclesStore,
            @NonNull final StoreInterface<Integer, VehicleLocation, VehicleLocation> vehicleLocationStore,
            @NonNull final StoreInterface<Integer, ItemList<Integer>, ItemList<Integer>> userVehiclesLocationStore) {
        this.mNetworkRequestStatusStore = get(networkRequestStatusStore);
        this.mVehicleUserStore = get(vehicleUserStore);
        this.mVehicleUsersStore = get(vehicleUsersStore);
        this.mVehicleStore = get(vehicleStore);
        this.mVehiclesStore = get(vehiclesStore);
        this.mVehicleLocationStore = get(vehicleLocationStore);
        this.mVehiclesLocationStore = get(userVehiclesLocationStore);
    }

}

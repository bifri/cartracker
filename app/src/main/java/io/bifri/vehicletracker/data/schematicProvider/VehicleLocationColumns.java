package io.bifri.vehicletracker.data.schematicProvider;

import net.simonvt.schematic.annotation.DataType;
import net.simonvt.schematic.annotation.PrimaryKey;

public interface VehicleLocationColumns extends JsonColumns, UpdateColumns {

    @DataType(DataType.Type.INTEGER) @PrimaryKey String VEHICLE_ID = "_id";

}

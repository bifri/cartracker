/*
 * The MIT License
 *
 * Copyright (c) 2013-2016 reark project contributors
 *
 * https://github.com/reark/reark/graphs/contributors
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package io.bifri.vehicletracker.data.stores;

import android.content.ContentResolver;
import android.support.annotation.NonNull;

import com.google.gson.Gson;

import io.bifri.vehicletracker.data.stores.cores.NetworkRequestStatusStoreCore;
import io.reark.reark.data.stores.DefaultStore;
import io.reark.reark.pojo.NetworkRequestStatus;

public class NetworkRequestStatusStore
        extends DefaultStore<Integer, NetworkRequestStatus, NetworkRequestStatus> {

    @NonNull
    private static final GetIdForItem<Integer, NetworkRequestStatus> GET_ID_FOR_ITEM =
            status -> status.getUri().hashCode();

    @NonNull
    private static final GetNullSafe<NetworkRequestStatus, NetworkRequestStatus> GET_NULL_SAFE =
            status -> status == null ? NetworkRequestStatus.none() : status;

    @NonNull
    private static final GetEmptyValue<NetworkRequestStatus> GET_EMPTY_VALUE =
            NetworkRequestStatus::none;

    public NetworkRequestStatusStore(@NonNull final ContentResolver contentResolver, @NonNull final Gson gson) {
        super(new NetworkRequestStatusStoreCore(contentResolver, gson),
                GET_ID_FOR_ITEM,
                GET_NULL_SAFE,
                GET_EMPTY_VALUE);
    }

}

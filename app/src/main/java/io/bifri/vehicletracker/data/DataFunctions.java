/*
 * The MIT License
 *
 * Copyright (c) 2013-2016 reark project contributors
 *
 * https://github.com/reark/reark/graphs/contributors
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package io.bifri.vehicletracker.data;

import android.support.annotation.NonNull;

import io.bifri.vehicletracker.pojo.ItemList;
import io.bifri.vehicletracker.pojo.AppUserSettings;
import io.bifri.vehicletracker.pojo.VehicleUser;
import io.bifri.vehicletracker.pojo.response.Vehicle;
import io.bifri.vehicletracker.pojo.response.VehicleLocation;
import io.reark.reark.data.DataStreamNotification;
import rx.Observable;
import rx.functions.Func1;

public class DataFunctions {

    public interface GetUserSettings {
        @NonNull
        Observable<AppUserSettings> call();
    }

    public interface SetUserSettings {
        void call(@NonNull final AppUserSettings appUserSettings);
    }

    public interface GetVehicleUser {
        @NonNull
        Observable<VehicleUser> call(int vehicleUserId);
    }

    public interface FetchAndGetVehicleUser extends GetVehicleUser {

    }

    public interface GetVehicleUsers {
        @NonNull
        Observable<DataStreamNotification<ItemList<String>>> call(@NonNull final String search);
    }

    public interface FetchAndGetVehicleUsers extends GetVehicleUsers {

    }

    public interface GetUserVehicles {
        @NonNull
        Observable<DataStreamNotification<ItemList<Integer>>> call(@NonNull final Integer userId);
    }

    public interface GetVehicle {
        @NonNull
        Observable<Vehicle> call(int vehicleId);
    }

    public interface GetVehicles {
        @NonNull
        Observable<DataStreamNotification<ItemList<Integer>>> call(@NonNull final Integer userId);
    }

    public interface GetVehicleLocation {
        @NonNull
        Observable<VehicleLocation> call(int vehicleId);
    }

    public interface GetUserVehiclesLocation {
        @NonNull
        Observable<DataStreamNotification<ItemList<Integer>>> call(final int userId);
    }

    public interface FetchAndGetUserVehiclesLocation extends GetUserVehiclesLocation {

    }

    public interface RepeatFetchAndGetUserVehiclesLocation {
        @NonNull
        Observable<DataStreamNotification<ItemList<Integer>>> call(
                int vehicleId,
                Func1<Observable<? extends Void>, Observable<?>> repeatFrequency);
    }

}

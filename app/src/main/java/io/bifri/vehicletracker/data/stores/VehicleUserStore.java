/*
 * The MIT License
 *
 * Copyright (c) 2013-2016 reark project contributors
 *
 * https://github.com/reark/reark/graphs/contributors
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package io.bifri.vehicletracker.data.stores;

import android.content.ContentResolver;
import android.support.annotation.NonNull;

import com.google.gson.Gson;

import io.bifri.vehicletracker.data.stores.cores.VehicleUserStoreCore;
import io.bifri.vehicletracker.pojo.VehicleUser;
import io.reark.reark.data.stores.DefaultStore;

public class VehicleUserStore
        extends DefaultStore<Integer, VehicleUser, VehicleUser> {

    @NonNull
    private static final GetIdForItem<Integer, VehicleUser> GET_ID_FOR_ITEM =
            VehicleUser::getId;

    @NonNull
    private static final GetNullSafe<VehicleUser, VehicleUser> GET_NULL_SAFE =
            vehicleUser -> vehicleUser != null ? vehicleUser : VehicleUser.none();

    @NonNull
    private static final GetEmptyValue<VehicleUser> GET_EMPTY_VALUE =
            VehicleUser::none;

    public VehicleUserStore(@NonNull final ContentResolver contentResolver, @NonNull final Gson gson) {
        super(new VehicleUserStoreCore(contentResolver, gson),
                GET_ID_FOR_ITEM,
                GET_NULL_SAFE,
                GET_EMPTY_VALUE);
    }

}

package io.bifri.vehicletracker.data.schematicProvider;

public interface VehicleColumns extends IdColumns, JsonColumns, UpdateColumns {

}

package io.bifri.vehicletracker.data.schematicProvider;

import net.simonvt.schematic.annotation.DataType;

public interface UpdateColumns {

    // Date of the last data change
    @DataType(DataType.Type.INTEGER) String UPDATED = "updated";

}

/*
 * The MIT License
 *
 * Copyright (c) 2013-2016 reark project contributors
 *
 * https://github.com/reark/reark/graphs/contributors
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package io.bifri.vehicletracker.data.stores.cores;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Date;

import io.bifri.vehicletracker.data.schematicProvider.DatabaseProvider;
import io.bifri.vehicletracker.data.schematicProvider.VehicleUserListColumns;
import io.bifri.vehicletracker.pojo.ItemList;
import io.bifri.vehicletracker.utils.DateUtils;
import io.reark.reark.data.stores.cores.ContentProviderStoreCore;

import static io.reark.reark.utils.Preconditions.checkNotNull;
import static io.reark.reark.utils.Preconditions.get;

public class VehicleUsersStoreCore extends ContentProviderStoreCore<String, ItemList<String>> {

    private final Gson mGson;

    public VehicleUsersStoreCore(@NonNull final ContentResolver contentResolver, @NonNull final Gson gson) {
        super(contentResolver);

        this.mGson = get(gson);
    }

    @NonNull
    @Override
    protected String getAuthority() {
        return DatabaseProvider.AUTHORITY;
    }

    @NonNull
    @Override
    public Uri getContentUri() {
        return DatabaseProvider.VehicleUserLists.VEHICLE_USER_LISTS;
    }

    @NonNull
    @Override
    protected String[] getProjection() {
        return new String[] {
                VehicleUserListColumns.ID,
                VehicleUserListColumns.JSON,
                VehicleUserListColumns.UPDATED
        };
    }

    @NonNull
    @Override
    protected ContentValues getContentValuesForItem(@NonNull final ItemList<String> item) {
        checkNotNull(item);

        ContentValues contentValues = new ContentValues();
        contentValues.put(VehicleUserListColumns.ID, item.key());
        contentValues.put(VehicleUserListColumns.JSON, mGson.toJson(item));
        contentValues.put(VehicleUserListColumns.UPDATED, DateUtils.toDbValue(item.updateDate()));
        return contentValues;
    }

    @NonNull
    @Override
    protected ItemList<String> read(@NonNull final Cursor cursor) {
        checkNotNull(cursor);

        final String json = cursor.getString(cursor.getColumnIndex(VehicleUserListColumns.JSON));
        final Date updated = DateUtils.fromDbValue(
                cursor.getLong(cursor.getColumnIndex(VehicleUserListColumns.UPDATED)));

        Type listType = new TypeToken<ItemList<String>>(){}.getType();
        ItemList<String> vehicleUserList = mGson.fromJson(json, listType);
        vehicleUserList = vehicleUserList.withUpdateDate(updated);

        return vehicleUserList;
    }

    @NonNull
    @Override
    public Uri getUriForId(@NonNull final String id) {
        checkNotNull(id);

        return DatabaseProvider.VehicleUserLists.withId(id);
    }

    @NonNull
    @Override
    protected String getIdForUri(@NonNull final Uri uri) {
        checkNotNull(uri);

        return DatabaseProvider.VehicleUserLists.fromUri(uri);
    }

}

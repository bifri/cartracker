/*
 * The MIT License
 *
 * Copyright (c) 2013-2016 reark project contributors
 *
 * https://github.com/reark/reark/graphs/contributors
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package io.bifri.vehicletracker.data.stores;

import android.content.ContentResolver;

import com.google.gson.Gson;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.bifri.vehicletracker.data.DataLayer;

@Module
public final class StoreModule {

    @Provides
    @Singleton
    public UserSettingsStore provideUserSettingsStore(ContentResolver contentResolver, Gson gson) {
        return new UserSettingsStore(DataLayer.DEFAULT_USER_ID, contentResolver, gson);
    }

    @Provides
    @Singleton
    public NetworkRequestStatusStore provideNetworkRequestStatusStore(ContentResolver contentResolver, Gson gson) {
        return new NetworkRequestStatusStore(contentResolver, gson);
    }

    @Provides
    @Singleton
    public VehicleUserStore provideVehicleUserStore(ContentResolver contentResolver, Gson gson) {
        return new VehicleUserStore(contentResolver, gson);
    }

    @Provides
    @Singleton
    public VehicleUsersStore provideVehicleUsersStore(ContentResolver contentResolver, Gson gson) {
        return new VehicleUsersStore(contentResolver, gson);
    }

    @Provides
    @Singleton
    public VehicleStore provideVehicleStore(ContentResolver contentResolver, Gson gson) {
        return new VehicleStore(contentResolver, gson);
    }

    @Provides
    @Singleton
    public VehiclesStore provideVehiclesStore(ContentResolver contentResolver, Gson gson) {
        return new VehiclesStore(contentResolver, gson);
    }

    @Provides
    @Singleton
    public VehicleLocationStore provideVehicleLocationStore(ContentResolver contentResolver, Gson gson) {
        return new VehicleLocationStore(contentResolver, gson);
    }

    @Provides
    @Singleton
    public UserVehiclesLocationStore provideUserVehiclesLocationStore(ContentResolver contentResolver, Gson gson) {
        return new UserVehiclesLocationStore(contentResolver, gson);
    }

}

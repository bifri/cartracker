/*
 * The MIT License
 *
 * Copyright (c) 2013-2016 reark project contributors
 *
 * https://github.com/reark/reark/graphs/contributors
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package io.bifri.vehicletracker.viewmodels;

import android.content.Context;

import dagger.Module;
import dagger.Provides;
import io.bifri.vehicletracker.data.DataFunctions.FetchAndGetVehicleUser;
import io.bifri.vehicletracker.data.DataFunctions.FetchAndGetVehicleUsers;
import io.bifri.vehicletracker.data.DataFunctions.GetUserSettings;
import io.bifri.vehicletracker.data.DataFunctions.GetVehicle;
import io.bifri.vehicletracker.data.DataFunctions.GetVehicleLocation;
import io.bifri.vehicletracker.data.DataFunctions.GetVehicleUser;
import io.bifri.vehicletracker.data.DataFunctions.RepeatFetchAndGetUserVehiclesLocation;
import io.bifri.vehicletracker.injections.ForApplication;
import pl.charmas.android.reactivelocation.ReactiveLocationProvider;

@Module
public class ViewModelModule {

    @Provides
    public VehicleUsersViewModel provideVehicleUsersViewModel(
            FetchAndGetVehicleUsers vehicleUsers,
            GetVehicleUser vehicleUser) {
        return new VehicleUsersViewModel(vehicleUsers, vehicleUser);
    }

    @Provides
    public VehicleUserViewModel provideVehicleUserViewModel(
            GetUserSettings getUserSettings,
            FetchAndGetVehicleUser fetchAndGetVehicleUser) {
        return new VehicleUserViewModel(getUserSettings, fetchAndGetVehicleUser);
    }

    @Provides
    public UserVehiclesLocationViewModel provideUserVehiclesLocationViewModel(
            RepeatFetchAndGetUserVehiclesLocation repeatGetFetchAndGetUserVehiclesLocation,
            GetVehicle getUserVehicle,
            GetVehicleLocation getVehicleLocation) {
        return new UserVehiclesLocationViewModel(
                repeatGetFetchAndGetUserVehiclesLocation, getUserVehicle, getVehicleLocation);
    }

    @Provides
    public LocationServicesViewModel provideLocationServicesViewModel(
            @ForApplication Context appContext,
            ReactiveLocationProvider locationProvider) {

        return new LocationServicesViewModel(appContext, locationProvider);
    }

}

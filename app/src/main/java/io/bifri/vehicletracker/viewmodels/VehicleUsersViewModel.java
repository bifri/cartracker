/*
 * The MIT License
 *
 * Copyright (c) 2013-2016 reark project contributors
 *
 * https://github.com/reark/reark/graphs/contributors
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package io.bifri.vehicletracker.viewmodels;

import android.support.annotation.NonNull;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.bifri.vehicletracker.data.DataFunctions.FetchAndGetVehicleUsers;
import io.bifri.vehicletracker.data.DataFunctions.GetVehicleUser;
import io.bifri.vehicletracker.pojo.ItemList;
import io.bifri.vehicletracker.pojo.VehicleUser;
import io.bifri.vehicletracker.utils.rx.RxState;
import io.bifri.vehicletracker.utils.rx.StartWith;
import io.reark.reark.data.DataStreamNotification;
import io.reark.reark.utils.Log;
import io.reark.reark.utils.RxUtils;
import io.reark.reark.viewmodels.AbstractViewModel;
import rx.Observable;
import rx.functions.Func1;
import rx.observables.ConnectableObservable;
import rx.subjects.PublishSubject;
import rx.subscriptions.CompositeSubscription;

import static io.reark.reark.utils.Preconditions.checkNotNull;
import static io.reark.reark.utils.Preconditions.get;
import static rx.android.schedulers.AndroidSchedulers.mainThread;

public class VehicleUsersViewModel extends AbstractViewModel {

    private static final String TAG = VehicleUsersViewModel.class.getSimpleName();

    public enum ProgressStatus {
        LOADING, ERROR, IDLE
    }

    private static final int SEARCH_INPUT_DELAY = 500;

    @NonNull private final FetchAndGetVehicleUsers mFetchGetVehicleUsers;
    @NonNull private final GetVehicleUser mGetVehicleUser;

    @NonNull private final PublishSubject<String> mInStreamOfSearchString = PublishSubject.create();
    @NonNull private final PublishSubject<VehicleUser> mOutStreamOfSelectedVehicleUser = PublishSubject.create();

    @NonNull private final RxState<List<VehicleUser>> mOutStreamOfVehicleUsers =
            new RxState<>(null, mainThread());
    @NonNull private final RxState<ProgressStatus> mOutStreamNetworkRequestStatusText =
            new RxState<>(null, mainThread());

    public VehicleUsersViewModel(@NonNull final FetchAndGetVehicleUsers fetchAndGetVehicleUsers,
                                 @NonNull final GetVehicleUser getVehicleUser) {
        mFetchGetVehicleUsers = get(fetchAndGetVehicleUsers);
        mGetVehicleUser = get(getVehicleUser);
    }

    @NonNull
    public Observable<VehicleUser> getStreamOfSelectedVehicleUser() {
        return mOutStreamOfSelectedVehicleUser.asObservable();
    }

    @NonNull
    public Observable<List<VehicleUser>> getStreamVehicleUsers() {
        return mOutStreamOfVehicleUsers.values(StartWith.NO);
    }

    @NonNull
    public Observable<ProgressStatus> getStreamOfNetworkRequestStatusText() {
        return mOutStreamNetworkRequestStatusText.values(StartWith.NO);
    }

    public void setSearchString(@NonNull final String searchString) {
        checkNotNull(searchString);

        mInStreamOfSearchString.onNext(searchString);
    }

    public void selectedVehicleUser(@NonNull final VehicleUser vehicleUser) {
        checkNotNull(vehicleUser);

        mOutStreamOfSelectedVehicleUser.onNext(vehicleUser);
    }

    @NonNull
    static Func1<DataStreamNotification<ItemList<String>>, ProgressStatus> toProgressStatus() {
        return notification -> {
            if (notification.isFetchingStart()) {
                return ProgressStatus.LOADING;
            } else if (notification.isFetchingError()) {
                return ProgressStatus.ERROR;
            } else {
                return ProgressStatus.IDLE;
            }
        };
    }

    @Override
    public void subscribeToDataStoreInternal(@NonNull final CompositeSubscription compositeSubscription) {
        checkNotNull(compositeSubscription);
        Log.v(TAG, "subscribeToDataStoreInternal");

        ConnectableObservable<DataStreamNotification<ItemList<String>>> vehicleUsersSource =
                mInStreamOfSearchString
                        .debounce(SEARCH_INPUT_DELAY, TimeUnit.MILLISECONDS)
                        .distinctUntilChanged()
                        .filter(value -> value.length() > 2)
                        .doOnNext(value -> Log.d(TAG, "Searching with: " + value))
                        .switchMap(mFetchGetVehicleUsers::call)
                        .publish();

        compositeSubscription.add(vehicleUsersSource
                .map(toProgressStatus())
                .doOnNext(progressStatus -> Log.d(TAG, "Progress status: " + progressStatus.name()))
                .subscribe(this::setNetworkStatusText)
        );

        compositeSubscription.add(vehicleUsersSource
                .filter(DataStreamNotification::isOnNext)
                .map(DataStreamNotification::getValue)
                .map(ItemList::items)
                .flatMap(toVehicleUserList())
                .doOnNext(list -> Log.d(TAG, "Publishing " + list.size() + " vehicleUsers from the ViewModel"))
                .subscribe(vehicleUsers -> mOutStreamOfVehicleUsers.apply(oldVehicleUsers -> vehicleUsers))
        );

        compositeSubscription.add(vehicleUsersSource.connect());
    }

    @NonNull
    Func1<List<Integer>, Observable<List<VehicleUser>>> toVehicleUserList() {
        return vehicleUserIds -> Observable.from(vehicleUserIds)
                .map(this::getVehicleUserObservable)
                .toList()
                .flatMap(RxUtils::toObservableList);
    }

    @NonNull
    Observable<VehicleUser> getVehicleUserObservable(@NonNull final Integer vehicleUserId) {
        checkNotNull(vehicleUserId);

        return mGetVehicleUser
                .call(vehicleUserId)
                .doOnNext((vehicleUser) -> Log.v(TAG, "Received vehicle user " + vehicleUser.getId()));
    }

    void setNetworkStatusText(@NonNull final ProgressStatus status) {
        checkNotNull(status);

        mOutStreamNetworkRequestStatusText.apply(oldStatus -> status);
    }

}

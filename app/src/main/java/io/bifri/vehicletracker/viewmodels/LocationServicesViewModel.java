/*
 * The MIT License
 *
 * Copyright (c) 2013-2016 reark project contributors
 *
 * https://github.com/reark/reark/graphs/contributors
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package io.bifri.vehicletracker.viewmodels;

import android.content.Context;
import android.location.Address;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.v4.util.Pair;

import com.fernandocejas.frodo.annotation.RxLogObservable;
import com.github.polok.routedrawer.RouteRest;
import com.github.polok.routedrawer.model.Routes;
import com.github.polok.routedrawer.model.TravelMode;
import com.github.polok.routedrawer.parser.RouteJsonParser;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.model.LatLng;

import java.util.List;
import java.util.Locale;

import io.bifri.vehicletracker.utils.rx.RxState;
import io.bifri.vehicletracker.utils.rx.StartWith;
import io.reark.reark.utils.Log;
import io.reark.reark.viewmodels.AbstractViewModel;
import java8.util.Optional;
import pl.charmas.android.reactivelocation.ReactiveLocationProvider;
import rx.Observable;
import rx.subscriptions.CompositeSubscription;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.content.pm.PackageManager.PERMISSION_GRANTED;
import static android.support.v4.app.ActivityCompat.checkSelfPermission;
import static android.text.format.DateUtils.MINUTE_IN_MILLIS;
import static io.reark.reark.utils.Preconditions.checkNotNull;
import static io.reark.reark.utils.Preconditions.get;
import static rx.android.schedulers.AndroidSchedulers.mainThread;
import static rx.schedulers.Schedulers.io;

public class LocationServicesViewModel extends AbstractViewModel {

    private static final String TAG = LocationServicesViewModel.class.getSimpleName();

    public static final String DEFAULT_PROVIDER = "MobiApi";
    private static final int DEFAULT_MAX_ADDRESSES = 1;
    private static final int DEFAULT_PRIORITY = LocationRequest.PRIORITY_HIGH_ACCURACY;
    private static final long DEFAULT_INTERVAL_IN_MILLIS = MINUTE_IN_MILLIS;
    private static final TravelMode DEFAULT_TRAVEL_MODE = TravelMode.DRIVING;
    private static final Address EMPTY_ADDRESS = new Address(Locale.ROOT);
    public static final Routes EMPTY_ROUTES = new Routes();

    public enum ProgressStatus {
        LOADING, ERROR, IDLE
    }

    @NonNull private final Context mAppContext;

    @NonNull private final ReactiveLocationProvider mLocationProvider;
    @NonNull private final RxState<Location> mInStreamRequestForAddress = new RxState<>(null, io());
    @NonNull private final RxState<Optional<Address>> mOutStreamOfAddresses =
            new RxState<>(Optional.empty(), mainThread());
    @NonNull private final RxState<Optional<Location>> mOutStreamOfCurrentLocation =
            new RxState<>(Optional.empty(), mainThread());
    @NonNull private final RxState<LatLng> mInStreamRequestForRoute = new RxState<>(null, io());
    @NonNull private final RxState<Optional<Routes>> mOutStreamOfRoutes =
            new RxState<>(Optional.empty(), mainThread());
    @NonNull private final RxState<ProgressStatus> mOutStreamOfNetworkRequestStatusText =
            new RxState<>(null, mainThread());

    public LocationServicesViewModel(
            @NonNull final Context appContext,
            @NonNull final ReactiveLocationProvider locationProvider) {
        this.mAppContext = appContext;
        this.mLocationProvider = get(locationProvider);
    }

    public Observable<Address> getStreamOfAddress() {
        return mOutStreamOfAddresses.values(StartWith.SCHEDULE)
                .filter(Optional::isPresent)
                .map(Optional::get);
    }

    public void setRequestForAddress(@NonNull Location location) {
        checkNotNull(location);

        mInStreamRequestForAddress.apply(__ -> location);
    }

    public Observable<Location> getStreamOfCurrentLocation() {
        return mOutStreamOfCurrentLocation.values(StartWith.SCHEDULE)
                .filter(Optional::isPresent)
                .map(Optional::get);
    }

    private @NonNull Observable<List<Address>> getAddressStream(Location location) {
        return mLocationProvider.getReverseGeocodeObservable(
                location.getLatitude(), location.getLongitude(), DEFAULT_MAX_ADDRESSES);
    }

    @RxLogObservable
    public Observable<Routes> getStreamOfRoutes() {
        return mOutStreamOfRoutes.values(StartWith.SCHEDULE)
                .filter(Optional::isPresent)
                .map(Optional::get);
    }

    @RxLogObservable
    public void setRequestForRoute(@NonNull LatLng latLng) {
        checkNotNull(latLng);

        mInStreamRequestForRoute.apply(__ -> latLng);
    }

    @RxLogObservable
    private @NonNull Observable<Routes> getRoutesStream(LatLng origin, LatLng destination) {
        return new RouteRest().getJsonDirections(origin, destination, DEFAULT_TRAVEL_MODE)
                .map(s -> {
                    Log.d(TAG, s);
                    return new RouteJsonParser<Routes>().parse(s, Routes.class);
                });
    }

    @RxLogObservable
    private @NonNull Observable<Location> getCurrentLocationStream() {
        if (checkSelfPermission(mAppContext, ACCESS_FINE_LOCATION) != PERMISSION_GRANTED
                && checkSelfPermission(mAppContext, ACCESS_COARSE_LOCATION) != PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return Observable.empty();
        }
        return mLocationProvider.getUpdatedLocation(locationRequest())
                .startWith(mLocationProvider.getLastKnownLocation().take(1));
    }

    protected LocationRequest locationRequest() {
        return LocationRequest.create()
                .setPriority(DEFAULT_PRIORITY)
                .setInterval(DEFAULT_INTERVAL_IN_MILLIS);
    }

    @Override
    public void subscribeToDataStoreInternal(@NonNull final CompositeSubscription compositeSubscription) {
        checkNotNull(compositeSubscription);
        Log.v(TAG, "subscribeToDataStoreInternal");

        compositeSubscription.add(mInStreamRequestForAddress.values(StartWith.NO)
                .switchMap(this::getAddressStream)
                .map(list -> {
                    if (list == null || list.size() == 0 || list.get(0) == null) {
                        return EMPTY_ADDRESS;
                    }
                    return list.get(0);
                })
                .subscribe(address -> mOutStreamOfAddresses.apply(__ -> Optional.of(address)),
                        e -> Log.e(TAG, "Error getting address", e)));

        compositeSubscription.add(mInStreamRequestForRoute.values(StartWith.NO)
                .switchMap(origin ->
                        mOutStreamOfCurrentLocation.values(StartWith.SCHEDULE)
                                .filter(Optional::isPresent)
                                .map(Optional::get)
                                .map(location -> new LatLng(location.getLatitude(), location.getLongitude()))
                                .map(destination -> new Pair<>(origin, destination)))
                .switchMap(pair -> getRoutesStream(pair.first, pair.second))
                .map(routes -> {
                    if (routes == null
                            || routes.status == null
                            || !routes.status.equals(Routes.STATUS_OK)
                            || routes.routes == null
                            || routes.routes.size() == 0
                            || routes.routes.get(0) == null) {
                        return EMPTY_ROUTES;
                    }
                    return routes;
                })
                .subscribe(routes -> mOutStreamOfRoutes.apply(__ -> Optional.of(routes)),
                        e -> Log.e(TAG, "Error getting routes", e)));

        compositeSubscription.add(getCurrentLocationStream()
                .subscribe(location ->
                        mOutStreamOfCurrentLocation.apply(__ -> Optional.of(location))));
    }

}

/*
 * The MIT License
 *
 * Copyright (c) 2013-2016 reark project contributors
 *
 * https://github.com/reark/reark/graphs/contributors
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package io.bifri.vehicletracker.viewmodels;

import android.support.annotation.NonNull;
import android.support.v4.util.Pair;
import android.text.format.DateUtils;

import com.fernandocejas.frodo.annotation.RxLogObservable;

import java.util.List;

import io.bifri.vehicletracker.data.DataFunctions.GetVehicle;
import io.bifri.vehicletracker.data.DataFunctions.GetVehicleLocation;
import io.bifri.vehicletracker.data.DataFunctions.RepeatFetchAndGetUserVehiclesLocation;
import io.bifri.vehicletracker.pojo.ItemList;
import io.bifri.vehicletracker.pojo.response.Vehicle;
import io.bifri.vehicletracker.pojo.response.VehicleLocation;
import io.bifri.vehicletracker.utils.rx.RepeatWithDelay;
import io.bifri.vehicletracker.utils.rx.RxState;
import io.bifri.vehicletracker.utils.rx.StartWith;
import io.reark.reark.data.DataStreamNotification;
import io.reark.reark.utils.Log;
import io.reark.reark.utils.RxUtils;
import io.reark.reark.viewmodels.AbstractViewModel;
import rx.Observable;
import rx.functions.Func1;
import rx.observables.ConnectableObservable;
import rx.subjects.BehaviorSubject;
import rx.subjects.PublishSubject;
import rx.subscriptions.CompositeSubscription;

import static io.bifri.vehicletracker.utils.rx.DelayStrategy.CONSTANT_DELAY;
import static io.reark.reark.utils.Preconditions.checkNotNull;
import static io.reark.reark.utils.Preconditions.get;
import static rx.android.schedulers.AndroidSchedulers.mainThread;

public class UserVehiclesLocationViewModel extends AbstractViewModel {

    private static final String TAG = UserVehiclesLocationViewModel.class.getSimpleName();

    private static final long LOCATION_FETCH_FREQUENCY_IN_MILLIS = DateUtils.MINUTE_IN_MILLIS;

    public enum ProgressStatus {
        LOADING, ERROR, IDLE
    }

    @NonNull private final RepeatFetchAndGetUserVehiclesLocation mFetchAndGetGetUserVehiclesLocation;
    @NonNull private final GetVehicle mGetVehicle;
    @NonNull private final GetVehicleLocation mGetVehicleLocation;

    @NonNull private final BehaviorSubject<Integer> mInStreamOfUserId = BehaviorSubject.create();
    @NonNull private final PublishSubject<VehicleLocation> mOutStreamOfSelectedVehicleLocation =
            PublishSubject.create();

    @NonNull private final RxState<List<Pair<Vehicle, VehicleLocation>>> mOutStreamOfVehicleLocationList =
            new RxState<>(null, mainThread());
    @NonNull private final RxState<ProgressStatus> mOutStreamOfNetworkRequestStatusText =
            new RxState<>(null, mainThread());

    public UserVehiclesLocationViewModel(
            @NonNull final RepeatFetchAndGetUserVehiclesLocation fetchAndGetUserVehiclesLocation,
            @NonNull final GetVehicle getVehicle,
            @NonNull final GetVehicleLocation getVehicleLocation) {
        this.mFetchAndGetGetUserVehiclesLocation = get(fetchAndGetUserVehiclesLocation);
        this.mGetVehicle = get(getVehicle);
        this.mGetVehicleLocation = get(getVehicleLocation);
    }

    @NonNull
    public Observable<VehicleLocation> getStreamOfSelectedVehicleLocation() {
        return mOutStreamOfSelectedVehicleLocation.asObservable();
    }

    @RxLogObservable
    @NonNull
    public Observable<List<Pair<Vehicle, VehicleLocation>>> getStreamOfVehicleLocationLists() {
        return mOutStreamOfVehicleLocationList.values(StartWith.NO);
    }

    @NonNull
    public Observable<ProgressStatus> getStreamOfNetworkRequestStatusText() {
        return mOutStreamOfNetworkRequestStatusText.values(StartWith.NO);
    }

    public void setVehicleUser(@NonNull final Integer vehicleUserId) {
        checkNotNull(vehicleUserId);
        if (vehicleUserId == -1) {
            return;
        }

        mInStreamOfUserId.onNext(vehicleUserId);
    }

    public void selectedVehicleUserLocation(@NonNull final VehicleLocation vehicleLocation) {
        checkNotNull(vehicleLocation);

        mOutStreamOfSelectedVehicleLocation.onNext(vehicleLocation);
    }

    @NonNull
    static Func1<DataStreamNotification<ItemList<Integer>>, ProgressStatus> toProgressStatus() {
        return notification -> {
            if (notification.isFetchingStart()) {
                return ProgressStatus.LOADING;
            } else if (notification.isFetchingError()) {
                return ProgressStatus.ERROR;
            } else {
                return ProgressStatus.IDLE;
            }
        };
    }

    @Override
    public void subscribeToDataStoreInternal(@NonNull final CompositeSubscription compositeSubscription) {
        checkNotNull(compositeSubscription);
        Log.v(TAG, "subscribeToDataStoreInternal");

        ConnectableObservable<DataStreamNotification<ItemList<Integer>>> userVehiclesLocationSource =
                mInStreamOfUserId
                        .doOnNext(value -> Log.d(TAG, "Searching with userId: " + value))
                        .switchMap(userId -> mFetchAndGetGetUserVehiclesLocation.call(
                                userId,
                                new RepeatWithDelay(LOCATION_FETCH_FREQUENCY_IN_MILLIS, CONSTANT_DELAY))
)                        .publish();

        compositeSubscription.add(userVehiclesLocationSource
                .map(toProgressStatus())
                .doOnNext(progressStatus -> Log.d(TAG, "Progress status: " + progressStatus.name()))
                .subscribe(this::setNetworkStatusText)
        );

        compositeSubscription.add(userVehiclesLocationSource
                .filter(DataStreamNotification::isOnNext)
                .map(DataStreamNotification::getValue)
                .map(ItemList::items)
                .flatMap(toVehicleLocationList())
                .doOnNext(list -> Log.d(TAG, "Publishing " + list.size()
                        + " user vehicles location from the ViewModel"))
                .subscribe(vehicleLocations ->
                        mOutStreamOfVehicleLocationList.apply(oldVehicleLocations -> vehicleLocations))
        );

        compositeSubscription.add(userVehiclesLocationSource.connect());
    }

    @NonNull
    Func1<List<Integer>, Observable<List<Pair<Vehicle, VehicleLocation>>>> toVehicleLocationList() {
        return vehicleIds -> Observable.from(vehicleIds)
                .map(this::getVehicleLocationObservable)
                .toList()
                .flatMap(RxUtils::toObservableList);
    }

    @NonNull
    Observable<Pair<Vehicle, VehicleLocation>> getVehicleLocationObservable(
            @NonNull final Integer vehicleId) {
        checkNotNull(vehicleId);

        return Observable.combineLatest(
                mGetVehicle.call(vehicleId),
                mGetVehicleLocation.call(vehicleId),
                Pair::new)
                .doOnNext(pair -> Log.v(TAG, "Received location of vehicle " + vehicleId));
    }

    void setNetworkStatusText(@NonNull final ProgressStatus status) {
        checkNotNull(status);

        mOutStreamOfNetworkRequestStatusText.apply(oldStatus -> status);
    }

}

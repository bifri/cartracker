/*
 * The MIT License
 *
 * Copyright (c) 2013-2016 reark project contributors
 *
 * https://github.com/reark/reark/graphs/contributors
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package io.bifri.vehicletracker.viewmodels;

import android.support.annotation.NonNull;

import io.bifri.vehicletracker.data.DataFunctions.FetchAndGetVehicleUser;
import io.bifri.vehicletracker.data.DataFunctions.GetUserSettings;
import io.bifri.vehicletracker.pojo.VehicleUser;
import io.bifri.vehicletracker.utils.rx.RxState;
import io.bifri.vehicletracker.utils.rx.StartWith;
import io.reark.reark.viewmodels.AbstractViewModel;
import rx.Observable;
import rx.subscriptions.CompositeSubscription;

import static io.reark.reark.utils.Preconditions.checkNotNull;
import static io.reark.reark.utils.Preconditions.get;
import static rx.android.schedulers.AndroidSchedulers.mainThread;

public class VehicleUserViewModel extends AbstractViewModel {

    @NonNull private final GetUserSettings mGetUserSettings;
    @NonNull private final FetchAndGetVehicleUser mFetchAndGetVehicleUser;

    @NonNull private final RxState<VehicleUser> mOutStreamOfVehicleUser = new RxState<>(null, mainThread());

    public VehicleUserViewModel(@NonNull final GetUserSettings getUserSettings,
                            @NonNull final FetchAndGetVehicleUser fetchAndGetVehicleUser) {
        mGetUserSettings = get(getUserSettings);
        mFetchAndGetVehicleUser = get(fetchAndGetVehicleUser);
    }

    @Override
    public void subscribeToDataStoreInternal(@NonNull final CompositeSubscription compositeSubscription) {
        checkNotNull(compositeSubscription);

/*
        compositeSubscription.add(
                mGetUserSettings.call()
                        .map(AppUserSettings::getSelectedVehicleUserId)
                        .switchMap(mFetchAndGetVehicleUser::call)
                        .subscribe(vehicleUser -> mOutStreamOfVehicleUser.apply(oldVehicleUser -> vehicleUser))
        );
*/
    }

    @NonNull
    public Observable<VehicleUser> getStreamOfVehicleUser() {
        return mOutStreamOfVehicleUser.values(StartWith.NO);
    }

}

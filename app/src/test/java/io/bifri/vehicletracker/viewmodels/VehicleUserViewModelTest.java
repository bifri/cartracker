/*
 * The MIT License
 *
 * Copyright (c) 2013-2016 reark project contributors
 *
 * https://github.com/reark/reark/graphs/contributors
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package io.bifri.vehicletracker.viewmodels;

import org.junit.Test;

import io.bifri.vehicletracker.pojo.VehicleUser;
import io.bifri.vehicletracker.pojo.response.Owner;
import io.bifri.vehicletracker.pojo.AppUserSettings;
import rx.Observable;
import rx.observers.TestSubscriber;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;

public class VehicleUserViewModelTest {

    @Test(timeout = 1000)
    public void testVehicleUserViewModelFetchesValidVehicleUser() throws Exception {
        VehicleUser vehicleUser = new VehicleUser(2,
                "vehicleuser",
                3,
                4,
                mock(Owner.class));
        VehicleUserViewModel vehicleUserViewModel = new VehicleUserViewModel(
                () -> Observable.just(new AppUserSettings(1)),
                vehicleUserId -> Observable.just(vehicleUser));
        TestSubscriber<VehicleUser> observer = new TestSubscriber<>();
        vehicleUserViewModel.getStreamOfVehicleUser().subscribe(observer);

        vehicleUserViewModel.subscribeToDataStore();

        observer.awaitTerminalEvent();
        assertEquals("Invalid number of vehicle users",
                     1,
                     observer.getOnNextEvents().size());
        assertEquals("Provided VehicleUser does not match",
                vehicleUser,
                     observer.getOnNextEvents().get(0));
    }

}

/*
 * The MIT License
 *
 * Copyright (c) 2013-2016 reark project contributors
 *
 * https://github.com/reark/reark/graphs/contributors
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package io.bifri.vehicletracker.viewmodels;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import io.bifri.vehicletracker.data.DataFunctions;
import io.bifri.vehicletracker.pojo.VehicleUser;
import io.bifri.vehicletracker.pojo.ItemList;
import rx.Observable;
import rx.observers.TestSubscriber;

import static io.reark.reark.data.DataStreamNotification.fetchingError;
import static io.reark.reark.data.DataStreamNotification.fetchingStart;
import static io.reark.reark.data.DataStreamNotification.onNext;
import static io.bifri.vehicletracker.viewmodels.VehicleUsersViewModel.ProgressStatus.ERROR;
import static io.bifri.vehicletracker.viewmodels.VehicleUsersViewModel.ProgressStatus.IDLE;
import static io.bifri.vehicletracker.viewmodels.VehicleUsersViewModel.ProgressStatus.LOADING;
import static io.bifri.vehicletracker.viewmodels.VehicleUsersViewModel.toProgressStatus;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;

public class VehicleUsersViewModelTest {

    private VehicleUsersViewModel mViewModel;

    @Before
    public void setUp() {
        mViewModel = new VehicleUsersViewModel(
                mock(DataFunctions.GetVehicleUsers.class),
                vehicleUserId -> Observable.just(mock(VehicleUser.class)));
    }

    @Test
    public void testStartFetchingReportedAsLoading() {
        assertEquals(LOADING, toProgressStatus().call(fetchingStart()));
    }

    @Test
    public void testFetchingErrorReportedAsError() {
        assertEquals(ERROR, toProgressStatus().call(fetchingError()));
    }

    @Test
    public void testAnyValueReportedAsIdle() {
        ItemList<String> value = new ItemList<>("", Collections.emptyList(), null);

        assertEquals(IDLE, toProgressStatus().call(onNext(value)));
    }

    @Test
    public void testTooManyVehicleUsersAreCappedToFive() {
        TestSubscriber<List<VehicleUser>> observer = new TestSubscriber<>();

        mViewModel.toVehicleUserList()
                 .call(Arrays.asList(1, 2, 3, 4, 5, 6))
                 .subscribe(observer);

        observer.awaitTerminalEvent();
        assertEquals("Invalid number of vehicle users",
                     5,
                     observer.getOnNextEvents().get(0).size());
    }

    @Test
    public void testTooLittleVehicleUsersReturnThoseVehicleUsers() {
        TestSubscriber<List<VehicleUser>> observer = new TestSubscriber<>();

        mViewModel.toVehicleUserList()
                 .call(Arrays.asList(1, 2, 3))
                 .subscribe(observer);

        observer.awaitTerminalEvent();
        assertEquals("Invalid number of vehicle users",
                     3,
                     observer.getOnNextEvents().get(0).size());
    }

    @Test(expected = NullPointerException.class)
    public void testThrowsNullPointerExceptionWhenVehicleUserIdIsNull() {
        //noinspection ConstantConditions
        mViewModel.getVehicleUserObservable(null);
    }

    @Test(expected = NullPointerException.class)
    public void testThrowsNullPointerExceptionWhenNetworkStatusIsNull() {
        //noinspection ConstantConditions
        mViewModel.setNetworkStatusText(null);
    }

    @Test(expected = NullPointerException.class)
    public void testThrowsNullPointerExceptionWhenSearchStringIsNull() {
        //noinspection ConstantConditions,ConstantConditions
        mViewModel.setSearchString(null);
    }

    @Test(expected = NullPointerException.class)
    public void testThrowsNullPointerExceptionWhenSelectedVehicleUserIsNull() {
        //noinspection ConstantConditions
        mViewModel.selectedVehicleUser(null);
    }

    @Test(expected = NullPointerException.class)
    public void testThrowsNullPointerExceptionConstructedWithNullVehicleUsers() {
        //noinspection ConstantConditions
        new VehicleUsersViewModel(null, mock(DataFunctions.GetVehicleUser.class));
    }

    @Test(expected = NullPointerException.class)
    public void testThrowsNullPointerExceptionConstructedWithNullVehicleUser() {
        //noinspection ConstantConditions
        new VehicleUsersViewModel(mock(DataFunctions.GetVehicleUsers.class), null);
    }

}

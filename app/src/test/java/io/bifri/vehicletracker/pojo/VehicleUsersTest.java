package io.bifri.vehicletracker.pojo;

import org.junit.Test;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class VehicleUsersTest {

    private static final List<Integer> ITEMS = Arrays.asList(10, 20, 30);
    
    @Test
    public void testSameIdPojoEquals() {
        Date updateDate = new Date();
        ItemList<String> search1 = new ItemList<>("search", ITEMS, updateDate);
        ItemList<String> search2 = new ItemList<>("search", ITEMS, updateDate);

        assertEquals(search1, search2);
    }

    @Test
    public void testDifferentIdPojoDoesNotEqual() {
        Date updateDate = new Date();
        ItemList<String> search1 = new ItemList<>("search", ITEMS, updateDate);
        ItemList<String> search2 = new ItemList<>("other", ITEMS, updateDate);

        assertFalse(search1.equals(search2));
    }

}

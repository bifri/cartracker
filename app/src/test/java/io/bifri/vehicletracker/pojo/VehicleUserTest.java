package io.bifri.vehicletracker.pojo;

import org.junit.Test;

import java.util.Date;

import io.bifri.vehicletracker.pojo.response.Owner;

import static io.bifri.vehicletracker.utils.DateUtils.EMPTY_DATE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class VehicleUserTest {

    private static final Owner OWNER = new Owner("owner");
    private static final Date DATE = EMPTY_DATE;

    @Test
    public void testSameIdPojoEquals() {
        VehicleUser vehicleUser1 = new VehicleUser(100, OWNER, DATE);
        VehicleUser vehicleUser2 = new VehicleUser(100, OWNER, DATE);

        assertEquals(vehicleUser1, vehicleUser2);
    }

    @Test
    public void testDifferentIdPojoDoesNotEqual() {
        VehicleUser vehicleUser1 = new VehicleUser(100, OWNER, DATE);
        VehicleUser vehicleUser2 = new VehicleUser(200, OWNER, DATE);

        assertFalse(vehicleUser1.equals(vehicleUser2));
    }

    @Test
    public void testOverwrite_WithItself() {
        VehicleUser vehicleUser = new VehicleUser(100, "foo", 10, 10, OWNER);

        vehicleUser.overwrite(vehicleUser);

        assertEquals(100, vehicleUser.getId());
        assertEquals("foo", vehicleUser.getName());
    }

    @Test
    public void testOverwrite_WithSameId_WithDifferentName() {
        VehicleUser vehicleUser1 = new VehicleUser(100, "foo", 10, 10, OWNER);
        VehicleUser vehicleUser2 = new VehicleUser(100, "bar", 10, 10, OWNER);

        vehicleUser1.overwrite(vehicleUser2);

        assertEquals(vehicleUser1, vehicleUser2);
    }

    @Test
    public void testOverwrite_WithSameId_WithDifferentCount() {
        VehicleUser vehicleUser1 = new VehicleUser(100, "foo", 10, 10, OWNER);
        VehicleUser vehicleUser2 = new VehicleUser(100, "foo", 20, 20, OWNER);

        vehicleUser1.overwrite(vehicleUser2);

        assertEquals(vehicleUser1, vehicleUser2);
    }

    @Test
    public void testOverwrite_WithSameId_WithDifferentOwner() {
        VehicleUser vehicleUser1 = new VehicleUser(100, "foo", 10, 10, OWNER);
        VehicleUser vehicleUser2 = new VehicleUser(100, "foo", 10, 10, new Owner("thief"));

        vehicleUser1.overwrite(vehicleUser2);

        assertEquals(vehicleUser1, vehicleUser2);
    }

    @Test
    public void testOverwrite_WithAnother() {
        VehicleUser vehicleUser1 = new VehicleUser(100, "foo", 10, 10, OWNER);
        VehicleUser vehicleUser2 = new VehicleUser(200, "bar", 10, 10, OWNER);

        vehicleUser1.overwrite(vehicleUser2);

        assertFalse(vehicleUser1.equals(vehicleUser2));
        assertEquals(100, vehicleUser1.getId());
        assertEquals("bar", vehicleUser1.getName());
    }

}

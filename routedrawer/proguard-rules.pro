# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /opt/android-sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

-verbose
-dontobfuscate
-dontusemixedcaseclassnames
-dontskipnonpubliclibraryclasses

# Optimization step doesn't update things correctly
-optimizations !code/allocation/variable

# Retain declared checked exceptions for use by a Proxy instance.
#-keepattributes Exceptions
# Retain generic type information for use by reflection by converters and adapters.
#-keepattributes Signature
# Fabric uses annotations internally
#-keepattributes *Annotation*
#-keepattributes SourceFile,LineNumberTable

-keepattributes Exceptions,Signature,SourceFile,LineNumberTable,*Annotation*,EnclosingMethod

# Removes logging
-assumenosideeffects class android.util.Log {
    public static boolean isLoggable(java.lang.String, int);
    public static int v(...);
    public static int i(...);
    public static int w(...);
    public static int d(...);
    public static int e(...);
}
-assumenosideeffects class io.reark.reark.utils.Log {
    public static int v(...);
    public static int i(...);
    public static int w(...);
    public static int d(...);
    public static int e(...);
}

# Google Play Services Exceptions
-keep class * extends java.util.ListResourceBundle {
    protected java.lang.Object[][] getContents();
}

-keep public class com.google.android.gms.common.internal.safeparcel.SafeParcelable {
    public static final *** NULL;
}

-keepnames @com.google.android.gms.common.annotation.KeepName class *
-keepclassmembernames class * {
    @com.google.android.gms.common.annotation.KeepName *;
}

-keepnames class * implements android.os.Parcelable {
    public static final ** CREATOR;
}

# Glide
-keep public class * implements com.bumptech.glide.module.GlideModule
-keep public enum com.bumptech.glide.load.resource.bitmap.ImageHeaderParser$** {
    **[] $VALUES;
    public *;
}
-keep class com.bumptech.glide.integration.okhttp.OkHttpGlideModule

# Parceler library
-keep interface org.parceler.Parcel
-keep @org.parceler.Parcel class * { *; }
-keep class **$$Parcelable { *; }

# RxJava
-dontwarn sun.misc.**
-keep class rx.internal.util.unsafe.** { *; }
-keepclassmembers class rx.internal.util.unsafe.*ArrayQueue*Field* {
   long producerIndex;
   long consumerIndex;
}
-keepclassmembers class rx.internal.util.unsafe.BaseLinkedQueueProducerNodeRef {
    rx.internal.util.atomic.LinkedQueueNode producerNode;
}
-keepclassmembers class rx.internal.util.unsafe.BaseLinkedQueueConsumerNodeRef {
    rx.internal.util.atomic.LinkedQueueNode consumerNode;
}
-dontnote rx.internal.util.PlatformDependent

# Retrolambda
-dontwarn java.lang.invoke.*

# Gson
-keep class sun.misc.Unsafe { *; }
-keep class com.google.gson.** { *; }
-dontnote sun.misc.Unsafe

# OkHttp
-keepattributes Signature
-keepattributes *Annotation*
-keep class okhttp3.** { *; }
-keep interface okhttp3.** { *; }
-dontwarn okhttp3.**

# Retrofit
#-keep class retrofit.** { *; }
#-keepclasseswithmembers class * { @retrofit.** *; }
#-keepclassmembers class * { @retrofit.** *; }
-keep class com.squareup.okhttp.** { *; }
-keep interface com.squareup.okhttp.** { *; }
-dontwarn com.squareup.okhttp.**
-dontwarn rx.**
-dontwarn retrofit.**
-dontnote retrofit.**
-keep class retrofit.** { *; }
-keep class rx.Subscriber.** { *; }
-keepclasseswithmembers class * {
    @retrofit.http.* <methods>;
}

# Retrofit2
-dontwarn retrofit2.**
-keep class retrofit2.** { *; }
-keepattributes Signature
-keepattributes Exceptions
-keepclasseswithmembers class * {
    @retrofit2.http.* <methods>;
}

# Okio
-keep class okio.*

# Jsoup
-keeppackagenames org.jsoup.nodes

# Crashlytics (Fabric)
# https://docs.fabric.io/android/crashlytics/dex-and-proguard.html
# https://docs.fabric.io/android/crashlytics/advanced-setup.html
-keep public class * extends java.lang.Exception
#To skip running ProGuard on Crashlytics
-keep class com.crashlytics.** { *; }
-dontwarn com.crashlytics.**

-dontnote org.apache.http.conn.scheme.HostNameResolver
-dontnote org.apache.http.conn.**
-dontnote org.apache.http.params.**
-dontnote android.net.http.**

# java
-dontwarn javax.naming.**
-dontwarn java.lang.invoke**
-dontwarn java.nio.file.Files
-dontwarn java.nio.file.Path
-dontwarn java.nio.file.OpenOption
-dontwarn org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement

# support-v4
#https://stackoverflow.com/questions/18978706/obfuscate-android-support-v7-widget-gridlayout-issue
-dontnote android.support.v4.**
-keep class android.support.v4.app.** { *; }
-keep interface android.support.v4.app.** { *; }
-keep class android.support.v4.** { *; }

# support-v7
-dontnote android.support.v7.**
-keep class android.support.v7.internal.** { *; }
-keep interface android.support.v7.internal.** { *; }
-keep class android.support.v7.** { *; }

# support design
#@link http://stackoverflow.com/a/31028536
-dontwarn android.support.design.**
-keep class android.support.design.** { *; }
-keep interface android.support.design.** { *; }
-keep public class android.support.design.R$* { *; }

#error : Note: the configuration refers to the unknown class 'com.google.vending.licensing.ILicensingService'
#solution : @link http://stackoverflow.com/a/14463528
-dontnote com.google.vending.licensing.ILicensingService
-dontnote **ILicensingService

-dontnote com.android.vending.billing.IInAppBillingService

# Nucleus
-keep class nucleus.factory.PresenterFactory
-keep class nucleus.factory.PresenterFactory.** { *; }

-dontnote com.android.org.conscrypt.SSLParametersImpl
-dontnote org.apache.harmony.xnet.provider.jsse.SSLParametersImpl
-dontnote sun.security.ssl.SSLContextImpl
-dontnote com.google.gson.internal.$Gson$Types$ParameterizedTypeImpl
-dontnote com.google.gson.internal.UnsafeAllocator

-dontnote com.google.android.gms.common.internal.safeparcel.SafeParcelable
-dontnote com.google.android.gms.common.annotation.KeepName
-dontnote com.bumptech.glide.module.GlideModule
-dontnote com.bumptech.glide.integration.okhttp.OkHttpGlideModule
-dontnote org.parceler.Parcel

# This file only contains the proguard options required by the Google Maps
# Android API v2. It should be used in addition to the one provided by the
# Android SDK (<sdk>/tools/proguard/proguard-android-optimize.txt).
#
# For more details on the use of proguard in Android, please read:
# http://proguard.sourceforge.net/manual/examples.html#androidapplication

-optimizations !code/simplification/variable

# The Maps API uses custom Parcelables.
# Use this rule (which is slightly broader than the standard recommended one)
# to avoid obfuscating them.
-keepclassmembers class * implements android.os.Parcelable {
    static *** CREATOR;
}

# The Maps API uses serialization.
-keepclassmembers class * implements java.io.Serializable {
    static final long serialVersionUID;
    static final java.io.ObjectStreamField[] serialPersistentFields;
    private void writeObject(java.io.ObjectOutputStream);
    private void readObject(java.io.ObjectInputStream);
    java.lang.Object writeReplace();
    java.lang.Object readResolve();
}

# Icepick
-dontwarn icepick.**
-keep class icepick.** { *; }
-keep class **$$Icepick { *; }
-keepclasseswithmembernames class * {
    @icepick.* <fields>;
}
-keepnames class * { @icepick.State *;}
